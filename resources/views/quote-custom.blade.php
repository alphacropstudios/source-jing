@extends('layouts.semantic')

@section('title')
    Quote Custom
@endsection

@section('description')

@endsection

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="ui horizontal divider"> </div>
    <form class="ui form" method="GET" action="{{ route('quote.custom.index') }}">
        <div class="ui grid fields">
            <div class="four wide field">
                <label>Order Date From</label>
                <div class="ui calendar" id="datetimepicker1">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="orderdatefrom" value="{{ request('orderdatefrom') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Order Date To</label>
                <div class="ui calendar" id="datetimepicker2">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="orderdateto" value="{{ request('orderdateto') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Delivery Date From</label>
                <div class="ui calendar" id="datetimepicker3">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="deliverydatefrom" value="{{ request('deliverydatefrom') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Delivery Date To</label>
                <div class="ui calendar" id="datetimepicker4">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="deliverydateto" value="{{ request('deliverydateto') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Main</label>
                <select class="ui dropdown" name="main" >
                    <option>All </option>
                    @foreach ($categories['1'] as $k => $category)
                        <option {{ request('main') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>General</label>
                <select class="ui dropdown" name="general" >
                    <option>All </option>
                    @foreach ($categories['2'] as $k => $category)
                        <option {{ request('general') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="four wide field">
                <label>Customer Name</label>
                <select class="ui dropdown" name="customer">
                    <option>All </option>
                    @foreach ($customers as $key => $value)
                        <option {{ request('customer') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Supplier</label>
                <select class="ui dropdown" name="supplier">
                    <option>All </option>
                    @foreach ($suppliers as $key => $value)
                        <option {{ request('supplier') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>
            <div class="four wide field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'orderdate' => 'Order Date',
                        'description' => 'Description',
                        'main' => 'Main',
                        'general' => 'General',
                        'qty' => 'Quantity',
                        'deliverydate' => 'Delivery Date',
                        'quote' => 'Quote',
                        'customer' => 'Customer',
                        'company' => 'Company',
                        'supplier' => 'Supplier',
                        'status' => 'Status',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Sort Type</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'asc' => 'Ascending',
                        'desc' => 'Descending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>&nbsp;</label>
                <div class="ui action input">
                    <input type="text" placeholder="Type a keyword.." name="q" value="{{ request('q') }}">
                    <button class="ui button">Search</button>
                </div>
            </div>
        </div>
    </form>
    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Order Date</th>
                    <th>Description</th>
                    <th>Main</th>
                    <th>General</th>
                    <th>Quantity</th>
                    <th>Quote ID</th>
                    <th>Customer Name</th>
                    <th>Company</th>
                    <th>Supplier</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if($quotecustoms->count() > 0)
                    @foreach ($quotecustoms as $k => $quotecustom)
                        <tr>
                            <td>
                                <a href="{{ route('product.create',[
                                    'main' => $quotecustom->category->parent->name,
                                    'general' => $quotecustom->category->name,
                                    'description' => $quotecustom->customname
                                    ]) }}">Create</a>
                            </td>
                            <td>{{ $quotecustom->quote->orderdate->timestamp <= 0 ? '-' : $quotecustom->quote->orderdate->format('M d, Y') }}</td>
                            <td>{{ $quotecustom->customname }}</td>
                            <td>{{ $quotecustom->category->parent->name }}</td>
                            <td>{{ $quotecustom->category->name }}</td>
                            <td>{{ $quotecustom->quantity }}</td>
                            <td>{{ str_pad($quotecustom->quote->id, 6, "0", STR_PAD_LEFT) }}</td>
                            <td>{{ $quotecustom->quote->contact->name }}</td>
                            <td>{{ $quotecustom->quote->company->name }}</td>
                            <td>{{ $quotecustom->supplier && $quotecustom->supplier->count() > 0 ? $quotecustom->supplier->first()->name : '-' }}</td>
                            <td>{{ $quotecustom->quote->status->name }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="11" class="text-center">No Results found</td>
                    </tr>
                @endif
            </tbody>
        </table>

    </div>

    <div class="ui horizontal divider"> </div>
    <div class="ui grid">
        <div class="three column row">
            <div class="left floated column">
                {{ $quotecustoms->appends(collect($_GET)->reject(function ($name) {
                    return empty($name);
                })->all())->links() }}
            </div>
            <div class="column">
                @if($quotecustoms->count())
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing {{ $quotecustoms->count() }} of {{ $quotecustoms->total() }} Results
                    </h4>
                @else
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing 0 Results
                    </h4>
                @endif
            </div>
            <div class="right floated right aligned column">
                @if($quotecustoms->count())
                    <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                        @foreach ($_GET as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                        @endforeach
                        <div class="ui action">
                            <input type="hidden" name="export" value="xls"/>
                            <button class="ui teal right labeled icon button">
                                <i class="download icon"></i>
                                Export to XLS
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection


@push('styles')
    <link rel="stylesheet" href="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.css">
@endpush
@push('scripts')
    <script type="text/javascript" src="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.js"></script>

    <script type="text/javascript">
    $(function () {
        $('#datetimepicker1').calendar({
            type: 'date',
            endCalendar: $('#datetimepicker2')
        });
        $('#datetimepicker2').calendar({
            type: 'date',
            startCalendar: $('#datetimepicker1')
        });

        $('#datetimepicker3').calendar({
            type: 'date',
            endCalendar: $('#datetimepicker4')
        });
        $('#datetimepicker4').calendar({
            type: 'date',
            startCalendar: $('#datetimepicker3')
        });
    });
    </script>
@endpush
