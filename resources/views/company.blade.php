@extends('layouts.semantic')

@section('title')
    Company
@endsection

@section('description')
    Search and Manage Companies
@endsection

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="ui form" method="GET" action="{{ route('company') }}">
        <div class="ui four fields">
            <div class="field">
                <label>Type of Business</label>
                <select class="ui dropdown" name="type">
                    <option>All </option>
                    @foreach ($businesstypes as $k => $businesstype)
                        <option {{ request('type') == $businesstype->name ? 'selected' : '' }}>{{ $businesstype->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Province</label>
                <select class="ui dropdown" name="state">
                    <option>All </option>
                    @foreach ($state as $k => $s)
                        <option {{ request('state') == $s->name ? 'selected' : '' }}>{{ $s->name }}</option>
                    @endforeach
                </select>
            </select>
        </div>
        <div class="field">
            <label>City</label>
            <select class="ui dropdown" name="city">
                <option>All </option>
                @foreach ($city as $k => $c)
                    <option {{ request('city') == $c->name ? 'selected' : '' }}>{{ $c->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="field">
            <label>Supplier</label>
            <select class="ui dropdown" name="supplier">
                <option>All </option>
                @foreach ([
                    'y' => 'Supplier',
                    'n' => 'Customer',
                    ] as $key => $value)
                    <option {{ request('supplier') == $key ? 'selected' : '' }} value="{{ $key }}">
                        {{ $value }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="ui four fields">
        <div class="field">
            <label>Results per page</label>
            <div class="ui input">
                <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
            </div>
        </div>
        <div class="field">
            <label>Order by</label>
            <select class="ui dropdown" name="sortby">
                @foreach ([
                    'name' => 'Name',
                    'type' => 'Type of Business',
                    'city' => 'City',
                    'state' => 'Province',
                    'supplier' => 'Supplier',
                    'status' => 'Status',
                    ] as $key => $value)
                    <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                        {{ $value }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="field">
            <label>Sort Type</label>
            <select class="ui dropdown" name="sortdir">
                @foreach ([
                    'asc' => 'Ascending',
                    'desc' => 'Descending',
                    ] as $key => $value)
                    <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                        {{ $value }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="field">
            <label>&nbsp;</label>
            <div class="ui action input">
                <input type="text" placeholder="Type a keyword.." name="q" value="{{ request('q') }}">
                <button class="ui button">Search</button>
            </div>
        </div>
    </div>
</form>


<div class="ui horizontal divider"> </div>

<div class="responsive-table">
    <table class="ui compact small table">
        <thead>
            <tr>
                <th>ACTION</th>
                <th>Name</th>
                <th>Type of Business</th>
                <th>City</th>
                <th>Province</th>
                <th>Supplier</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>

            @foreach ($companies as $key => $company)
                <tr>
                    <td>
                        <label><input type="checkbox" name="deleteIds" value="{{ $company->id }}"></label>&nbsp;&nbsp;&nbsp;
                        <a href="{{ route('company.edit',['company' => $company->id ]) }}">Edit</a> |
                        <a href="{{ route('company.destroy',['company' => $company->id ]) }}">Delete</a> |
                        <a href="{{ route('company.contacts',['company' => $company->id ]) }}">Contact</a> |
                        @if($company->isBlacklisted())
                            <a href="{{ route('company.whitelist',['company' => $company->id ]) }}">Whitelist</a> |
                        @else
                            <a href="{{ route('company.saveblacklist',['company' => $company->id ]) }}">Blacklist</a> |
                        @endif
                        <a href="{{ route('company.account',['company' => $company->id ]) }}">User</a> |
                        <a href="{{ route('company.product',['company' => $company->id ]) }}">Product List</a> |
                        @if($company->enabled_flag == 'y')
                            <a href="{{ route('company.deactivateaccount',['company' => $company->id ]) }}">Deactivate</a>
                        @else
                            <a href="{{ route('company.sendactivation',['company' => $company->id ]) }}">Send Activation Code</a>
                        @endif
                    </td>
                    <td>{{ $company->name }}</td>
                    <td>{{ $company->businesstype ? $company->businesstype->name : '-' }}</td>
                    <td>{{ $company->address ? $company->address->city : '-' }}</td>
                    <td>{{ $company->address ? $company->address->state : '-' }}</td>
                    <td>{{ $company->isSupplier() }}</td>
                    <td>{{ $company->isEnabled() }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot class="full-width">
            <tr>
                <th colspan="8">
                    <a class="ui primary labeled icon small button" href="{{ route('company.create') }}"><i class="plus icon"></i>Add</a>
                    <a class="ui red labeled icon small button" href="#" id="deleteCompanies"><i class="trash icon"></i>Delete</a>
                </th>
            </tr>
        </tfoot>
    </table>
</div>


<div class="ui horizontal divider"> </div>

<div class="ui grid">
    <div class="three column row">
        <div class="left floated column">
            {{ $companies->appends(collect($_GET)->reject(function ($name) {
                return empty($name);
            })->all())->links() }}
        </div>
        <div class="column">
            @if($companies->count())
                <h4 class="ui horizontal divider header">
                    <i class="tag icon"></i>
                    Showing {{ $companies->count() }} of {{ $companies->total() }} Results
                </h4>
            @else
                <h4 class="ui horizontal divider header">
                    <i class="tag icon"></i>
                    Showing 0 Results
                </h4>
            @endif
        </div>
        <div class="right floated right aligned column">
            @if($companies->count())
                <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                    @foreach ($_GET as $key => $value)
                        <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                    @endforeach
                    <div class="ui action">
                        <input type="hidden" name="export" value="xls"/>
                        <button class="ui teal right labeled icon button">
                            <i class="download icon"></i>
                            Export to XLS
                        </button>
                    </div>
                </form>
            @endif
        </div>
    </div>
</div>

@endsection
@push('scripts')
    <script type="text/javascript">
    $(function () {

        $('#deleteCompanies').click(function(e){
            e.preventDefault();
            var ids = [];
            $('[name="deleteIds"]:checked').each(function(){
                ids.push($(this).val());
            });

            if(ids.length){
                window.location.assign('/company/deleteBatch?ids=' + ids.join(','));
            }

        });

    });
    </script>
@endpush
