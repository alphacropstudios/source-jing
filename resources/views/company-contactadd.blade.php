@extends('layouts.semantic')

@section('title')
    Add Company Contact
@endsection

@section('description')
    {{ $company->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('company.contacts',[ 'company' => $company->id ]) }}">
        <i class="chevron left icon"></i> Back to Company Contacts
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" action="{{ route('company.contactstore',[ 'company' => $company->id ]) }}" method="POST">
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">
                <div class="field required">
                    <label>First Name</label>
                    <div class="ui input">
                        <input type="text" name="first_name" value="{{ old('first_name') }}" required>
                    </div>
                </div>
                <div class="field">
                    <label>Middle Name</label>
                    <div class="ui input">
                        <input type="text" name="middle_name" value="{{ old('middle_name') }}">
                    </div>
                </div>
                <div class="field required">
                    <label>Last Name</label>
                    <div class="ui input">
                        <input type="text" name="last_name" value="{{ old('last_name') }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Designation</label>
                    <div class="ui input">
                        <input type="text" name="designation" value="{{ old('designation') }}" required>
                    </div>
                </div>

                <div class="field">
                    <label>Other Details</label>
                    <div class="ui input">
                        <input type="email" name="email" value="{{ old('email') }}" placeholder="Email Address">
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="default_phone" value="{{ old('default_phone') }}" placeholder="Default Phone">
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="home_phone" value="{{ old('home_phone') }}" placeholder="Home Phone">
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="work_phone" value="{{ old('work_phone') }}" placeholder="Work Phone">
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="work_phone" value="{{ old('mobile_phone') }}" placeholder="Mobile Phone">
                    </div>
                </div>

                <div class="field required">
                    <label>Is Primary?</label>
                    <select class="ui dropdown" name="primary" required >
                        @foreach ([
                            'y' => 'Yes',
                            'n' => 'No',
                            ] as $key => $value)
                            <option {{ old('primary') == $key ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Add</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>
    @endsection
