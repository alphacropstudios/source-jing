@extends('layouts.semantic')

@section('title')
    Edit Quotes Regular
@endsection

@section('description')
    {{ $quote->id }} | {{ $quote->title ?: 'Untitled Quotation' }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('quote.regular.index') }}">
        <i class="chevron left icon"></i> Back to Quotes
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="ui horizontal divider"> </div>

    <form class="ui grid form" method="POST" action="{{ route('quote.regular.update', ['quote' => $quote->id]) }}">
        {{ csrf_field() }}

        <div class="six wide column">
            <h4 class="ui horizontal divider header">
                <i class="user icon"></i>
                Customer
            </h4>
            <div class="field required">
                <label>Customer: </label>
                <select class="ui dropdown" name="customer" >
                    <option value="">Select Customer</option>
                    @foreach ($customers as $id => $name)
                        <option {{ $quote->contact_id == $id ? 'selected' : '' }} value="{{ $id }}">{{ $name }}</option>
                    @endforeach
                </select>
            </div>

            @if(!empty($quote->contact_id))

                <div class="field required">
                    <label>Customer Details: </label>
                    <table class="ui table">
                        <tr>
                            <td> Company </td><td>{{ $quote->contact->company->name }}</td>
                        </tr>
                        <tr>
                            <td> Address </td><td>{{ join(', ',[
                                $quote->contact->company->address->street,
                                $quote->contact->company->address->city,
                                $quote->contact->company->address->state,
                                $quote->contact->company->address->zipcode,
                                ]) }}</td>
                            </tr>
                            <tr>
                                <td> Contact </td><td>{{ $quote->contact->details->where('contacttype_id','=',2)->first()->name }}</td>
                            </tr>
                            <tr>
                                <td> Email </td><td>{{ $quote->contact->details->where('contacttype_id','=',1)->first()->name }}</td>
                            </tr>
                        </table>
                    </div>
                @else
                    <div class="ui warning message" style="display:block">
                        <i class="close icon"></i>
                        <div class="header">
                            Please select a customer
                        </div>
                    </div>
                @endif
                <h4 class="ui horizontal divider header">
                    <i class="tag icon"></i>
                    Add Product
                </h4>

                <div class="field">
                    <label>Keyword</label>
                    <div class="ui left icon action input">
                        <i class="search icon"></i>
                        <input type="search" name="q">
                        <div class="ui submit button btn-search-product">Search</div>
                    </div>
                    <input type="hidden" name="redirect" value="{{ urlencode(request()->fullUrl()) }}"/>
                    <a href="{{ route('quote.regular.add.custom.item',[
                        'redirect' => urlencode(request()->fullUrl()),
                        'quote' => $quote->id
                        ]) }}">Not here? Submit a custom item</a>
                    </div>


                    <div class="field required">
                        <label>Main: </label>
                        <select class="ui dropdown" name="main" >
                            <option>All</option>
                            @foreach ($categories['1'] as $k => $category)
                                <option {{ request('main') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="field required">
                        <label>General: </label>
                        <select class="ui dropdown" name="general" >
                            @if($categories['2']->count() > 0)
                                <option>All</option>
                                @foreach ($categories['2'] as $k => $category)
                                    <option {{ request('general') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                                @endforeach
                            @else
                                <option selected disabled>None</option>
                            @endif;
                        </select>
                    </div>
                    <div class="field required">
                        <label>Brand: </label>

                        <select class="ui dropdown" name="brand" required>
                            <option>All</option>
                            @if($categories['2']->count() > 0 && $categories['3']->count() > 0)
                                @foreach ($categories['3'] as $k => $category)
                                    <option {{ request('brand') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                                @endforeach
                            @else
                                <option selected disabled>None</option>
                            @endif;
                        </select>
                    </div>

                    <div class="field required">
                        <label>Description</label>
                        <select class="ui dropdown" name="product" required>
                            @if($products->count() > 0)
                                <option value="">Select Description</option>
                                @foreach ($products as $k => $item)
                                    <option {{ request('product') == $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            @else
                                <option selected disabled>None</option>
                            @endif;
                        </select>
                    </div>


                    @if($product)
                        @if($product->images->where('primaryimage_flag','=','y')->count())
                            <img src="{{ $product->images->where('primaryimage_flag','=','y')->first()->getImageURI() }}" class="img-responsive">
                        @endif
                        <table class="ui table">
                            <thead>
                                <tr>
                                    <th>

                                    </th>
                                    <th>
                                        Quantity
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($product->prices->count())
                                    <tr>
                                        <td>
                                            Retail
                                        </td>
                                        <td>
                                            <div class="ui input" style="width:100%;">
                                                <input type="text" name="retailqty" step="1" min="1" max="{{ $product->wholesalequantity - 1 }}" value="1 to {{ $product->wholesalequantity ? $product->wholesalequantity - 1 : 'Max' }}" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="ui input">
                                                <input type="text" name="retailprice" step="any" min="0.01" data-value="{{ $product->bestRetailPrice()->retailprice }}" value="{{ formatNumber($product->bestRetailPrice()->retailprice) }}" readonly>
                                            </div>
                                        </td>
                                            <input type="hidden" name="supplier_retail" value="{{ $product->bestRetailPrice()->company_id }}"/>
                                    </tr>

                                    <tr>
                                        <td>
                                            Wholesale
                                        </td>
                                        <td>
                                            <div class="ui input" style="width:100%;">
                                                <input type="text" name="wholesalequantity" step="1" min="{{ $product->wholesalequantity }}" value="{{ $product->wholesalequantity }} up" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="ui input">
                                                <input type="text" name="wholesaleprice" step="any" min="0.01" data-value="{{ $product->bestWholeSalePrice()->wholesaleprice }}" value="{{ formatNumber($product->bestWholeSalePrice()->wholesaleprice) }}" readonly>
                                            </div>
                                            <input type="hidden" name="supplier_wholesale" value="{{ $product->bestWholeSalePrice()->company_id }}"/>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>
                                            Retail
                                        </td>
                                        <td>
                                            <div class="ui input" style="width:100%;">
                                                <input type="text" name="retailqty" step="1" min="1" max="{{ $product->wholesalequantity - 1 }}" value="1 to {{ $product->wholesalequantity ? $product->wholesalequantity - 1 : 'Max' }}" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="ui input">
                                                <input type="text" name="retailprice" data-value="{{ $product->srp }}" step="any" min="0.01" value="{{ formatNumber($product->srp) }}" readonly>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Wholesale
                                        </td>
                                        <td>
                                            <div class="ui input" style="width:100%;">
                                                <input type="text" name="wholesalequantity" step="1" min="{{ $product->wholesalequantity }}" value="{{ $product->wholesalequantity }} up" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="ui input">
                                                <input type="text" name="wholesaleprice" step="any" min="0.01" data-value="{{ $product->srp }}" value="{{ formatNumber($product->srp) }}" readonly>
                                            </div>
                                            <input type="hidden" name="supplier" value="false"/>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>


                        <div class="field required">
                            <label>Quantity to Order: </label>
                            <div class="ui action input">
                                <input type="number" name="qty" step="1" min="1" value="1">
                                <a class="ui button" id="btn-add-item" href="#">Add</a>
                            </div>
                        </div>
                    @else
                        <div class="ui warning message" style="display:block">
                            <i class="close icon"></i>
                            <div class="header">
                                Please select a description
                            </div>
                        </div>
                    @endif


                </div>
                <div class="ten wide column">
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Quotation
                    </h4>
                    <div class="ui piled segment">
                        @if($quote->quoteDetails->count())
                            <table class="ui table">
                                <thead>
                                    <tr>
                                        <th> Action </th>
                                        <th> Item </th>
                                        <th> Description </th>
                                        <th> Quantity </th>
                                        <th> Unit Price </th>
                                        <th> Total Price </th>
                                        <th> Supplier </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($quote->quoteDetails as $key => $item)

                                        <tr data-product="{{ $item->product ? $item->product->id : 0 }}"  data-cart="{{ $item->cartdetail ? $item->cartdetail->cart_id : 0 }}">
                                            <td>
                                                <a href="#" class="btn-edit-cart-item">Edit</a> |
                                                <a href="#" class="btn-delete-cart-item">Delete</a> |
                                                <a href="{{ route('product.prices',[
                                                    'product' => $item['product'] ,
                                                    'redirect' => urlencode(request()->fullUrl()),
                                                    'quote' => $quote->id,
                                                    'quotedetail' => $item->id
                                                    ]) }}" class="btn-assign-cart-item">Assign Supplier</a>
                                                </td>
                                                <td>
                                                    {{ $key + 1 }}
                                                </td>
                                                <td>
                                                    {{ $item->product->name }}
                                                </td>
                                                <td>
                                                    {{ $item->quantity }}
                                                </td>
                                                <td>
                                                    {{ $item->unitprice }}
                                                </td>
                                                <td>
                                                    {{ formatNumber($item->unitprice * $item->quantity) }}
                                                </td>
                                                <td>
                                                    {{ $item->supplier->count() ? $item->supplier->first()->name : 'None' }}
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td> Total </td>
                                            <td> {{ collect($quote->quoteDetails)->count() }} </td>
                                            <td> </td>
                                            <td> {{ collect($quote->quoteDetails)->sum('quantity') }}</td>
                                            <td> </td>
                                            <td> {{ formatNumber(collect($quote->quoteDetails)->reduce(function($carry,$item){
                                                return $carry + ($item['unitprice'] * $item['quantity']);
                                            })) }}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            @endif

                            @if($quote->quoteCustom->count())
                                <label>Other Inquiry</label>
                                <table class="ui table">
                                    <thead>
                                        <tr>
                                            <th> Item </th>
                                            <th> Description </th>
                                            <th> Gen Class </th>
                                            <th> Qty </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($quote->quoteCustom as $key => $item)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $item->customname }}</td>
                                                <td>{{ $item->category->name }}</td>
                                                <td>{{ $item->quantity }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            @endif

                            <div class="field required">
                                <label>Delivery Date</label>
                                <div class="ui calendar" id="datetimepicker1">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" name="deliverydate" value="{{ $quote->deliverydate }}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="field">
                                <label>Title: </label>
                                <div class="ui input">
                                    <input type="text" name="title" value="{{ $quote->title }}">
                                </div>
                            </div>

                            <div class="field">
                                <label>Delivery Instructions: </label>
                                <div class="ui input">
                                    <textarea name="instructions">{{ $quote->instruction }}</textarea>
                                </div>
                            </div>

                            <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Update</button>
                            @if($quote->quoteDetails->count())
                                <a href="{{ route('quote.regular.edit.print',$quote) }}" class="ui labeled icon button print-quote" target="_blank"><i class="print icon"></i>Print Quote</a>
                            @else
                                <a href="#" class="ui labeled icon button disabled" disabled><i class="print icon"></i>Print Quote</a>
                            @endif
                            <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>

                        </div>
                    </div>
                </form>
            @endsection
            @push('styles')
                <link rel="stylesheet" href="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.css">
            @endpush
            @push('scripts')
                <script type="text/javascript" src="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.js"></script>

                <script type="text/javascript">
                $(function () {

                    $('[name="main"]').on('change',function(){
                        var qry = [];

                        qry.push('main=' + $(this).val());

                        var fields = ['customer'];
                        for(var i in fields){
                            var fieldvalue = $('[name="' + fields[i] + '"]').val();
                            if(fieldvalue && fieldvalue.length){
                                qry.push( fields[i] + '=' + fieldvalue);
                            }
                        }

                        <?php
                        $url = route('quote.regular.edit',['quote' => $quote->id]);
                        ?>
                        window.location.assign('{!! $url !!}?' + qry.join('&'));
                    });
                    $('[name="general"]').on('change',function(){

                        var qry = [];

                        qry.push('general=' + $(this).val());

                        var fields = ['customer','main'];
                        for(var i in fields){
                            var fieldvalue = $('[name="' + fields[i] + '"]').val();
                            if(fieldvalue && fieldvalue.length){
                                qry.push( fields[i] + '=' + fieldvalue);
                            }
                        }

                        <?php
                        $url = route('quote.regular.edit',['quote' => $quote->id]);
                        ?>
                        window.location.assign('{!! $url !!}?' + qry.join('&'));
                    });
                    $('[name="brand"]').on('change',function(){

                        var qry = [];

                        qry.push('brand=' + $(this).val());

                        var fields = ['customer','main','general'];
                        for(var i in fields){
                            var fieldvalue = $('[name="' + fields[i] + '"]').val();
                            if(fieldvalue && fieldvalue.length){
                                qry.push( fields[i] + '=' + fieldvalue);
                            }
                        }

                        <?php
                        $url = route('quote.regular.edit',['quote' => $quote->id]);
                        ?>
                        window.location.assign('{!! $url !!}?' + qry.join('&'));
                    });
                    $('[name="customer"]').on('change',function(){

                        var qry = [];

                        qry.push('customer=' + $(this).val());

                        var fields = ['general','main','brand'];
                        for(var i in fields){
                            var fieldvalue = $('[name="' + fields[i] + '"]').val();
                            if(fieldvalue && fieldvalue.length){
                                qry.push( fields[i] + '=' + fieldvalue);
                            }
                        }

                        <?php
                        $url = route('quote.regular.edit',['quote' => $quote->id]);
                        ?>
                        window.location.assign('{!! $url !!}?' + qry.join('&'));
                    });
                    $('[name="product"]').on('change',function(){

                        var qry = [];

                        qry.push('product=' + $(this).val());

                        var fields = ['customer','general','main','brand'];
                        for(var i in fields){
                            var fieldvalue = $('[name="' + fields[i] + '"]').val();
                            if(fieldvalue && fieldvalue.length){
                                qry.push( fields[i] + '=' + fieldvalue);
                            }
                        }

                        <?php
                        $url = route('quote.regular.edit',['quote' => $quote->id]);
                        ?>
                        window.location.assign('{!! $url !!}?' + qry.join('&'));
                    });

                    // $('[name="retailqty"]').change(function() {
                    //     var totalprice = $(this).val() * parseFloat($('[name="retailprice"]').data('value'));
                    //     $('[name="retailprice"]').val(totalprice);
                    // });
                    //
                    // $('[name="wholesalequantity"]').change(function() {
                    //     var totalprice = $(this).val() * parseFloat($('[name="wholesaleprice"]').data('value'));
                    //     $('[name="wholesaleprice"]').val(totalprice);
                    // });
                    //
                    // $('[name="retailqty"],[name="wholesalequantity"]').trigger('change');

                    $('#btn-add-item').click(function(e){
                        e.preventDefault();
                        var qty = parseInt($('[name="qty"]').val());
                        var product = parseInt("<?=(request('product',0))?>");
                        var supplier = parseInt($('[name="supplier"]').val()) || 0;
                        var maxqty = parseInt($('[name="retailqty"]').attr('max'));

                        if(qty > maxqty && $('[name="supplier_wholesale"]').length){
                            var supplier = parseInt($('[name="supplier_wholesale"]').val()) || 0;
                        }

                        if(qty <= maxqty && $('[name="supplier_retail"]').length){
                            var supplier = parseInt($('[name="supplier_retail"]').val()) || 0;
                        }

                        window.location.assign("<?=route('quote.regular.add.cart.item',['quote' => $quote->id ])?>?qty="+qty+"&product="+product+"&supplier=" + supplier);

                        return false;
                    });

                    $('body').delegate('.btn-edit-cart-item','click',function(e){
                        e.preventDefault();

                        var qtycolumn = $(this).closest('tr').find('td:nth-child(4)');
                        $(qtycolumn).html('<input type="number" class="ui input input-qty-cart" min="1" value="'+parseInt($(qtycolumn).text())+'">');


                        $(this).parent().html('<a href="#" class="btn-edit-cart-item-save">Save</a> | <a href="#" class="btn-edit-cart-item-cancel">Cancel</a>');
                        return false;
                    });

                    $('body').delegate('.btn-edit-cart-item-save','click',function(e){
                        e.preventDefault();
                        var qtycolumn = $(this).closest('tr').find('td:nth-child(4)');
                        var itemcolumn = $(this).closest('tr').data('product');
                        window.location.assign("<?=route('quote.regular.edit.cart.item',['quote' => $quote])?>?qty=" + parseInt($(qtycolumn).find('input').first().val()) + "&item=" + parseInt(itemcolumn));
                        return false;
                    });

                    $('body').delegate('.btn-edit-cart-item-cancel','click',function(e){
                        e.preventDefault();

                        var qtycolumn = $(this).closest('tr').find('td:nth-child(4)');
                        var qtyinput = $(qtycolumn).find('input')[0];
                        if(qtyinput){
                            $(qtycolumn).html(qtyinput.defaultValue);
                        }

                        $(this).parent().html('<a href="#" class="btn-edit-cart-item">Edit</a> | <a href="#" class="btn-delete-cart-item">Delete</a>  | <a href="#" class="btn-assign-cart-item">Assign Supplier</a>');
                        return false;
                    });

                    $('body').delegate('.btn-delete-cart-item','click',function(e){
                        e.preventDefault();
                        var itemcolumn = $(this).closest('tr').data('product');
                        window.location.assign("<?=route('quote.regular.delete.cart.item',['quote' => $quote])?>?item=" + parseInt(itemcolumn));
                        return false;
                    });

                    $('#datetimepicker1').calendar({
                        type: 'date',
                    });


                    $('.btn-search-product').click(function() {
                        var redirect = $('[name="redirect"]').val();
                        var search = $('[name="q"]').val();
                        window.location.assign("<?=route('product')?>?q=" + search + "&search=selectredirect&redirect=" + redirect);
                    });

                    $('[name="q"]').keydown( function( event ) {
                        if ( event.which === 13 ) {
                            event.preventDefault();
                            $('.btn-search-product').trigger('click');
                            return false;
                        }
                    });

                });
                </script>
            @endpush
