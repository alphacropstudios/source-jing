@extends('layouts.semantic')

@section('title')
    Edit Product Image
@endsection

@section('description')
    {{ $product->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('product.images',['product' => $product->id ]) }}">
        <i class="chevron left icon"></i> Back to Product Images
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" action="{{ route('product.imageupdate',['product' => $product->id ,'image' => $image->id ]) }}" method="POST" enctype='multipart/form-data'>
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">

                <div class="field">
                    <label>Filename</label>
                    <div class="ui input">
                        <input type="text"  value="{{ $image->filename }}" readonly>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="image" tabindex="0" class="hidden" value="upload" checked>
                        <label>Upload Product Image</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="file" name="upload" class="ui teal right labeled icon button">
                    </div>
                </div>
                <div class="field">
                    <label>File Repository</label>
                    <select class="ui dropdown" name="level" >
                        @foreach ([
                            'Image Path','Other Path'
                            ] as $k => $path)
                            <option>{{ $path }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="description" placeholder="/images">
                    </div>
                </div>

                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="image" class="hidden" value="url">
                        <label>Use Image from URL</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="imageurl" >
                    </div>
                </div>

                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="image" class="hidden" value="current" checked>
                        <label>Use Current Image</label>
                    </div>
                </div>
                <img src="{{ $image->getImageURI() }}" class="img-responsive">
                <div class="field">
                    <div class="ui input">
                        <input type="hidden" name="current_image_id" value="{{ $image->id }}"/>
                        <input type="text" name="current_image"  value="{{ $image->getImageURI() }}" readonly>
                    </div>
                </div>

                <div class="field">
                    <label>Primary Image</label>
                    <select class="ui dropdown" name="isPrimary">
                        @foreach ([
                            'yes' => 'Yes',
                            'no' => 'No',
                            ] as $key => $value)
                            <option {{ $image->primaryimage_flag == $key[0] ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <label>Image Type</label>
                    <select class="ui dropdown" name="imagetype">
                        @foreach ([
                            'primary' => 'Primary',
                            'thumbnail' => 'Thumbnail',
                            'regular' => 'Regular',
                            'medium' => 'Medium',
                            'large' => 'Large',
                            'extra-large' => 'Extra Large',
                            ] as $key => $value)
                            <option {{ $image->imagetype->name == $value ? 'selected' : '' }} value="{{ $value }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <label>Active</label>
                    <select class="ui dropdown" name="enable_flag">
                        @foreach ([
                            'yes' => 'Yes',
                            'no' => 'No',
                            ] as $key => $value)
                            <option {{ $image->enable_flag == $key[0] ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Update</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>

@endsection

@push('scripts')

    <script type="text/javascript">
    $(function () {

        // Remove empty fields from GET forms
        // Author: Bill Erickson
        // URL: http://www.billerickson.net/code/hide-empty-fields-get-form/

        // Change 'form' to class or ID of your specific form
        $("form").submit(function() {
            $(this).find(":input").filter(function(){ return !this.value; }).attr("disabled", "disabled");
            return true; // ensure form still submits
        });

        // Un-disable form fields when page loads, in case they click back after submission
        $( "form" ).find( ":input" ).prop( "disabled", false );

    });
    </script>
@endpush
