@extends('layouts.semantic')

@section('title')
    Product Images
@endsection

@section('description')
    {{ $product->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('product') }}">
        <i class="chevron left icon"></i> Back to Products
    </a>
@endpush

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form class="ui form" action="{{ route('product.images',['product' => $product->id ]) }}" method="get">
        <div class="ui five fields">
            <div class="field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>
            <div class="field">
                <label>Image Type</label>
                <select class="ui dropdown" name="imagetype">
                    <option>All</option>
                    @foreach ($imagetype as $key => $type)
                        <option {{ request('imagetype') == $type->name ? 'selected' : '' }} value="{{ $type->name }}">
                            {{ $type->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'name' => 'Name',
                        'type' => 'Type',
                        'status' => 'Status',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'asc' => 'Ascending',
                        'desc' => 'Descending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>&nbsp;</label>
                <div class="ui action ">
                    <button class="ui button" type="submit">Search</button>
                </div>
            </div>
        </div>
    </form>

    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">
            <thead>
                <tr>
                    <th>ACTION</th>
                    <th></th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Status</th>

                </tr>
            </thead>
            <tbody>
                @if($images->count())
                    @foreach ($images as $key => $image)
                        <tr>
                            <td>
                                <label><input type="checkbox" name="deleteIds" value="{{ $image->id }}"></label>
                                &nbsp;&nbsp;&nbsp;
                                <a href="{{ route('product.imageedit',['product' => $product->id ,'image' => $image->id ]) }}">Edit</a> |
                                <a href="{{ route('product.imagedestroy',['product' => $product->id ,'image' => $image->id ]) }}">Delete</a>
                            </td>
                            <td>
                                <img src="{{ $image->getImageURI() }}" height="100"/>
                            </td>
                            <td>{{ $image->filename }}</td>
                            <td>{{ $image->imagetype->name }}</td>
                            <td>{{ $image->isEnabled() }}</td>

                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4" class="text-center">No images found</td>
                    </tr>
                @endif
                <tfoot class="full-width">
                    <tr>
                        <th colspan="5">
                            <div class="ui tiny buttons">
                                <a class="ui primary labeled icon button" href="{{ route('product.imageadd',['product'=>$product->id]) }}"><i class="plus icon"></i>Add</a>
                                <a class="ui right labeled icon button" href="#" id="deleteImages">Delete<i class="trash icon"></i></a>
                            </div>
                        </th>
                    </tr>
                </tfoot>
            </tbody>
        </table>
    </div>

    <div class="ui horizontal divider"> </div>
    <div class="ui grid">
        <div class="three column row">
            <div class="left floated column">
                {{ $images->appends(collect($_GET)->reject(function ($name) {
                    return empty($name);
                })->all())->links() }}
            </div>
            <div class="column">
                @if($images->count())
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing {{ $images->count() }} of {{ $images->total() }} Results
                    </h4>
                @else
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing 0 Results
                    </h4>
                @endif
            </div>
            <div class="right floated right aligned column">

                @if($images->count())
                    <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                        @foreach ($_GET as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                        @endforeach
                        <div class="ui action">
                            <input type="hidden" name="export" value="xls"/>
                            <button class="ui teal right labeled icon button">
                                <i class="download icon"></i>
                                Export to XLS
                            </button>
                        </div>
                    </form>
                @endif


            </div>
        </div>
    </div>

@endsection

@push('scripts')

    <script type="text/javascript">
    $(function () {

        $('#deleteImages').click(function(e){
            e.preventDefault();
            var ids = [];
            $('[name="deleteIds"]:checked').each(function(){
                ids.push($(this).val());
            });

            if(ids.length){
                window.location.assign('/product/image/deleteBatch?ids=' + ids.join(','));
            }

        });
    });
    </script>
@endpush
