@extends('layouts.semantic')

@section('title')
    Category
@endsection

@section('description')
    Search and Manage Categories
@endsection

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form id="category-search" class="ui form" method="GET" action="{{ route('category') }}">
        <div class="ui grid fields">
            <div class="four wide field">
                <label>Level</label>
                <select class="ui dropdown" name="level" >
                    <option>All </option>
                    @foreach ([
                        'Main','General','Brand'
                        ] as $k => $category)
                        <option {{ strtolower(request('level')) == strtolower($category) ? 'selected' : '' }} value="{{ strtolower($category) }}">{{ $category }}</option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Parent</label>
                <select class="ui dropdown" name="parent" >
                    @if($parents->count() > 0 )
                        <option>All </option>
                    @else
                        <option selected value="">None</option>
                    @endif
                    @foreach ($parents as $k => $category)
                        <option {{ strtolower(request('parent')) == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach

                </select>
            </div>
            <div class="four wide field">

            </div>
            <div class="four wide field">

            </div>
            <div class="four wide field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>
            <div class="four wide field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'name' => 'Name',
                        'level' => 'Level',
                        'parent' => 'Parent',
                        'status' => 'Status',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Sort Type</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'asc' => 'Ascending',
                        'desc' => 'Descending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>&nbsp;</label>
                <div class="ui action input">
                    <input type="text" placeholder="Type a keyword.." name="q" value="{{ request('q') }}">
                    <button class="ui button">Search</button>
                </div>
            </div>
        </div>
    </form>
    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">
            <thead>
                <tr>
                    <th>ACTION</th>
                    <th>Name</th>
                    <th>Level</th>
                    <th>Parent</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if($categories->count() > 0)
                    @foreach ($categories as $key => $category)
                        <tr>
                            <td>
                                <label><input name="deleteIds" value="{{ $category->id }}" type="checkbox"></label>&nbsp;&nbsp;&nbsp;
                                <a href="{{ route('category.edit',$category->id) }}">Edit</a> |
                                <a href="{{ route('category.destroy',$category->id) }}">Delete</a>
                            </td>
                            <td>{{ $category->name }}</td>
                            <td>{{ ['','Main','General','Brand'][$category->categorylevel_id] }}</td>
                            <td>{{ $category->parent_id === 0 ? 'None' : $category->parent->name }}</td>
                            <td>{{ $category->enable_flag == 'y' ? 'Active' : 'Inactive' }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5" class="text-center">No Results found</td>
                    </tr>
                @endif
            </tbody>
            <tfoot class="full-width">
                <tr>
                    <th colspan="5">
                        <div class="ui small buttons">
                            <a class="ui primary labeled icon button" href="{{ url('category/add') }}"><i class="plus icon"></i>Add</a>
                            <a class="ui right labeled icon button" href="#" id="deleteCategories">Delete<i class="trash icon"></i></a>
                        </div>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>


    <div class="ui horizontal divider"> </div>
    <div class="ui grid">
        <div class="three column row">
            <div class="left floated column">
                {{ $categories->appends(collect($_GET)->reject(function ($name) {
                    return empty($name);
                })->all())->links() }}
            </div>
            <div class="column">
                @if($categories->count())
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing {{ $categories->count() }} of {{ $categories->total() }} Results
                    </h4>
                @else
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing 0 Results
                    </h4>
                @endif
            </div>
            <div class="right floated right aligned column">

                @if($categories->count())
                    <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                        @foreach ($_GET as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                        @endforeach
                        <div class="ui action">
                            <input type="hidden" name="export" value="xls"/>
                            <button class="ui teal right labeled icon button">
                                <i class="download icon"></i>
                                Export to XLS
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script type="text/javascript">
    $(function () {

        $('#deleteCategories').click(function(e){
            e.preventDefault();
            var ids = [];
            $('[name="deleteIds"]:checked').each(function(){
                ids.push($(this).val());
            });

            if(ids.length){
                window.location.assign('/category/deleteBatch?ids=' + ids.join(','));
            }
        });

    });

    $(function () {
        $('[name="level"]').on('change',function(){
            $('#category-search').submit();
        });
    });
    </script>
@endpush
