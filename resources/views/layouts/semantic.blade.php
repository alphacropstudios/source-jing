<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Source.com.ph | @yield('title','Admin Log In')</title>
    <link rel="stylesheet" href="{{ asset('semantic/dist/semantic.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    @stack('styles')

</head>
<body>
    @if (Auth::user())
        <div class="ui top fixed menu">
            <a class="item toggler">
                <i class="sidebar icon"></i>
            </a>
            @stack('menubar')
            <div class="right menu">
                <div class="item">
                    <i class="user circle icon"></i>
                    {{ Auth::user()->name }}
                </div>

                <a href="{{ route('logout') }}" class="ui item" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Logout</a>

            </div>

        </div>
        <div class="ui bottom attached ">
            <div class="ui sidebar inverted vertical menu">
                @include('sidebar')
            </div>
            <div class="pusher">
                <div class="ui  container">
                    <h1 class="ui header">
                        @yield('title','Administrator')
                        <div class="sub header">@yield('description','')</div>
                    </h1>
                    @yield('content')
                </div>
            </div>
        </div>



    @else
        @yield('content')
    @endif


    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/semantic.js') }}"></script>
    <script>

    $('.ui.sidebar')
    .sidebar({
        context: $('.bottom.attached')
    })
    .sidebar('attach events', '.menu .item.toggler');

    $('.message .close')
    .on('click', function() {
        $(this)
        .closest('.message')
        .transition('fade')
        ;
    });

    $('.ui.dropdown').dropdown();
    $('.ui.radio.checkbox')
  .checkbox()
;

    $("form").submit(function() {
        $(this).addClass('loading');
        $(this).find(":input").filter(function(){ return !this.value; }).attr("disabled", "disabled");
        return true;
    });

    $( "form" ).find( ":input" ).prop( "disabled", false );

    </script>
    @stack('scripts')


</body>
</html>
