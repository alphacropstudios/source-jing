@extends('layouts.semantic')

@section('title')
    Add Category Level
@endsection

@section('description')

@endsection

@push('menubar')
    <a class="item" href="{{ route('category.level') }}">
        <i class="chevron left icon"></i> Back to Category Levels
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" action="{{ route('category.levelstore') }}" method="POST">
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">
                <div class="field required">
                    <label>Name</label>
                    <div class="ui input">
                        <input type="text" name="name" value="{{ old('name') }}" required>
                    </div>
                </div>
                <div class="field">
                    <label>Description</label>
                    <div class="ui input">
                        <input type="text" name="description" value="{{ old('description') }}" >
                    </div>
                </div>

                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Save</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>
@endsection
