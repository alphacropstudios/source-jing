@extends('layouts.semantic')

@section('title')
    Add Company
@endsection

@section('description')

@endsection

@push('menubar')
    <a class="item" href="{{ route('company') }}">
        <i class="chevron left icon"></i> Back to Companies
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui form" action="{{ route('company.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="ui tab active" data-tab="step-one">
            <div class="ui grid">
                <div class="three column row">
                    <div class="column">
                        <div class="field required">
                            <label>Username</label>
                            <div class="ui input">
                                <input type="text" name="username" value="{{ old('username') }}" >
                            </div>
                        </div>
                        <div class="field required">
                            <label>Password</label>
                            <div class="ui input">
                                <input type="password" name="password" value="{{ old('password') }}" >
                            </div>
                        </div>
                        <div class="field required">
                            <label>Confirm Password</label>
                            <div class="ui input">
                                <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" >
                            </div>
                        </div>
                        <div class="field required">
                            <label>Company Name</label>
                            <div class="ui input">
                                <input type="text" name="companyname" value="{{ old('companyname') }}" >
                            </div>
                        </div>
                        <div class="field required">
                            <label>Address: Unit # / Bldg / House # / Street / Barangay</label>
                            <div class="ui input">
                                <textarea rows="2" name="address" >{{ old('address') }}</textarea>
                            </div>

                        </div>
                        <div class="field">
                            <label>City</label>
                            <select class="ui dropdown" name="city" >
                                @foreach ($city as $key => $c)
                                    <option {{ old('city') == $c->name ? 'selected' : '' }}>{{ $c->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label>Province</label>
                            <select class="ui dropdown" name="state" >
                                @foreach ($state as $key => $s)
                                    <option {{ old('state') == $s->name ? 'selected' : '' }}>{{ $s->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label>Country</label>
                            <select class="ui dropdown" name="country" >
                                @foreach ($country as $key => $c)
                                    <option {{ old('country') == $c['id'] ? 'selected' : '' }} value="{{ $c['id'] }}">{{ $c['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field required">
                            <label>Zipcode</label>
                            <div class="ui input">
                                <input type="text" name="zipcode" value="{{ old('zipcode') }}" >
                            </div>
                        </div>
                        <div class="field required">
                            <label>Primary Contact Name</label>
                            <div class="ui input">
                                <input type="text" name="contact" value="{{ old('contact') }}" >
                            </div>
                        </div>
                        <div class="field required">
                            <label>Contact Number</label>
                            <div class="ui input">
                                <input type="text" name="contact_number" value="{{ old('contact_number') }}" >
                            </div>
                        </div>
                        <div class="field required">
                            <label>Contact Email</label>
                            <div class="ui input">
                                <input type="text" name="email" value="{{ old('email') }}" >
                            </div>
                        </div>
                        <div class="field required">
                            <label>Contact Designation</label>
                            <div class="ui input">
                                <input type="text" name="designation" value="{{ old('designation') }}" >
                            </div>
                        </div>
                        <div class="field">
                            <label>Supplier Account?</label>
                            <select class="ui dropdown" name="supplier" >
                                <option>Yes</option>
                                <option>No</option>
                            </select>
                        </div>
                        <a class="ui left primary floated labeled icon button btn-nxt-tab" href="#"><i class="save icon"></i>Next</a>
                        <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui tab" data-tab="step-two">
            <div class="ui grid">
                <div class="row" style="flex-direction: row-reverse;">
                    <a class="ui right floated labeled icon button btn-prev-tab" href="#"><i class="left arrow icon"></i>Back</a>
                </div>
                <div class="three column row">
                    <div class="column">
                        <div class="field required">
                            <label>VAT #</label>
                            <div class="ui input">
                                <input type="text" name="vat" value="{{ old('vat') }}" >
                            </div>
                        </div>
                        <div class="field">
                            <label>Type of Business</label>
                            <select class="ui dropdown" name="businesstype" >
                                @foreach ($businesstypes as $key => $c)
                                    <option {{ old('businesstype') == $c['id'] ? 'selected' : '' }} value="{{ $c['id'] }}">{{ $c['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field required">
                            <label>Business Registration</label>
                            <div class="ui input">
                                <input type="text" name="registration" value="{{ old('registration') }}" >
                            </div>
                        </div>
                        <div class="field">
                            <label>Upload Photo of Business Location</label>
                            <div class="ui input">
                                <input type="file" name="upload" class="ui teal right labeled icon button" >
                            </div>
                            <small>Maximum of 100MB only (jpeg/gif/png)</small>
                        </div>
                        <div class="field required">
                            <label>Secondary Contact Name</label>
                            <div class="ui input">
                                <input type="text" name="contact2" value="{{ old('contact2') }}">
                            </div>
                        </div>
                        <div class="field required">
                            <label>Contact Number</label>
                            <div class="ui input">
                                <input type="text" name="contact_number2" value="{{ old('contact_number2') }}">
                            </div>
                        </div>
                        <div class="field required">
                            <label>Contact Email</label>
                            <div class="ui input">
                                <input type="email" name="email2" value="{{ old('email2') }}">
                            </div>
                        </div>
                        <div class="field required">
                            <label>Contact Designation</label>
                            <div class="ui input">
                                <input type="text" name="designation2" value="{{ old('designation2') }}">
                            </div>
                        </div>
                    </div>
                </div>

                <a class="ui left primary floated labeled icon button btn-form-submit" href="#"><i class="save icon"></i>Save</a>
            </div>
        </div>
    </form>
@endsection
@push('scripts')
    <script>
    jQuery(function(){
        $('.btn-nxt-tab').click(function(){

            var valid = 1;
            var fields = [
                'username','password', 'password_confirmation', 'companyname', 'address', 'country', 'zipcode', 'contact', 'contact_number', 'email', 'designation', 'supplier'
            ];

            for (var i = 0; i < fields.length; i++) {

                if(!$('.ui.form').form('is valid',fields[i])){
                    valid = 0;
                    break;
                }
            }
            if(valid){
                $.tab('change tab', 'step-two');
            }
        });
        $('.btn-prev-tab').click(function(){
            $.tab('change tab', 'step-one');
        });
        $('.btn-form-submit').on('click',function(e){
            e.preventDefault();
            $('.ui.form').form('validate form');

            if($('.ui.form').form('is valid')){
                $('.ui.form').form('submit');
                return true;
            }
            return false;
        });

        $('.ui.form')
        .form({
            fields: {
                username : ['empty'],
                password : ['empty'],
                password_confirmation : ['empty'],
                companyname : ['empty'],
                address : ['empty'],
                country : ['empty'],
                zipcode : ['empty'],
                contact : ['empty'],
                contact_number : ['empty'],
                email : ['empty'],
                designation : ['empty'],
                supplier : ['empty'],
            }
        })
        ;
    });
    </script>
@endpush
