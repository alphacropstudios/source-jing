@extends('layouts.semantic')


@section('title')
    Edit Category
@endsection

@section('description')
    {{ $category->name }} {{ $category->description }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('category') }}">
        <i class="chevron left icon"></i> Back to Categories
    </a>
@endpush

@section('content')

    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <form class="ui grid form" method="POST" action="{{ route('category.update',$category->id) }}">
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">

                <div class="field">
                    <label>Category Name</label>
                    <div class="ui input">
                        <input type="text" name="name" value="{{ $category->name }}">
                    </div>
                </div>
                <div class="field">
                    <label>Description</label>
                    <div class="ui input">
                        <textarea name="description" rows="2">{{ $category->description }}</textarea>
                    </div>
                </div>
                <div class="field">
                    <label>Level</label>
                    <select id="category-level" class="ui dropdown" name="level" >
                        @foreach ([
                            'Main','General','Brand'
                            ] as $k => $c)
                            <option {{ (request('level') && request('level') == $k+1) || ($category->categorylevel_id == $k+1) ? 'selected' : '' }} value="{{ $k+1 }}">{{ $c }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="field">
                    <label>Parent</label>
                    <select class="ui dropdown" name="parent" >
                        @if($parents->count() == 0 )
                            <option value="0">None</option>
                        @else
                            @foreach ($parents as $k => $c)
                                <option {{ $c->id == $category->parent_id ? 'selected' : '' }} value="{{ $c->id }}">{{ $c->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="field">
                    <label>Active</label>
                    <select class="ui dropdown" name="enable_flag">
                        @foreach ([
                            'y' => 'Yes',
                            'n' => 'No',
                            ] as $key => $value)
                            <option {{ $category->enable_flag == $key[0] ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Update</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Reset</button>
            </div>
        </div>

    </form>

@endsection

@push('scripts')

    <script type="text/javascript">
    $(function () {

        $('#category-level').on('change',function(){

            qry.push('level=' + $(this).val());

            var fields = ['parent'];
            for(var i in fields){
                var fieldvalue = $('[name="' + fields[i] + '"]').val();
                if(fieldvalue && fieldvalue.length){
                    qry.push( fields[i] + '=' + fieldvalue);
                }
            }

            <?php
            $url = route('category.edit',[ 'category' => $category->id ]);
            ?>
            window.location.assign('{!! $url !!}?' + qry.join('&'));

        });

        $('[name="parent"]').on('change',function(){

            qry.push('parent=' + $(this).val());

            var fields = ['level'];
            for(var i in fields){
                var fieldvalue = $('[name="' + fields[i] + '"]').val();
                if(fieldvalue && fieldvalue.length){
                    qry.push( fields[i] + '=' + fieldvalue);
                }
            }

            <?php
            $url = route('category.edit',[ 'category' => $category->id ]);
            ?>
            window.location.assign('{!! $url !!}?' + qry.join('&'));

        });

    });
    </script>
@endpush
