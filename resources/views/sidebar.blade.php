<div class="item">
    <div class="header">Source Database</div>
    <div class="menu">
        <a class="item" href="{{ url('home') }}">Dashboard</a>
    </div>
</div>
<div class="item">
    <div class="header">Actions</div>
    <div class="menu">
        <a href="{{ route('category') }}" class="item">Category</a>
        <a href="{{ route('product') }}" class="item">Product</a>
        <a href="{{ route('company') }}" class="item">Company</a>
        <a href="{{ route('category.level') }}" class="item">Category Level</a>
        <a href="{{ route('quote.regular.index') }}" class="item">Quote - Regular</a>
        <a href="{{ route('quote.custom.index') }}" class="item">Quote - Custom</a>
        <a href="{{ route('customer.reviews') }}" class="item">Customer Reviews</a>
        <a href="{{ route('admin.users') }}" class="item">Admin User</a>
    </div>
</div>
<div class="item">
    <div class="header">Account</div>
    <div class="menu">
        <a href="{{ route('logout') }}" class="item" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">Logout</a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>
