@extends('layouts.semantic')

@section('title')
    Send Activation
@endsection

@section('description')
    {{ $company->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('company') }}">
        <i class="chevron left icon"></i> Back to Companies
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @if($company->account)
        <form class="ui grid form" action="{{ route('company.sendactivationemail',[ 'company' => $company->id ]) }}" method="POST">
            {{ csrf_field() }}
            <div class="three column row">
                <div class="column">



                    <div class="field">
                        <label>Account Fullname</label>
                        <div class="ui input">
                            <input type="text" name="first_name" value="{{ $company->contact->name }}" readonly>
                        </div>
                    </div>
                    <div class="field">
                        <label>Email Address</label>
                        <div class="ui input">
                            <input type="email" name="email" value="{{  $company->contact->details()->where('contacttype_id',1)->first()->name }}" placeholder="Email Address" required>
                        </div>
                    </div>

                    <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Send</button>
                    <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
                </div>
            </div>
        </form>
    @else
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                No Account yet
            </div>
            <ul class="list">
                <li><a href="{{ route('company.account',['company' => $company->id]) }}">Click here to create now.</a></li>
            </ul>
        </div>
    @endif
@endsection
