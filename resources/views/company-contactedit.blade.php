@extends('layouts.semantic')

@section('title')
    Edit Company Contact
@endsection

@section('description')
    {{ $company->name }} | {{ $contact->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('company.contacts',[ 'company' => $company->id ]) }}">
        <i class="chevron left icon"></i> Back to Company Contacts
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" action="{{ route('company.contactupdate',[ 'company' => $company->id , 'contact' => $contact->id ]) }}" method="POST">
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">
                <div class="field required">
                    <label>First Name</label>
                    <div class="ui input">
                        <input type="text" name="first_name" value="{{ $contact->firstname }}" required>
                    </div>
                </div>
                <div class="field">
                    <label>Middle Name</label>
                    <div class="ui input">
                        <input type="text" name="middle_name" value="{{ $contact->middlename }}">
                    </div>
                </div>
                <div class="field required">
                    <label>Last Name</label>
                    <div class="ui input">
                        <input type="text" name="last_name" value="{{ $contact->lastname }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Designation</label>
                    <div class="ui input">
                        <input type="text" name="designation" value="{{ $contact->designation }}" required>
                    </div>
                </div>

                <div class="field">
                    <label>Other Details</label>
                </div>
                @foreach ([ 'email' => 'Email Address', 'default_phone' => 'Default Phone', 'home_phone' => 'Home Phone', 'work_phone' => 'Work Phone', 'mobile_phone' => 'Mobile Phone' ] as $key => $value)
                    @php
                        $detail = $contact->details()->where('contacttype_id',$loop->index + 1)->first();
                    @endphp
                    <div class="field">
                        <div class="ui input">
                            <input type="text" name="{{ $key }}" value="{{ $detail ? $detail->name : '' }}" placeholder="{{ $value }}">
                        </div>
                    </div>
                @endforeach

                <div class="field required">
                    <label>Is Primary?</label>
                    <select class="ui dropdown" name="primary" required >
                        @foreach ([
                            'y' => 'Yes',
                            'n' => 'No',
                            ] as $key => $value)
                            <option {{ $contact->primary_flag == $key ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Save</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>
    @endsection
