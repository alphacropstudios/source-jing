@extends('layouts.semantic')

@section('title')
    Admin Users
@endsection

@section('description')

@endsection

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="ui form" action="" method="get">
        <div class="ui four fields">
            <div class="field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>
            <div class="field">
                <label>Type</label>
                <select class="ui dropdown" name="type">
                    <option>All</option>
                    @foreach ($types as $key => $value)
                        <option {{ request('type') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'username' => 'Username',
                        'email' => 'Email',
                        'type' => 'Type',
                        'name' => 'Name',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'asc' => 'Ascending',
                        'desc' => 'Descending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>&nbsp;</label>
                <div class="ui action ">
                    <button class="ui button" type="submit">Search</button>
                </div>
            </div>
        </div>
    </form>

    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                @if($users->count() > 0 )
                    @foreach ($users as $key => $user)
                        <tr>
                            <td>
                                <label><input name="deleteIds" value="{{ $user->id }}" type="checkbox"></label>&nbsp;&nbsp;&nbsp;
                                <a href="{{ route('admin.users.edit',['user' => $user->id]) }}">Edit</a> |
                                <a href="{{ route('admin.users.destroy',['user' => $user->id]) }}">Delete</a>
                            </td>
                            <td>{{ $user->username }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->type ? $user->type->name : '-' }}</td>
                            <td>{{ $user->name }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="85">No Users yet</td>
                    </tr>
                @endif
            </tbody>
            <tfoot class="full-width">
                <tr>
                    <th colspan="8">
                        <div class="ui tiny buttons">
                            <a class="ui primary labeled icon button" href="{{ route('admin.users.add') }}"><i class="plus icon"></i>Add</a>
                            <a class="ui right labeled icon button" href="#" id="deleteUsers">Delete<i class="trash icon"></i></a>
                        </div>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="ui horizontal divider"> </div>

    <div class="ui grid">
        <div class="three column row">
            <div class="left floated column">
                @if($users->count())
                    {{ $users->appends(collect($_GET)->reject(function ($name) {
                        return empty($name);
                    })->all())->links() }}
                @endif
            </div>
            <div class="column">
                @if($users->count())
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing {{ $users->count() }} of {{ $users->total() }} Results
                    </h4>
                @else
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing 0 Results
                    </h4>
                @endif
            </div>
            <div class="right floated right aligned column">
                @if($users->count())
                    <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                        @foreach ($_GET as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                        @endforeach
                        <div class="ui action">
                            <input type="hidden" name="export" value="xls"/>
                            <button class="ui teal right labeled icon button">
                                <i class="download icon"></i>
                                Export to XLS
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>


@endsection

@push('scripts')
    @if($users->count())
        <script type="text/javascript">
        $(function () {

            $('#deleteUsers').click(function(e){
                e.preventDefault();
                var ids = [];
                $('[name="deleteIds"]:checked').each(function(){
                    ids.push($(this).val());
                });

                if(ids.length){
                    window.location.assign('{{ route('admin.users.destroyBatch',['user' => $user->id ]) }}?ids=' + ids.join(','));
                }
            });

        });
        </script>
    @endif
@endpush
