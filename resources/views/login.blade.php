@extends('layouts.semantic')

@section('content')
    <div id="particles-js"></div>
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui grey image header">
                <div class="content">
                    Log-in to your account
                </div>
            </h2>

            @if (session('status'))
                <div class="ui success message">
                    <i class="close icon"></i>
                    <div class="header">
                        All good!
                    </div>
                    <p>{{ session('status') }}</p>
                </div>
            @endif

            @if (session('message'))
                <div class="ui success message">
                    <i class="close icon"></i>
                    <div class="header">
                        Whoops!
                    </div>
                    <p>{{ session('message') }}</p>
                </div>
            @endif
            <form class="ui large form {{ $errors->count() ? 'error' : '' }}" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="ui stacked segment">
                    <div class="field {{ $errors->has('username') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="user icon"></i>
                            <input type="text" name="username" placeholder="Username" value="{{ old('username') }}">
                        </div>
                    </div>
                    <div class="field {{ $errors->has('password') ? ' error' : '' }}">
                        <div class="ui left icon input">
                            <i class="lock icon"></i>
                            <input type="password" name="password" placeholder="Password">
                        </div>
                    </div>
                    <button class="ui fluid large black submit button" type="submit">Login</button>
                </div>
                @if($errors->count())
                    <div class="ui error message">
                        <div class="header">Could you check something!</div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </form>
        </div>
    </div>

@endsection


@push('styles')
<style type="text/css">
body {
    background-color: #111;
}
body > .grid {
    height: 100%;
    z-index: 2;
    position: relative;
}
.image {
    margin-top: -100px;
}
.column {
    max-width: 450px;
}
#particles-js {
  position: absolute;
  width: 100vw;
  height: 100vh;
  z-index: 1;
  display: block;
}
#particles-js canvas {
  width: 100vw!important;
  height: 100vh!important;
}
</style>
@endpush

@push('scripts')
    <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script>
    if($('#particles-js').length > 0) {
        particlesJS("particles-js", {
            "particles": {
                "number": {
                    "value": 80,
                    "density": {
                        "enable": true,
                        "value_area": 800
                    }
                },
                "color": {
                    "value": "#eee"
                },
                "shape": {
                    "type": "circle",
                    "stroke": {
                        "width": 0,
                        "color": "#000000"
                    },
                    "polygon": {
                        "nb_sides": 5
                    },
                    "image": {
                        "src": "img/github.svg",
                        "width": 100,
                        "height": 100
                    }
                },
                "opacity": {
                    "value": 0.5,
                    "random": false,
                    "anim": {
                        "enable": false,
                        "speed": 1,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                },
                "size": {
                    "value": 3,
                    "random": true,
                    "anim": {
                        "enable": false,
                        "speed": 40,
                        "size_min": 0.1,
                        "sync": false
                    }
                },
                "line_linked": {
                    "enable": true,
                    "distance": 150,
                    "color": "#ffffff",
                    "opacity": 0.4,
                    "width": 1
                },
                "move": {
                    "enable": true,
                    "speed": 3,
                    "direction": "none",
                    "random": true,
                    "straight": false,
                    "out_mode": "out",
                    "bounce": false,
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                }
            },
            "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "bubble"
                    },
                    "onclick": {
                        "enable": true,
                        "mode": "repulse"
                    },
                    "resize": true
                },
                "modes": {
                    "grab": {
                        "distance": 400,
                        "line_linked": {
                            "opacity": 1
                        }
                    },
                    "bubble": {
                        "distance": 207.079689136843,
                        "size": 7,
                        "duration": 5,
                        "opacity": 0.5,
                        "speed": 3
                    },
                    "repulse": {
                        "distance": 200,
                        "duration": 0.4
                    },
                    "push": {
                        "particles_nb": 4
                    },
                    "remove": {
                        "particles_nb": 2
                    }
                }
            },
            "retina_detect": true
        });
    }
    </script>
@endpush
