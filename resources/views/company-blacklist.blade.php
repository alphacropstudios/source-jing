@extends('layouts.semantic')

@section('title')
    Blacklist Company
@endsection

@section('description')
    {{ $company->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('company') }}">
        <i class="chevron left icon"></i> Back to Companies
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" action="{{ route('company.saveblacklist',['company' => $company->id ]) }}" method="POST">
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">
                <div class="field required">
                    <label>Reason</label>
                    <div class="ui input">
                        <textarea rows="2" name="message" required>{{ old('message') }}</textarea>
                    </div>
                </div>
                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Blacklist</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>
    @endsection
