@extends('layouts.semantic')

@section('title')
    Product Prices
@endsection

@section('description')
    {{ $product->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('product') }}">
        <i class="chevron left icon"></i> Back to Products
    </a>
@endpush

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="ui form" action="{{ route('product.prices',['product' => $product->id ]) }}" method="get">
        <div class="ui four fields">
            <div class="field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>
            <div class="field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'supplier' => 'Supplier',
                        'retailprice' => 'Retail Price',
                        'wholesaleprice' => 'Wholesale Price',
                        'dateupdatedretailprice' => 'Date Updated Retail Price',
                        'dateupdatedwholesaleprice' => 'Date Wholesale Retail Price',
                        'status' => 'Status',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'asc' => 'Ascending',
                        'desc' => 'Descending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>&nbsp;</label>
                <div class="ui action ">
                    <button class="ui button" type="submit">Search</button>
                </div>
            </div>
            @if(request('search'))
                <input type="hidden" name="search" value="{{ request('search') }}"/>
            @endif
            @if(request('quote'))
                <input type="hidden" name="quote" value="{{ request('quote') }}"/>
            @endif
            @if(request('quotedetail'))
                <input type="hidden" name="quotedetail" value="{{ request('quotedetail') }}"/>
            @endif
            @if(request('redirect'))
                <input type="hidden" name="redirect" value="{{ request('redirect') }}"/>
            @endif
            @if(request('tmp'))
                <input type="hidden" name="tmp" value="{{ request('tmp') }}"/>
            @endif
        </div>
    </form>

    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">
            <thead>
                <tr>
                    @if(request('redirect'))
                        <th>Action</th>
                    @endif
                    <th>Supplier</th>
                    <th>Retail Price</th>
                    <th>Wholesale Price</th>
                    <th>Date Retail Updated</th>
                    <th>Date Wholesale Updated</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if($prices->count() > 0 )
                    @foreach ($prices as $key => $price)
                        @if($price->supplier)
                            <tr>
                                @if(request('redirect'))
                                    <td>
                                        <a href="{{ route('quote.regular.assign',[
                                            'supplier' => $price->supplier->id,
                                            'product' => $product->id,
                                            'tmp' => request('tmp',false),
                                            'redirect' => request('redirect'),
                                            'quote' => request('quote',null),
                                            'quotedetail' => request('quotedetail',null)
                                            ]) }}" class="btn-edit-cart-item">
                                            Select
                                        </a>
                                    </td>
                                @endif
                                <td>{{ $price->supplier->name }}</td>
                                <td>{{ $price->retailprice }}</td>
                                <td>{{ $price->wholesaleprice }}</td>
                                <td>{{ $price->dateupdatedretailprice }}</td>
                                <td>{{ $price->dateupdatedwholesaleprice }}</td>
                                <td>{{ $price->enable_flag=='y' ? 'Active' : 'Inactive'}}</td>
                            </tr>
                        @endif
                    @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="6">No Prices yet</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>

    <div class="ui horizontal divider"> </div>

    <div class="ui grid">
        <div class="three column row">
            <div class="left floated column">
                {{ $prices->appends(collect($_GET)->reject(function ($name) {
                    return empty($name);
                })->all())->links() }}
            </div>
            <div class="column">
                @if($prices->count())
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing {{ $prices->count() }} of {{ $prices->total() }} Results
                    </h4>
                @else
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing 0 Results
                    </h4>
                @endif
            </div>
            <div class="right floated right aligned column">
                @if($prices->count())
                    <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                        @foreach ($_GET as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                        @endforeach
                        <div class="ui action">
                            <input type="hidden" name="export" value="xls"/>
                            <button class="ui teal right labeled icon button">
                                <i class="download icon"></i>
                                Export to XLS
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>


@endsection
