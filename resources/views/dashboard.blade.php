@extends('layouts.semantic')

@section('title')
    Dashboard
@endsection

@section('description')
    Search and Track Quotations
@endsection

@section('content')
    <div class="ui equal width grid tiny statistics">
        <div class="column center aligned">
            <div class="ui tiny statistic">
                <div class="value">
                    {{ formatNumber($stats['total'],0) }}
                </div>
                <div class="label">
                    Total Items
                </div>
            </div>
        </div>
        <div class="column center aligned">
            <div class="ui tiny statistic">
                <div class="value">
                    {{ formatNumber($stats['quantity'],0) }}
                </div>
                <div class="label">
                    Total Quantity
                </div>
            </div>
        </div>
        <div class="column center aligned">
            <div class="ui tiny statistic">
                <div class="value">
                    {{ formatNumber($stats['totalprice']) }}
                </div>
                <div class="label">
                    Total Amount
                </div>
            </div>
        </div>
        <div class="column center aligned">

        </div>
    </div>
    <div class="ui horizontal divider"> </div>
    <form class="ui form" method="GET" action="{{ route('home') }}">
        <div class="ui grid fields">
            <div class="four wide field">
                <label>Order Date From</label>
                <div class="ui calendar" id="datetimepicker1">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="orderdatefrom" value="{{ request('orderdatefrom') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Order Date To</label>
                <div class="ui calendar" id="datetimepicker2">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="orderdateto" value="{{ request('orderdateto') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Delivery Date From</label>
                <div class="ui calendar" id="datetimepicker3">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="deliverydatefrom" value="{{ request('deliverydatefrom') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Delivery Date To</label>
                <div class="ui calendar" id="datetimepicker4">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="deliverydateto" value="{{ request('deliverydateto') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Main</label>
                <select class="ui dropdown" name="main" >
                    <option>All </option>
                    @foreach ($categories['1'] as $k => $category)
                        <option {{ request('main') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>General</label>
                <select class="ui dropdown" name="general" >
                    <option>All </option>
                    @foreach ($categories['2'] as $k => $category)
                        <option {{ request('general') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Brand</label>
                <select class="ui dropdown" name="brand" >
                    <option>All </option>
                    @foreach ($categories['3'] as $k => $category)
                        <option {{ request('brand') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">

            </div>
            <div class="four wide field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>
            <div class="four wide field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'orderdate' => 'Order Date',
                        'description' => 'Description',
                        'main' => 'Main Class',
                        'general' => 'General Class',
                        'brand' => 'Brand Class',
                        'qty' => 'Quantity',
                        'unitprice' => 'Unit Price',
                        'totalprice' => 'Total Price',
                        'deliverydate' => 'Delivery Date',
                        'quoteid' => 'Quote ID',
                        'customername' => 'Customer Name',
                        'company' => 'Company',
                        'supplier' => 'Supplier',
                        'status' => 'Status',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Sort Type</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'asc' => 'Ascending',
                        'desc' => 'Descending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>&nbsp;</label>
                <div class="ui action input">
                    <input type="text" placeholder="Type a keyword.." name="q" value="{{ request('q') }}">
                    <button class="ui button">Search</button>
                </div>
            </div>
        </div>
    </form>
    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">
            <thead>
                <tr>
                    <th>Order Date</th>
                    <th>Description</th>
                    <th>Main </th>
                    <th>General</th>
                    <th>Brand</th>
                    <th>Quantity</th>
                    <th>Unit Price</th>

                    <th>Total Price</th>
                    <th>Delivery Date</th>
                    <th>Quote ID</th>
                    <th>Customer Name</th>
                    <th>Company</th>
                    <th>Supplier</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if($quotedetails->count() > 0)
                    @foreach ($quotedetails as $k => $quotedetail)
                        <tr>
                            <td title="{{ $quotedetail->quote->orderdate }}">{{ $quotedetail->quote->orderdate->timestamp <=0 ? '-' : $quotedetail->quote->orderdate->format('M d, Y') }}</td>
                            <td>{{ $quotedetail->product->name }}</td>
                            <td>{{ $quotedetail->product->category->parent->parent->name }}</td>
                            <td>{{ $quotedetail->product->category->parent->name }}</td>
                            <td>{{ $quotedetail->product->category->name }}</td>
                            <td>{{ formatNumber($quotedetail->quantity,0) }}</td>
                            <td>{{ formatNumber($quotedetail->unitprice) }}</td>
                            <td>{{ formatNumber($quotedetail->totalprice) }}</td>
                            <td>{{ $quotedetail->quote->deliverydate->timestamp <=0 ? '-' : $quotedetail->quote->deliverydate->format('M d, Y') }}</td>
                            <td>{{ $quotedetail->quote->id }}</td>
                            <td>{{ $quotedetail->cartdetail->cart->contact->name }}</td>
                            <td>{{ $quotedetail->cartdetail->cart->contact->company->name }}</td>
                            <td>{{ $quotedetail->quote->supplier->count() > 0 ? $quotedetail->quote->supplier->first()->name : '-' }}</td>
                            <td>{{ $quotedetail->quote->status->name }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="14" class="text-center">No Results found</td>
                    </tr>
                @endif
            </tbody>
        </table>

    </div>

    <div class="ui horizontal divider"> </div>
    <div class="ui grid">
        <div class="three column row">
            <div class="left floated column">
                {{ $quotedetails->appends(collect($_GET)->reject(function ($name) {
                    return empty($name);
                })->all())->links() }}
            </div>
            <div class="column">
                @if($quotedetails->count())
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing {{ $quotedetails->count() }} of {{ $quotedetails->total() }} Results
                    </h4>
                @else
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing 0 Results
                    </h4>
                @endif
            </div>
            <div class="right floated right aligned column">
                @if($quotedetails->count())
                    <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                        @foreach ($_GET as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                        @endforeach
                        <div class="ui action">
                            <input type="hidden" name="export" value="xls"/>
                            <button class="ui teal right labeled icon button">
                                <i class="download icon"></i>
                                Export to XLS
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection


@push('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" href="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.css">
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/moment-with-locales.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
    <script type="text/javascript" src="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.js"></script>

    <script type="text/javascript">
    $(function () {
        $('#datetimepicker1').calendar({
            type: 'date',
            endCalendar: $('#datetimepicker2')
        });
        $('#datetimepicker2').calendar({
            type: 'date',
            startCalendar: $('#datetimepicker1')
        });

        $('#datetimepicker3').calendar({
            type: 'date',
            endCalendar: $('#datetimepicker4')
        });
        $('#datetimepicker4').calendar({
            type: 'date',
            startCalendar: $('#datetimepicker3')
        });

    });
    </script>
@endpush
