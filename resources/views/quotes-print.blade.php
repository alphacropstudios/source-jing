<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Source.com.ph | Quote Print</title>
    <style>

    @media print {
        body{
            direction: ltr;
            font-size: 12px;
        }
        table.table {
            table-layout: fixed;
            border-collapse: collapse;
            border: 1px solid #aaa;
            width: 100%;
            margin-bottom: 20px;
        }
        .table td,th{
            border-top: 1px solid #aaa;
            padding: 10px;
            text-align: left;
            vertical-align: top;
            font-family: sans-serif;
        }
        .table th{
            font-weight: bold;
        }
        .table td:first-child{
            border-left: 1px solid #aaa;
        }
        .table td:last-child{
            border-right: 1px solid #aaa;
        }

    }

    </style>
</head>
<body >
    <h2 style="text-align:center;">Quote {{ empty($quote['id']) ? '' : '#' . str_pad($quote['id'], 6, "0", STR_PAD_LEFT) }} </h2>
    <table class="table">
        <thead>
            <tr>
                <th colspan="2"> Quote Details </th>
                <th colspan="2"> Customer Details </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td> Title </td><td>{{ $quote['title'] }}</td>
                <td> Company </td><td>{{ $customer['name'] }}</td>
            </tr>
            <tr>
                <td> Delivery Date </td><td>{{ $quote['date'] }}</td>
                <td> Address </td><td>{{ $customer['address'] }}</td>
            </tr>
            <tr>
                <td rowspan="2"> Instructions </td><td rowspan="2">{{ $quote['instructions'] }}</td>
                <td> Contact </td><td>{{ $customer['contact'] }}</td>
            </tr>
            <tr>
                <td> Email </td><td>{{ $customer['email'] }}</td>
            </tr>
        </tbody>
    </table>
    <table class="table">
        <thead>
            <tr>
                <th colspan="6"> Products</th>
            </tr>
            <tr>
                <th> Item </th>
                <th> Description </th>
                <th> Quantity </th>
                <th> Unit Price </th>
                <th> Total Price </th>
                <th> Supplier </th>
            </tr>
        </thead>
        <tbody>
            @if(count($cartdata))
                @foreach ($cartdata as $k => $cart)
                    <tr>
                        @foreach ($cart as $kk => $value)
                            <td>
                                {{ $value }}
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6">
                        No items
                    </td>
                </tr>
            @endif
        </tbody>
        <tfoot>
            <tr>
                <th> {{ $carttotal['item'] }} </th>
                <th>  </th>
                <th> {{ $carttotal['qty'] }} </th>
                <th>  </th>
                <th> {{ $carttotal['price'] }} </th>
                <th>  </th>
            </tr>
        </tfoot>
    </table>

</body>
</html>
