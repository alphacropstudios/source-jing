@extends('layouts.semantic')

@section('title')
    Product
@endsection

@section('description')
    Search and Manage Products
@endsection

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="ui form" method="GET" action="{{ route('product') }}">
        <div class="ui grid fields">
            <div class="four wide field">
                <label>Main</label>
                <select class="ui dropdown" name="main" >
                    <option>All </option>
                    @foreach ($categories['1'] as $k => $category)
                        <option {{ request('main') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>General</label>
                <select class="ui dropdown" name="general" >
                    <option>All </option>
                    @foreach ($categories['2'] as $k => $category)
                        <option {{ request('general') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Brand</label>
                <select class="ui dropdown" name="brand" >
                    <option>All </option>
                    @foreach ($categories['3'] as $k => $category)
                        <option {{ request('brand') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">

            </div>
            <div class="four wide field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>
            <div class="four wide field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'name' => 'Name',
                        'main' => 'Main Class',
                        'general' => 'General Class',
                        'brand' => 'Brand Class',
                        'srp' => 'SRP',
                        'qty' => 'WholeSale Quantity',
                        'status' => 'Status',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Sort Type</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'asc' => 'Ascending',
                        'desc' => 'Descending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>&nbsp;</label>
                <div class="ui action input">
                    <input type="text" placeholder="Type a keyword.." name="q" value="{{ request('q') }}">
                    <button class="ui button">Search</button>
                </div>
            </div>
        </div>

        @if(request('search'))
            <input type="hidden" name="search" value="{{ request('search') }}"/>
        @endif

        @if(request('redirect'))
            <input type="hidden" name="redirect" value="{{ request('redirect') }}"/>
        @endif
    </form>

    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">

            <thead>
                <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Main</th>
                    <th>General</th>
                    <th>Brand</th>
                    <th>SRP</th>
                    <th>Wholesale Quantity</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if($products->count() > 0)
                    @foreach ($products as $key => $product)
                        <tr>
                            <td>
                                @if(request('search') && request('search')=='select')
                                    <a class="ui primary icon tiny button" href="{{ route('product.create',[
                                        'replicate' => $product->id,
                                        'main' => $product->category->parent->parent->name,
                                        'general' => $product->category->parent->name,
                                        'brand' => $product->category->name,
                                        'q' => request('q')
                                        ]) }}">
                                        Select
                                    </a>
                                @elseif(request('search') && request('search')=='selectredirect')
                                    @php
                                    $url = parse_url(urldecode(request('redirect')));
                                    $params = [];
                                    if(isset($url['query'])){
                                        parse_str($url['query'],$params);
                                    }
                                    $params['product'] = $product->id;
                                    $params['main'] = $product->category->parent->parent->name;
                                    $params['general'] = $product->category->parent->name;
                                    $params['brand'] = $product->category->name;
                                    $url = url($url['path']) . '?' . http_build_query($params);
                                    @endphp
                                    <a class="ui primary icon tiny button" href="{{ $url }}">
                                        Select
                                    </a>
                                @else
                                    <label>
                                        <input type="checkbox" name="deleteIds" value="{{ $product->id }}">
                                    </label>&nbsp;&nbsp;&nbsp;
                                    <a href="{{ route('product.edit',[
                                        'product' => $product->id,'q' => request('q')
                                        ]) }}">
                                        Edit
                                    </a> |
                                    <a href="{{ route('product.prices',['product' => $product->id ]) }}">Price</a> |
                                    <a href="{{ route('product.images',['product' => $product->id ]) }}">Image</a> |
                                    <a href="{{ route('product.destroy',['product' => $product->id ]) }}">Delete</a>
                                @endif
                            </td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->category->parent->parent->name }}</td>
                            <td>{{ $product->category->parent->name }}</td>
                            <td>{{ $product->category->name }}</td>
                            <td>{{ formatNumber($product->srp) }}</td>
                            <td>{{ formatNumber($product->wholesalequantity,0) }}</td>
                            <td>{{ $product->enable_flag == 'y' ? 'Active' : 'Inactive' }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8" class="text-center">No Results found</td>
                    </tr>
                @endif
            </tbody>
            <tfoot class="full-width">
                <tr>
                    <th colspan="8">
                        <a class="ui primary labeled icon small button" href="{{ route('product.create') }}"><i class="plus icon"></i>Add</a>
                        <a class="ui red labeled icon small button" href="#" id="deleteProducts"><i class="trash icon"></i>Delete</a>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="ui horizontal divider"> </div>

    <div class="ui grid">
        <div class="three column row">
            <div class="left floated column">
                {{ $products->appends(collect($_GET)->reject(function ($name) {
                    return empty($name);
                })->all())->links() }}
            </div>
            <div class="column">
                @if($products->count())
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing {{ $products->count() }} of {{ $products->total() }} Results
                    </h4>
                @else
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing 0 Results
                    </h4>
                @endif
            </div>
            <div class="right floated right aligned column">
                @if($products->count())
                    <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                        @foreach ($_GET as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                        @endforeach
                        <div class="ui action">
                            <input type="hidden" name="export" value="xls"/>
                            <button class="ui teal right labeled icon button">
                                <i class="download icon"></i>
                                Export to XLS
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script>
    jQuery(function(){
        $('#deleteProducts').click(function(e){
            e.preventDefault();
            var ids = [];
            $('[name="deleteIds"]:checked').each(function(){
                ids.push($(this).val());
            });

            if(ids.length){
                window.location.assign('{{ route('product.destroyBatch') }}?ids=' + ids.join(','));
            }
        });
    });
    </script>
@endpush
