@extends('layouts.semantic')

@section('title')
    Add Category
@endsection

@section('description')

@endsection

@push('menubar')
    <a class="item" href="{{ route('category') }}">
        <i class="chevron left icon"></i> Back to Categories
    </a>
@endpush

@section('content')

    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" method="POST" action="{{ route('category.store') }}">
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">

                <div class="field">
                    <label>Category Name</label>
                    <div class="ui input">
                        <input type="text" name="name" value="{{ old('name', request('name')) }}">
                    </div>
                </div>
                <div class="field">
                    <label>Description</label>
                    <div class="ui input">
                        <textarea name="description" rows="2">{{ old('description', request('description')) }}</textarea>
                    </div>
                </div>
                <div class="field">
                    <label>Level</label>
                    <select class="ui dropdown" name="level" >
                        @foreach ([
                            'Main','General','Brand'
                            ] as $k => $category)
                            <option {{ strtolower(request('level')) == strtolower($category) ? 'selected' : '' }} value="{{ strtolower($category) }}">{{ $category }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="field">
                    <label>Parent</label>
                    <select class="ui dropdown" name="parent" >
                        @if($parents->count() == 0 )
                            <option value="0">None</option>
                        @else
                            @foreach ($parents as $k => $category)
                                <option {{ strtolower(request('parent')) == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="field">
                    <label>Active</label>
                    <select class="ui dropdown" name="enable_flag">
                        @foreach ([
                            'y' => 'Yes',
                            'n' => 'No',
                            ] as $key => $value)
                            <option {{ request('enable_flag') == $key[0] ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Save</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>

    </form>

@endsection

@push('scripts')
    <script type="text/javascript">
    $(function () {
        $('[name="level"]').on('change',function(){

            var qry = [];
            qry.push('level=' + $(this).val());

            var fields = ['name','description','parent'];
            for(var i in fields){
                var fieldvalue = $('[name="' + fields[i] + '"]').val();
                if(fieldvalue && fieldvalue.length){
                    qry.push( fields[i] + '=' + fieldvalue);
                }
            }

            <?php
            $url = route('category.create');
            ?>
            window.location.assign('{!! $url !!}?' + qry.join('&'));

        });

        $('[name="parent"]').on('change',function(){

            var qry = [];

            qry.push('parent=' + $(this).val());

            var fields = ['level','name','description'];
            for(var i in fields){
                var fieldvalue = $('[name="' + fields[i] + '"]').val();
                if(fieldvalue && fieldvalue.length){
                    qry.push( fields[i] + '=' + fieldvalue);
                }
            }

            <?php
            $url = route('category.create');
            ?>
            window.location.assign('{!! $url !!}?' + qry.join('&'));

        });
    });
    </script>
@endpush
