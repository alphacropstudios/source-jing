@extends('layouts.semantic')

@section('title')
    Add Company
@endsection

@section('description')

@endsection

@push('menubar')
    <a class="item" href="{{ route('company') }}">
        <i class="chevron left icon"></i> Back to Companies
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form company-update" action="{{ route('company.update',['company' => $company->id ]) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">
                <div class="field required">
                    <label>Username</label>
                    <div class="ui input">
                        <input type="text" name="username" value="{{ $company->account ? $company->account->username : '' }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Company Name</label>
                    <div class="ui input">
                        <input type="text" name="companyname" value="{{ $company->name }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Address: Unit # / Bldg / House # / Street / Barangay</label>
                    <div class="ui input">
                        <textarea rows="2" name="address" required>{{ $company->address && $company->address->street }}</textarea>
                    </div>

                </div>
                <div class="field">
                    <label>City</label>
                    <select class="ui dropdown" name="city" >
                        @foreach ($city as $key => $c)
                            <option {{ $company->address && $company->address->city == $c->name ? 'selected' : '' }}>{{ $c->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <label>Province</label>
                    <select class="ui dropdown" name="state" >
                        @foreach ($state as $key => $s)
                            <option {{ $company->address && $company->address->state == $s->name ? 'selected' : '' }}>{{ $s->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <label>Country</label>
                    <select class="ui dropdown" name="country" >
                        @foreach ($country as $key => $c)
                            <option {{ $company->address && $company->address->country_id == $c['id'] ? 'selected' : '' }} value="{{ $c['id'] }}">{{ $c['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field required">
                    <label>Zipcode</label>
                    <div class="ui input">
                        <input type="text" name="zipcode" value="{{ $company->address && $company->address->zipcode }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Primary Contact Name</label>
                    <div class="ui input">
                        <input type="text" name="contact" value="{{ $company->contact && $company->contact->name }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Contact Number</label>
                    <div class="ui input">
                        <input type="text" name="contact_number" value="{{ $company->contact && $company->contact->details()->where('contacttype_id',2)->first()->name ?: '-' }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Contact Email</label>
                    <div class="ui input">
                        <input type="text" name="email" value="{{ $company->contact && $company->contact->details()->where('contacttype_id',1)->first()->name ?: '-' }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Contact Designation</label>
                    <div class="ui input">
                        <input type="text" name="designation" value="{{ $company->contact && $company->contact->designation }}" required>
                    </div>
                </div>
                <div class="field">
                    <label>Supplier Account?</label>
                    <select class="ui dropdown" name="supplier" >
                        <option value="y" {{ $company->supplier_flag == 'y' ? 'selected' : ''}}>Yes</option>
                        <option value="n" {{ $company->supplier_flag == 'n' ? 'selected' : ''}}>No</option>
                    </select>
                </div>
                <div class="field required">
                    <label>VAT</label>
                    <div class="ui input">
                        <input type="text" name="vat" value="{{ $company->vat }}" >
                    </div>
                </div>
                <div class="field required">
                    <label>Type of Business</label>
                    <select class="ui dropdown" name="businesstype" >
                        @foreach ($businesstypes as $key => $c)
                            <option {{ $company->businesstype_id == $c['id'] ? 'selected' : '' }} value="{{ $c['id'] }}">{{ $c['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field required">
                    <label>Business Registration</label>
                    <div class="ui input">
                        <input type="text" name="registration" value="{{ $company->businessregistration }}" >
                    </div>
                </div>
                <div class="field">
                    <label>Upload Photo of Business Location</label>
                    <div class="ui input">
                        <input type="file" name="upload" class="ui teal right labeled icon button maxfilesize" >
                    </div>
                    <small>Maximum of 100MB only (jpeg/gif/png)</small>
                </div>
                @if($company->image)
                    <img src="{{ $company->image->getImageURI() }}" class="ui medium rounded image"/>
                @endif
                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Update</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>
@endsection
@push('scripts')

@endpush
