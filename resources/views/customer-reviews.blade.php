@extends('layouts.semantic')

@section('title')
    Customer Reviews
@endsection

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="ui form" action="{{ route('customer.reviews') }}" method="get">
        <div class="ui four fields">
            <div class="field">
                <label>From Date</label>
                <div class="ui calendar" id="datetimepicker1">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="datefrom" value="{{ request('datefrom') }}">
                    </div>
                </div>
            </div>
            <div class="field">
                <label>To Date</label>
                <div class="ui calendar" id="datetimepicker2">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="dateto" value="{{ request('dateto') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="ui four fields">
            <div class="field">
                <label>Company</label>
                <select class="ui dropdown" name="company" >
                    <option>All </option>
                    @foreach ($companies as $k => $company)
                        <option {{ request('company') == $company->id ? 'selected' : '' }} value="{{ $company->id }}">{{ $company->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Rating</label>
                <select class="ui dropdown" name="rating">
                    <option>All </option>
                    @foreach ([
                        '5' => '5 Stars',
                        '4' => '4 Stars',
                        '3' => '3 Stars',
                        '2' => '2 Stars',
                        '1' => '1 Star',
                        ] as $key => $value)
                        <option {{ request('rating') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Rated By</label>
                <select class="ui dropdown" name="ratedby" >
                    <option>All </option>
                    @foreach ($users as $k => $user)
                        <option {{ request('ratedby') == $user->id ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="ui four fields">
            <div class="field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>

            <div class="field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'date' => 'Date',
                        'company' => 'Company',
                        'rating' => 'Rating',
                        'ratedby' => 'Rated By',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Sort by</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'desc' => 'Descending',
                        'asc' => 'Ascending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="field">
                <label>&nbsp;</label>
                <div class="ui action input">
                    <input type="text" placeholder="Type a keyword.." name="q" value="{{ request('q') }}">
                    <button class="ui button">Search</button>
                </div>
            </div>
        </div>
    </form>

    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>ID</th>
                    <th>Company</th>
                    <th>Rating</th>
                    <th>Remark</th>
                    <th>Date</th>
                    <th>Rated By</th>
                </tr>
            </thead>
            <tbody>

                @if($reviews->count() > 0 )
                    @foreach ($reviews as $key => $review)
                        <tr>
                            <td>
                                <label><input name="deleteIds" value="{{ $review->id }}" type="checkbox"></label>&nbsp;&nbsp;&nbsp;
                                <a href="{{ route('customer.review.destroy',['review'=>$review->id]) }}" class="btn-delete-review">
                                    Delete
                                </a>
                            </td>
                            <td>{{ str_pad($review->id, 6, "0", STR_PAD_LEFT) }}</td>
                            <td>{{ $review->company->name }}</td>
                            <td><div class="ui rating" data-rating="{{ $review->rating }}" data-max-rating="5"></div></td>
                            <td>{{ str_limit($review->remark,100) }}</td>
                            <td>{{ $review->date->timestamp <= 0 ? '-' : $review->date->format('M d, Y') }}</td>
                            <td>{{ $review->account->office->name }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center" colspan="7">No Reviews yet</td>
                        </tr>
                    @endif
                </tbody>
                <tfoot class="full-width">
                    <tr>
                        <th colspan="7">
                            <a class="ui red labeled icon small button" href="#" id="deleteReviews"><i class="trash icon"></i>Delete</a>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="ui horizontal divider"> </div>

        <div class="ui grid">
            <div class="three column row">
                <div class="left floated column">
                    {{ $reviews->appends(collect($_GET)->reject(function ($name) {
                        return empty($name);
                    })->all())->links() }}
                </div>
                <div class="column">
                    @if($reviews->count())
                        <h4 class="ui horizontal divider header">
                            <i class="tag icon"></i>
                            Showing {{ $reviews->count() }} of {{ $reviews->total() }} Results
                        </h4>
                    @else
                        <h4 class="ui horizontal divider header">
                            <i class="tag icon"></i>
                            Showing 0 Results
                        </h4>
                    @endif
                </div>
                <div class="right floated right aligned column">
                    @if($reviews->count())
                        <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                            @foreach ($_GET as $key => $value)
                                <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                            @endforeach
                            <div class="ui action">
                                <input type="hidden" name="export" value="xls"/>
                                <button class="ui teal right labeled icon button">
                                    <i class="download icon"></i>
                                    Export to XLS
                                </button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>


    @endsection
    @push('styles')
        <link rel="stylesheet" href="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.css">
    @endpush
    @push('scripts')
        <script src="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.js"></script>
        <script>
        jQuery(function(){
            $('#datetimepicker1').calendar({
                type: 'date',
                endCalendar: $('#datetimepicker2')
            });
            $('#datetimepicker2').calendar({
                type: 'date',
                startCalendar: $('#datetimepicker1')
            });

            $('.ui.rating').rating({
                interactive : false
            });

            $('#deleteReviews').click(function(e){
                e.preventDefault();
                var ids = [];
                $('[name="deleteIds"]:checked').each(function(){
                    ids.push($(this).val());
                });

                if(ids.length){
                    window.location.assign('{{ route('customer.review.destroyBatch') }}?ids=' + ids.join(','));
                }
            });
        });</script>
    @endpush
