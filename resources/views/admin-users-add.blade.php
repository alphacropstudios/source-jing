@extends('layouts.semantic')

@section('title')
    Add Admin User
@endsection

@push('menubar')
    <a class="item" href="{{ route('admin.users') }}">
        <i class="chevron left icon"></i> Back to Admin Users
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" action="{{ route('admin.users.create') }}" method="POST">
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">
                <div class="field required">
                    <label>Username</label>
                    <div class="ui input">
                        <input type="text" name="username" value="{{ old('username') }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Email</label>
                    <div class="ui input">
                        <input type="text" name="email" value="{{ old('email') }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Name</label>
                    <div class="ui input">
                        <input type="text" name="name" value="{{ old('name') }}" required>
                    </div>
                </div>


                <div class="field required">
                    <label>Type</label>
                    <select class="ui dropdown" name="type" required >
                        @foreach ($types as $key => $value)
                            <option {{ old('type') == $key ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="field required">
                    <label>Password</label>
                    <div class="ui input">
                        <input type="password" name="password" value="" required>
                    </div>
                </div>

                <div class="field required">
                    <label>Confirm</label>
                    <div class="ui input">
                        <input type="password" name="password_confirmation" value="" required>
                    </div>
                </div>

                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Add</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>
@endsection
