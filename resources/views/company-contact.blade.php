@extends('layouts.semantic')

@section('title')
    Company Contacts
@endsection

@section('description')
    {{ $company->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('company') }}">
        <i class="chevron left icon"></i> Back to Companies
    </a>
@endpush

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="ui form" action="{{ route('company.contacts',['company' => $company->id ]) }}" method="get">
        <div class="ui four fields">
            <div class="field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>
            <div class="field">
                <label>Designation</label>
                <select class="ui dropdown" name="designation">
                    @foreach ($designations as $key => $value)
                        <option {{ request('designation') == $value['designation'] ? 'selected' : '' }} value="{{ $value['designation'] }}">
                            {{ $value['designation'] }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'name' => 'Name',
                        'designation' => 'Designation',
                        'phone' => 'Phone Number',
                        'email' => 'Email',
                        'primary' => 'Primary Contact',
                        'status' => 'Status',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'asc' => 'Ascending',
                        'desc' => 'Descending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="field">
                <label>&nbsp;</label>
                <div class="ui action ">
                    <button class="ui button" type="submit">Search</button>
                </div>
            </div>
        </div>
    </form>

    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                    <th>Primary Contact</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if($contacts->count() > 0 )
                    @foreach ($contacts as $key => $contact)
                        <tr>
                            <td>
                                <label><input name="deleteIds" value="{{ $contact->id }}" type="checkbox"></label>&nbsp;&nbsp;&nbsp;
                                <a href="{{ route('company.contactedit',['company'=> $company->id , 'contact' => $contact->id ]) }}">Edit</a> |
                                <a href="{{ route('company.contactdelete',['company'=> $company->id , 'contact' => $contact->id ]) }}">Delete</a>
                            </td>
                            <td>{{ $contact->name }}</td>
                            <td>{{ $contact->designation }}</td>
                            <td>{{ $contact->details()->where('contacttype_id',2)->first()->name ?: '-' }}</td>
                            <td>{{ $contact->details()->where('contacttype_id',1)->first()->name ?: '-' }}</td>
                            <td>{{ $contact->primary_flag =='y' ? 'Yes' : 'No'  }}</td>
                            <td>{{ $contact->enable_flag=='y' ? 'Active' : 'Inactive' }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="8">No Contacts yet</td>
                    </tr>
                @endif
            </tbody>
            <tfoot class="full-width">
                <tr>
                    <th colspan="8">
                        <div class="ui tiny buttons">
                            <a class="ui primary labeled icon button" href="{{ route('company.contactadd',['company'=>$company->id]) }}"><i class="plus icon"></i>Add</a>
                            <a class="ui right labeled icon button" href="#" id="deleteContacts">Delete<i class="trash icon"></i></a>
                        </div>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>

    <div class="ui horizontal divider"> </div>

    <div class="ui grid">
        <div class="three column row">
            <div class="left floated column">
                {{ $contacts->appends(collect($_GET)->reject(function ($name) {
                    return empty($name);
                })->all())->links() }}
            </div>
            <div class="column">
                @if($contacts->count())
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing {{ $contacts->count() }} of {{ $contacts->total() }} Results
                    </h4>
                @else
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing 0 Results
                    </h4>
                @endif
            </div>
            <div class="right floated right aligned column">
                @if($contacts->count())
                    <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                        @foreach ($_GET as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                        @endforeach
                        <div class="ui action">
                            <input type="hidden" name="export" value="xls"/>
                            <button class="ui teal right labeled icon button">
                                <i class="download icon"></i>
                                Export to XLS
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>


@endsection

@push('scripts')

    <script type="text/javascript">
    $(function () {

        $('#deleteContacts').click(function(e){
            e.preventDefault();
            var ids = [];
            $('[name="deleteIds"]:checked').each(function(){
                ids.push($(this).val());
            });

            if(ids.length){
                window.location.assign('{{ route('company.contactdeletebatch',['company' => $company->id ]) }}?ids=' + ids.join(','));
            }
        });

    });
    </script>
@endpush
