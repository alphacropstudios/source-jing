@extends('layouts.semantic')

@section('title')
    Add Custom Item
@endsection

@push('menubar')
    <a class="item" href="{{ request('redirect',route('quote.regular.create')) }}">
        <i class="chevron left icon"></i> Back to Quotation
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" action="{{ route('quote.regular.add.custom.item.store') }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="redirect" value="{{ urlencode(request('redirect')) }}"/>
        @if(request('tmp'))
            <input type="hidden" name="tmp" value="true"/>
        @endif
        @if(request('quote'))
            <input type="hidden" name="quote" value="{{ request('quote') }}"/>
        @endif
        <div class="three column row">
            <div class="column">
                <div class="field required">
                    <label>Main: </label>
                    <select class="ui dropdown" name="main" required>
                        <option>All</option>
                        @foreach ($categories['1'] as $k => $category)
                            <option {{ request('main') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field required">
                    <label>General: </label>
                    <select class="ui dropdown" name="general" required>
                        @if($categories['2']->count() > 0)
                            @foreach ($categories['2'] as $k => $category)
                                <option {{ request('general') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        @else
                            <option selected disabled>None</option>
                        @endif;
                    </select>
                </div>
                <div class="field required">
                    <label>Custom Name: </label>
                    <div class="ui input">
                        <input type="text" name="name" value="{{ old('name',request('name')) }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Quantity: </label>
                    <div class="ui input">
                        <input type="number" name="qty" value="{{ old('name',request('qty')) }}" required>
                    </div>
                </div>

                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Submit</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script type="text/javascript">
    $(function () {

        $('[name="main"]').on('change',function(){
            var qry = [];

            qry.push('main=' + $(this).val());

            var fields = ['redirect','name','qty','tmp','quote'];
            for(var i in fields){
                var fieldvalue = $('[name="' + fields[i] + '"]').val();
                if(fieldvalue && fieldvalue.length){
                    qry.push( fields[i] + '=' + fieldvalue);
                }
            }

            <?php
            $url = route('quote.regular.add.custom.item');
            ?>
            window.location.assign('{!! $url !!}?' + qry.join('&'));
        });
        $('[name="general"]').on('change',function(){

            var qry = [];

            qry.push('general=' + $(this).val());

            var fields = ['redirect','main','name','qty','tmp','quote'];
            for(var i in fields){
                var fieldvalue = $('[name="' + fields[i] + '"]').val();
                if(fieldvalue && fieldvalue.length){
                    qry.push( fields[i] + '=' + fieldvalue);
                }
            }

            <?php
            $url = route('quote.regular.add.custom.item');
            ?>
            window.location.assign('{!! $url !!}?' + qry.join('&'));
        });
    });
    </script>
@endpush
