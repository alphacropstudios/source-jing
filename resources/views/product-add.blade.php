@extends('layouts.semantic')

@section('title')
    Add Product
@endsection

@section('description')

@endsection

@push('menubar')
    <a class="item" href="{{ route('product') }}">
        <i class="chevron left icon"></i> Back to Products
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" method="GET" action="{{ route('product') }}">
        <div class="three column row">
            <div class="column">
                <div class="field">
                    <label>Search Existing Product</label>
                    <div class="ui icon input">
                        <input type="hidden" name="search" value="select"/>
                        <input type="text" name="q" placeholder="" value="{{ old('q') }}">
                        <i class="search link icon"></i>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="ui horizontal divider"> </div>

    <form class="ui grid form" method="POST" action="{{ route('product.store') }}"  enctype='multipart/form-data'>
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">

                <div class="field required">
                    <label>Main: </label>
                    <select class="ui dropdown" name="main" >
                        <option>All </option>
                        @foreach ($categories['1'] as $k => $category)
                            <option {{ request('main') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field required">
                    <label>General: </label>
                    <select class="ui dropdown" name="general" >
                        @if($categories['2']->count() > 0)
                            <option>All </option>
                            @foreach ($categories['2'] as $k => $category)
                                <option {{ request('general') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        @else
                            <option selected disabled>None</option>
                        @endif;
                    </select>
                </div>
                <div class="field required">
                    <label>Brand: </label>
                    <select class="ui dropdown" name="brand" required>
                        @if($categories['2']->count() > 0 && $categories['3']->count() > 0)
                            @foreach ($categories['3'] as $k => $category)
                                <option {{ request('brand') == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        @else
                            <option selected disabled>None</option>
                        @endif;
                    </select>
                </div>
                <div class="field required">
                    <label>Description</label>
                    <div class="ui input">
                        <input type="text" name="description" value="{{ old('description', request('description')) }}" required>
                    </div>
                </div>

                <div class="field required">
                    <label>SRP</label>
                    <div class="ui input">
                        <input type="number" name="srp" step="any" min="0" value="{{ old('srp', request('srp')) }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Wholesale Qty</label>
                    <div class="ui input">
                        <input type="number" name="wholesalequantity" min="0" value="{{ old('wholesalequantity', request('wholesalequantity')) }}" required>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="image" tabindex="0" class="hidden" value="upload">
                        <label>Upload Product Image</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="file" name="upload" class="ui teal right labeled icon button">
                    </div>
                </div>
                <div class="field">
                    <label>File Repository</label>
                    <select class="ui dropdown" name="level" >
                        @foreach ([
                            'Image Path','Other Path'
                            ] as $k => $path)
                            <option>{{ $path }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="description" placeholder="/images">
                    </div>
                </div>

                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="image" class="hidden" value="url">
                        <label>Use Image from URL</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="imageurl"  value="{{ old('imageurl', request('imageurl')) }}">
                    </div>
                </div>
                @if(request()->has('replicate') && old('current_image', request('current_image')))
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" name="image" class="hidden" value="current" checked>
                            <label>Use Current Image</label>
                        </div>
                    </div>
                    <img src="{{ old('current_image', request('current_image')) }}" class="img-responsive">
                    <div class="field">
                        <div class="ui input">
                            <input type="hidden" name="current_image_id" value="{{ old('current_image_id', request('current_image_id')) }}"/>
                            <input type="text" name="current_image"  value="{{ old('current_image', request('current_image')) }}" readonly>
                        </div>
                    </div>
                @endif
                <div class="field">
                    <label>Active</label>
                    <select class="ui dropdown" name="enable_flag">
                        @foreach ([
                            'y' => 'Yes',
                            'n' => 'No',
                            ] as $key => $value)
                            <option {{ request('enable_flag') == $key[0] ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="field">
                    <div class="ui checkbox">
                        <input type="checkbox" name="remember" value="remember" {{ old('remember')=='remember' ? 'checked' : '' }} >
                        <label>Remember Form Entries after Submission</label>
                    </div>
                </div>

                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Add</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>

        </div>
    </form>
    <div class="ui grid">
        <div class="column">
            @if(session('added.product'))
                <table class="ui compact small table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Wholesale</th>
                            <th>SRP</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (session('added.product') as $key => $product)
                            <tr>
                                <td>{{ $product['name'] }}</td>
                                <td>{{ $product['wholesalequantity'] }}</td>
                                <td>{{ $product['srp'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
    $(function () {

        $('[name="main"]').on('change',function(){
            var qry = [];

            qry.push('main=' + $(this).val());

            var fields = ['description','srp','wholesalequantity','image'];
            for(var i in fields){
                var fieldvalue = $('[name="' + fields[i] + '"]').val();
                if(fieldvalue && fieldvalue.length){
                    qry.push( fields[i] + '=' + fieldvalue);
                }
            }

            <?php
            $url = route('product.create');
            ?>
            window.location.assign('{!! $url !!}?' + qry.join('&'));
        });
        $('[name="general"]').on('change',function(){

            var qry = [];

            qry.push('general=' + $(this).val());

            var fields = ['description','srp','wholesalequantity','image'];
            for(var i in fields){
                var fieldvalue = $('[name="' + fields[i] + '"]').val();
                if(fieldvalue && fieldvalue.length){
                    qry.push( fields[i] + '=' + fieldvalue);
                }
            }

            <?php
            $url = route('product.create');
            ?>
            window.location.assign('{!! $url !!}?' + qry.join('&'));
        });
    });
    </script>
@endpush
