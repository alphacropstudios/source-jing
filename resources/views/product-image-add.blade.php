@extends('layouts.semantic')

@section('title')
    Add Product Image
@endsection

@section('description')
    {{ $product->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('product.images',['product' => $product->id ]) }}">
        <i class="chevron left icon"></i> Back to Product Images
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" action="{{ route('product.imagecreate',['product' => $product->id ,'image' => $image->id ]) }}" method="POST" enctype='multipart/form-data'>
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">

                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="image" tabindex="0" class="hidden" value="upload" checked>
                        <label>Upload Product Image</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="file" name="upload" class="ui teal right labeled icon button">
                    </div>
                </div>
                <div class="field">
                    <label>File Repository</label>
                    <select class="ui dropdown" name="level" >
                        @foreach ([
                            'Image Path','Other Path'
                            ] as $k => $path)
                            <option>{{ $path }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="description" placeholder="/images">
                    </div>
                </div>

                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="image" class="hidden" value="url">
                        <label>Use Image from URL</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="imageurl"  value="{{ old('imageurl') }}">
                    </div>
                </div>

                <div class="field">
                    <label>Primary Image</label>
                    <select class="ui dropdown" name="isPrimary">
                        @foreach ([
                            'yes' => 'Yes',
                            'no' => 'No',
                            ] as $key => $value)
                            <option {{ request('isPrimary') == $value ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <label>Image Type</label>
                    <select class="ui dropdown" name="imagetype">
                        @foreach ([
                            'primary' => 'Primary',
                            'thumbnail' => 'Thumbnail',
                            'regular' => 'Regular',
                            'medium' => 'Medium',
                            'large' => 'Large',
                            'extra-large' => 'Extra Large',
                            ] as $key => $value)
                            <option {{ request('imagetype') == $value ? 'selected' : '' }} value="{{ $value }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <label>Active</label>
                    <select class="ui dropdown" name="enable_flag">
                        @foreach ([
                            'y' => 'Yes',
                            'n' => 'No',
                            ] as $key => $value)
                            <option {{ request('enable_flag') == $key[0] ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Add</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>

@endsection
