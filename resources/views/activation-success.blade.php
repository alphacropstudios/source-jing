<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Source.com.ph | @yield('title','Admin Log In')</title>
    <link rel="stylesheet" href="{{ asset('semantic/dist/semantic.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <style type="text/css">
    body {
        background-color: #111;
    }
    body > .grid {
        height: 100%;
        z-index: 2;
        position: relative;
    }
    .image {
        margin-top: -100px;
    }
    .column {
        max-width: 450px;
    }
    #particles-js {
      position: absolute;
      width: 100vw;
      height: 100vh;
      z-index: 1;
      display: block;
    }
    #particles-js canvas {
      width: 100vw!important;
      height: 100vh!important;
    }
    </style>

</head>
<body>
    <div id="particles-js"></div>
    <div class="ui middle aligned center aligned grid">
        <div class="column">
            <h2 class="ui grey image header">
                <div class="content">
                    Activate Account
                </div>
            </h2>

            @if (session('status'))
                <div class="ui success message">
                    <i class="close icon"></i>
                    <div class="header">
                        All good!
                    </div>
                    <p>{{ session('status') }}</p>
                </div>
            @endif

            @if (session('message'))
                <div class="ui success message">
                    <i class="close icon"></i>
                    <div class="header">
                        Whoops!
                    </div>
                    <p>{{ session('message') }}</p>
                </div>
            @endif
            <form class="ui large form " method="GET" action="{{ route('login') }}">
                <div class="ui stacked segment">
                    <button class="ui fluid large black submit button" type="submit">Click here to Login</button>
                </div>
            </form>
        </div>
    </div>

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('semantic/dist/semantic.js') }}"></script>
    <script>

    $('.ui.sidebar')
    .sidebar({
        context: $('.bottom.attached')
    })
    .sidebar('attach events', '.menu .item.toggler');

    $('.message .close').on('click', function() {
        $(this)
        .closest('.message')
        .transition('fade');
    });

    $('.ui.dropdown').dropdown();
    $('.ui.radio.checkbox').checkbox();

    $("form").submit(function() {
        $(this).addClass('loading');
        $(this).find(":input").filter(function(){ return !this.value; }).attr("disabled", "disabled");
        return true;
    });

    $( "form" ).find( ":input" ).prop( "disabled", false );

    </script>

    <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script>
    if($('#particles-js').length > 0) {
        particlesJS("particles-js", {
            "particles": {
                "number": {
                    "value": 80,
                    "density": {
                        "enable": true,
                        "value_area": 800
                    }
                },
                "color": {
                    "value": "#eee"
                },
                "shape": {
                    "type": "circle",
                    "stroke": {
                        "width": 0,
                        "color": "#000000"
                    },
                    "polygon": {
                        "nb_sides": 5
                    },
                    "image": {
                        "src": "img/github.svg",
                        "width": 100,
                        "height": 100
                    }
                },
                "opacity": {
                    "value": 0.5,
                    "random": false,
                    "anim": {
                        "enable": false,
                        "speed": 1,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                },
                "size": {
                    "value": 3,
                    "random": true,
                    "anim": {
                        "enable": false,
                        "speed": 40,
                        "size_min": 0.1,
                        "sync": false
                    }
                },
                "line_linked": {
                    "enable": true,
                    "distance": 150,
                    "color": "#ffffff",
                    "opacity": 0.4,
                    "width": 1
                },
                "move": {
                    "enable": true,
                    "speed": 3,
                    "direction": "none",
                    "random": true,
                    "straight": false,
                    "out_mode": "out",
                    "bounce": false,
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                }
            },
            "interactivity": {
                "detect_on": "canvas",
                "events": {
                    "onhover": {
                        "enable": true,
                        "mode": "bubble"
                    },
                    "onclick": {
                        "enable": true,
                        "mode": "repulse"
                    },
                    "resize": true
                },
                "modes": {
                    "grab": {
                        "distance": 400,
                        "line_linked": {
                            "opacity": 1
                        }
                    },
                    "bubble": {
                        "distance": 207.079689136843,
                        "size": 7,
                        "duration": 5,
                        "opacity": 0.5,
                        "speed": 3
                    },
                    "repulse": {
                        "distance": 200,
                        "duration": 0.4
                    },
                    "push": {
                        "particles_nb": 4
                    },
                    "remove": {
                        "particles_nb": 2
                    }
                }
            },
            "retina_detect": true
        });
    }
    </script>
</body>
</html>
