@component('mail::message')
# Company Account Activation

Please click the button below to start activating your account!

@component('mail::button', ['url' => $url])
Activate Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
