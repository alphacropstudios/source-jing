@extends('layouts.semantic')

@section('title')
    Edit Product
@endsection

@section('description')
    {{ $product->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('product') }}">
        <i class="chevron left icon"></i> Back to Products
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" method="GET" action="{{ route('product') }}">
        <div class="three column row">
            <div class="column">
                <div class="field">
                    <label>Search Existing Product</label>
                    <div class="ui icon input">
                        <input type="hidden" name="search" value="select"/>
                        <input type="text" name="q" placeholder="" value="{{ old('q') }}">
                        <i class="search link icon"></i>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="ui horizontal divider"> </div>

    <form class="ui grid form" method="POST" action="{{ route('product.update',[ 'product' => $product->id]) }}"  enctype='multipart/form-data'>
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">

                <div class="field required">
                    <label>Main: </label>
                    <select class="ui dropdown" name="main" >
                        <option>All </option>
                        @foreach ($categories['1'] as $k => $category)
                            <option {{ $product->category->parent->parent->name == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field required">
                    <label>General: </label>
                    <select class="ui dropdown" name="general" >
                        @if($categories['2']->count() > 0)
                            <option>All </option>
                            @foreach ($categories['2'] as $k => $category)
                                <option {{ $product->category->parent->name == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        @else
                            <option selected disabled>None</option>
                        @endif;
                    </select>
                </div>
                <div class="field required">
                    <label>Brand: </label>
                    <select class="ui dropdown" name="brand" required>
                        @if($categories['2']->count() > 0 && $categories['3']->count() > 0)
                            @foreach ($categories['3'] as $k => $category)
                                <option {{ $product->category->name == $category->name ? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        @else
                            <option selected disabled>None</option>
                        @endif;
                    </select>
                </div>
                <div class="field required">
                    <label>Description</label>
                    <div class="ui input">
                        <input type="text" name="description" value="{{ $product->name }}" required>
                    </div>
                </div>

                <div class="field required">
                    <label>SRP</label>
                    <div class="ui input">
                        <input type="number" name="srp" step="any" min="0" value="{{ $product->srp }}" required>
                    </div>
                </div>
                <div class="field required">
                    <label>Wholesale Qty</label>
                    <div class="ui input">
                        <input type="number" name="wholesalequantity" min="0" value="{{ $product->wholesalequantity }}" required>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="image" tabindex="0" class="hidden" value="upload">
                        <label>Upload Product Image</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="file" name="upload" class="ui teal right labeled icon button">
                    </div>
                </div>
                <div class="field">
                    <label>File Repository</label>
                    <select class="ui dropdown" name="level" >
                        @foreach ([
                            'Image Path','Other Path'
                            ] as $k => $path)
                            <option>{{ $path }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="description" placeholder="/images">
                    </div>
                </div>

                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="image" class="hidden" value="url">
                        <label>Use Image from URL</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui input">
                        <input type="text" name="imageurl"  value="{{ old('imageurl') }}">
                    </div>
                </div>
                @if($image)
                    <div class="field">
                        <div class="ui radio checkbox">
                            <input type="radio" name="image" class="hidden" value="current" checked>
                            <label>Use Current Image</label>
                        </div>
                    </div>
                    <img src="{{ $image->getImageURI() }}" class="img-responsive">
                    <div class="field">
                        <div class="ui input">
                            <input type="hidden" name="current_image_id" value="{{ $image->id }}"/>
                            <input type="text" name="current_image"  value="{{ $image->getImageURI() }}" readonly>
                        </div>
                    </div>
                @endif
                <div class="field">
                    <label>Active</label>
                    <select class="ui dropdown" name="enable_flag">
                        @foreach ([
                            'y' => 'Yes',
                            'n' => 'No',
                            ] as $key => $value)
                            <option {{ $product->enable_flag == $key[0] ? 'selected' : '' }} value="{{ $key }}">
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Update</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>

@endsection
@push('scripts')
    <script type="text/javascript">
    $(function () {

        $('[name="main"]').on('change',function(){
            <?php
            $request = request()->except(['main']);
            $url = route('product.edit',['product' => $product->id]) . '?' . (!empty($request) ? http_build_query($request) . '&' : '');
            ?>
            window.location.assign('{!! $url !!}main=' + $(this).val());
        });
        $('[name="general"]').on('change',function(){
            <?php
            $request = request()->except(['general']);
            $url = route('product.edit',['product' => $product->id]) . '?' . (!empty($request) ? http_build_query($request) . '&' : '');
            ?>
            window.location.assign('{!! $url !!}general=' + $(this).val());
        });
    });
    </script>
    @endpush
