@extends('layouts.semantic')

@section('title')
    Quote Regular
@endsection

@section('description')

@endsection

@section('content')

    @if (session('status'))
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                All good!
            </div>
            <p>{{ session('status') }}</p>
        </div>
    @endif

    @if($errors->count())
        <div class="ui error message">
            <i class="close icon"></i>
            <div class="header">
                There were some errors on your request
            </div>
            <ul class="list">
                @foreach ($errors->all() as $key => $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="ui equal width grid tiny statistics">
        <div class="column center aligned">
            <div class="ui tiny statistic">
                <div class="value">
                    {{ formatNumber($stats['total'],0) }}
                </div>
                <div class="label">
                    Total Items
                </div>
            </div>
        </div>
        <div class="column center aligned">
            <div class="ui tiny statistic">
                <div class="value">
                    {{ formatNumber($stats['quantity'],0) }}
                </div>
                <div class="label">
                    Total Quantity
                </div>
            </div>
        </div>
        <div class="column center aligned">
            <div class="ui tiny statistic">
                <div class="value">
                    {{ formatNumber($stats['totalprice']) }}
                </div>
                <div class="label">
                    Total Amount
                </div>
            </div>
        </div>
        <div class="column center aligned">

        </div>
    </div>
    <div class="ui horizontal divider"> </div>
    <form class="ui form" method="GET" action="{{ route('quote.regular.index') }}">
        <div class="ui grid fields">
            <div class="four wide field">
                <label>Order Date From</label>
                <div class="ui calendar" id="datetimepicker1">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="orderdatefrom" value="{{ request('orderdatefrom') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Order Date To</label>
                <div class="ui calendar" id="datetimepicker2">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="orderdateto" value="{{ request('orderdateto') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Delivery Date From</label>
                <div class="ui calendar" id="datetimepicker3">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="deliverydatefrom" value="{{ request('deliverydatefrom') }}">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <label>Delivery Date To</label>
                <div class="ui calendar" id="datetimepicker4">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="deliverydateto" value="{{ request('deliverydateto') }}">
                    </div>
                </div>
            </div>


            <div class="four wide field">
                <label>Customer Name</label>
                <select class="ui dropdown" name="customer">
                    <option>All </option>
                    @foreach ($customers as $key => $value)
                        <option {{ request('customer') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Company</label>
                <select class="ui dropdown" name="company">
                    <option>All </option>
                    @foreach ($companies as $key => $value)
                        <option {{ request('company') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Supplier</label>
                <select class="ui dropdown" name="supplier">
                    <option>All </option>
                    @foreach ($suppliers as $key => $value)
                        <option {{ request('supplier') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Status</label>
                <select class="ui dropdown" name="status">
                    <option>All </option>
                    @foreach ($statuses as $key => $value)
                        <option {{ request('status') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Results per page</label>
                <div class="ui input">
                    <input type="number" name="perpage" value="{{ request('perpage',20) }}" min="1">
                </div>
            </div>
            <div class="four wide field">
                <label>Order by</label>
                <select class="ui dropdown" name="sortby">
                    @foreach ([
                        'quote' => 'Quote ID',
                        'customer' => 'Customer Name',
                        'company' => 'Company',
                        'amount' => 'Amount',
                        'items' => 'Items',
                        'quantity' => 'Quantity',
                        'orderdate' => 'Order Date ',
                        'deliverydate' => 'Delivery Date',
                        'supplier' => 'Supplier',
                        'stats' => 'Status',
                        ] as $key => $value)
                        <option {{ request('sortby') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>Sort Type</label>
                <select class="ui dropdown" name="sortdir">
                    @foreach ([
                        'asc' => 'Ascending',
                        'desc' => 'Descending',
                        ] as $key => $value)
                        <option {{ request('sortdir') == $key ? 'selected' : '' }} value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="four wide field">
                <label>&nbsp;</label>
                <div class="ui action input">
                    <input type="text" placeholder="Type a keyword.." name="q" value="{{ request('q') }}">
                    <button class="ui button">Search</button>
                </div>
            </div>
        </div>
    </form>
    <div class="ui horizontal divider"> </div>

    <div class="responsive-table">
        <table class="ui compact small table">
            <thead>
                <tr>
                    <th>Action</th>
                    <th>ID</th>
                    <th>Customer Name</th>
                    <th>Company</th>
                    <th>Amount</th>
                    <th>Items</th>
                    <th>Quantity</th>
                    <th>Order Date</th>
                    <th>Delivery Date</th>
                    <th>Supplier</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if($quotes->count() > 0)
                    @foreach ($quotes as $k => $quote)
                        <tr class="top aligned">
                            <td>
                                <label><input name="deleteIds" value="{{ $quote->id }}" type="checkbox"></label>&nbsp;&nbsp;&nbsp;
                                <a href="{{ route('quote.regular.destroy',['quote' => $quote->id]) }}">Delete</a> |
                                <a href="{{ route('quote.regular.edit',['quote' => $quote->id]) }}">Edit</a> |
                                <a href=" ">Assign Supplier</a>
                            </td>
                            <td>{{ str_pad($quote->id, 6, "0", STR_PAD_LEFT) }}</td>
                            <td>{{ $quote->contact->name }}</td>
                            <td>{{ $quote->company->name }}</td>
                            <td>{{ formatNumber($quote->totalquoteprice) }}</td>
                            <td>{{ formatNumber($quote->quoteDetails->count(),0) }}</td>
                            <td>{{ formatNumber($quote->quoteDetails->sum('quantity'),0) }}</td>
                            <td>{{ $quote->orderdate->timestamp <= 0 ? '-' : $quote->orderdate->format('M d, Y') }}</td>
                            <td>{{ $quote->deliverydate->timestamp <= 0 ? '-' : $quote->deliverydate->format('M d, Y') }}</td>
                            <td>
                                @if($quote->supplier && $quote->supplier->count() > 0)
                                    @foreach ($quote->supplier as $key => $s)
                                        {{ $s->name }}<br>
                                    @endforeach
                                @else
                                    <span>-</span>
                                @endif
                            </td>
                            <td>{{ $quote->status->name }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="11" class="text-center">No Results found</td>
                    </tr>
                @endif
            </tbody>
            <tfoot class="full-width">
                <tr>
                    <th colspan="11">
                        <a class="ui primary labeled icon small button" href="{{ route('quote.regular.create') }}"><i class="plus icon"></i>Add</a>
                        <a class="ui red labeled icon small button" href="#" id="deleteQuotes"><i class="trash icon"></i>Delete</a>
                    </th>
                </tr>
            </tfoot>
        </table>

    </div>

    <div class="ui horizontal divider"> </div>
    <div class="ui grid">
        <div class="three column row">
            <div class="left floated column">
                {{ $quotes->appends(collect($_GET)->reject(function ($name) {
                    return empty($name);
                })->all())->links() }}
            </div>
            <div class="column">
                @if($quotes->count())
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing {{ $quotes->count() }} of {{ $quotes->total() }} Results
                    </h4>
                @else
                    <h4 class="ui horizontal divider header">
                        <i class="tag icon"></i>
                        Showing 0 Results
                    </h4>
                @endif
            </div>
            <div class="right floated right aligned column">
                @if($quotes->count())
                    <form action="{{ request()->fullUrlWithQuery([]) }}" method="GET">
                        @foreach ($_GET as $key => $value)
                            <input type="hidden" name="{{ $key }}" value="{{ $value }}"/>
                        @endforeach
                        <div class="ui action">
                            <input type="hidden" name="export" value="xls"/>
                            <button class="ui teal right labeled icon button">
                                <i class="download icon"></i>
                                Export to XLS
                            </button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection


@push('styles')
    <link rel="stylesheet" href="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.css">
@endpush
@push('scripts')
    <script type="text/javascript" src="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.js"></script>

    <script type="text/javascript">
    $(function () {
        $('#datetimepicker1').calendar({
            type: 'date',
            endCalendar: $('#datetimepicker2')
        });
        $('#datetimepicker2').calendar({
            type: 'date',
            startCalendar: $('#datetimepicker1')
        });

        $('#datetimepicker3').calendar({
            type: 'date',
            endCalendar: $('#datetimepicker4')
        });
        $('#datetimepicker4').calendar({
            type: 'date',
            startCalendar: $('#datetimepicker3')
        });


        $('#deleteQuotes').click(function(e){
            e.preventDefault();
            var ids = [];
            $('[name="deleteIds"]:checked').each(function(){
                ids.push($(this).val());
            });

            if(ids.length){
                window.location.assign('{{ route('quote.regular.destroyBatch') }}?ids=' + ids.join(','));
            }
        });

    });
    </script>
@endpush
