@extends('layouts.semantic')

@section('title')
    Company User Account
@endsection

@section('description')
    {{ $company->name }}
@endsection

@push('menubar')
    <a class="item" href="{{ route('company') }}">
        <i class="chevron left icon"></i> Back to Companies
    </a>
@endpush

@section('content')
    <div class="ui grid">
        <div class="three column row">
            <div class="column">
                @if (session('status'))
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            All good!
                        </div>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                @if($errors->count())
                    <div class="ui error message">
                        <i class="close icon"></i>
                        <div class="header">
                            There were some errors on your request
                        </div>
                        <ul class="list">
                            @foreach ($errors->all() as $key => $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <form class="ui grid form" action="{{ route('company.accountsave',['company' => $company->id ]) }}" method="POST">
        {{ csrf_field() }}
        <div class="three column row">
            <div class="column">
                <div class="field required">
                    <label>Username</label>
                    <div class="ui input">
                        <input type="text" name="username" value="{{ old('username',$company->account ? $company->account->username : '') }}" required>
                    </div>
                </div>

                @if($company->account)
                    <div class="field required">
                        <label>Current Password</label>
                        <div class="ui input">
                            <input type="password" name="old_password" value="{{ old('old_password') }}" required>
                        </div>
                    </div>
                @endif
                <div class="field required">
                    <label>New Password</label>
                    <div class="ui input">
                        <input type="password" name="password" value="{{ old('password') }}">
                    </div>
                </div>
                <div class="field required">
                    <label>Confirm</label>
                    <div class="ui input">
                        <input type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
                    </div>
                </div>
                <button class="ui left primary floated labeled icon button" type="submit"><i class="save icon"></i>Update</button>
                <button class="ui right floated labeled icon button" type="reset"><i class="wait icon"></i>Clear</button>
            </div>
        </div>
    </form>
@endsection
