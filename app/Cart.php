<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $table = 'cart';
    public $timestamps = false;
    public $dates = [
        'orderdate',
        'deliverydate'
    ];
    protected $fillable = [
        'contact_id', 'company_id', 'instruction', 'orderdate', 'deliverydate', 'status_id', 'title', 'delete_flag'

    ];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

}
