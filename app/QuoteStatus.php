<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteStatus extends Model
{
    //
    protected $table = 'quotestatus';
    protected $fillable = [
         'name', 'description', 'delete_flag'
    ];

}
