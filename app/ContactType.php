<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactType extends Model
{
    //
    protected $table = 'contacttype';
    public $timestamps = false;
    protected $fillable = [
          'name', 'description', 'delete_flag'
    ];

    public function contactdetail()
    {
        return $this->belongsTo('App\ContactDetails');
    }

}
