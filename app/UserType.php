<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    //
    protected $table = 'adminusertype';
    protected $fillable = [
         'name', 'description', 'delete_flag'
    ];

}
