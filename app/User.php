<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $table = 'adminuser';
    public $timestamps = false;
    protected $fillable = [
        'name', 'email', 'password', 'type_id' ,'delete_flag','username'
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password',
    ];

    public function getRememberToken()
    {
        return null; // will not support
    }

    /**
    * Set the token value for the "remember me" session.
    *
    * @param  string  $value
    * @return void
    */
    public function setRememberToken($value)
    {
        // will not support
    }

    /**
    * Get the column name for the "remember me" token.
    *
    * @return string
    */
    public function getRememberTokenName()
    {
        return null; // will not support
    }

    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();

        if( ! $isRememberTokenAttribute )
        {
            parent::setAttribute($key, $value);
        }
    }

    public function type()
    {
        return $this->belongsTo('App\UserType');
    }

}
