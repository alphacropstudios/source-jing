<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ProductPrices extends Model
{
    //
    protected $table = 'company_product';
    public $timestamps = false;
    protected $fillable = [
         'company_id', 'product_id', 'retailprice', 'wholesaleprice', 'dateupdatedretailprice', 'dateupdatedwholesaleprice', 'enable_flag', 'delete_flag'
    ];

    protected $dates = [
       'dateupdatedretailprice',
       'dateupdatedwholesaleprice',
   ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier','company_id');
    }

    public function getBestRetailPrice(){
        return $this->orderby('company_product.retailprice','desc')->first();
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active_supplier', function (Builder $builder) {
            $builder->join('company as c','c.id','=','company_product.company_id')
            ->where('c.blacklisted_flag', 'LIKE', 'n')
            ->where('company_product.enable_flag', 'LIKE', 'y')
            ->where('c.enabled_flag', 'LIKE', 'y')
            ->where('c.delete_flag', 'LIKE', 'n');
        });
    }

}
