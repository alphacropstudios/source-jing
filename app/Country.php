<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $table = 'country';
    public $timestamps = false;
    protected $fillable = [
        'name', 'description', 'delete_flag'
    ];

}
