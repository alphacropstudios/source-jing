<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Company;

class CompanyActivation extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $url;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Company $company)
    {
        //
        $this->company = $company;
        if(empty($company->account->activation_code)){
            $company->account->update([
                'activation_code' => str_random(8)
            ]);
        }

        $this->url = url('company/activate/' . $company->account->activation_code);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.activation');
    }
}
