<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartCustom extends Model
{
    //
    protected $table = 'cartcustom';
    public $timestamps = false;
    
    protected $fillable = [
        'cart_id', 'category_id', 'quantity', 'customname', 'delete_flag'
    ];

    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }

}
