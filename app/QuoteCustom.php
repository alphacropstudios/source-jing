<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class QuoteCustom extends Model
{
    //
    protected $table = 'quotecustom';
    public $timestamps = false;

    protected $fillable = [
        'quote_id', 'cartcustom_id', 'category_id', 'quantity', 'customname', 'delete_flag'
    ];

    public function cartcustom()
    {
        return $this->belongsTo('App\CartCustom');
    }

    public function quote()
    {
        return $this->belongsTo('App\Quote');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function supplier()
    {
        return $this->belongsToMany('App\Supplier','supplier_quotecustom','quotecustom_id','supplier_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('delete_flag', function (Builder $builder) {
            $builder->where('quotecustom.delete_flag', 'LIKE', 'n');
        });
    }
}
