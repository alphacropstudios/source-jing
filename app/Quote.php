<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Quote extends Model
{
    //
    protected $table = 'quote';
    public $timestamps = false;
    protected $fillable = [
        'cart_id', 'contact_id', 'company_id', 'instruction', 'orderdate', 'deliverydate', 'totalquoteprice', 'expirydate', 'title', 'status_id', 'delete_flag'
    ];

    protected $dates = [
        'orderdate',
        'deliverydate',
        'expirydate'
    ];

    public function quoteDetails()
    {
        return $this->hasMany('App\QuoteDetail');
    }

    public function quoteCustom()
    {
        return $this->hasMany('App\QuoteCustom');
    }

    public function supplier()
    {
        return $this->belongsToMany('App\Supplier','supplier_quotedetail','quote_id','supplier_id');
    }

    public function status()
    {
        return $this->belongsTo('App\QuoteStatus');
    }

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('delete_flag', function (Builder $builder) {
            $builder->where('quote.delete_flag', 'LIKE', 'n');
        });
    }

}
