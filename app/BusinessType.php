<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessType extends Model
{
    //
    protected $table = 'businesstype';
    public $timestamps = false;
    protected $fillable = [
          'name', 'description', 'delete_flag'
    ];

}
