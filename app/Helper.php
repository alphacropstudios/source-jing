<?php
/**
* change plain number to formatted currency
*
* @param $number
* @param $currency
*/
function formatNumber($number, $cents = 2 , $prefix = '')
{
    return $prefix . number_format($number, $cents, '.', ',');
}
