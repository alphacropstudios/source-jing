<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $table = 'address';
    public $timestamps = false;
    protected $fillable = [
         'company_id', 'street', 'city', 'state', 'country_id', 'zipcode', 'delete_flag'
    ];

}
