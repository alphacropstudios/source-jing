<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageType extends Model
{
    //
    protected $table = 'imagetype';
    public $timestamps = false;
    protected $fillable = [
          'name', 'description', 'delete_flag'
    ];

}
