<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CartDetail extends Model
{
    //
    protected $table = 'cartdetail';
    public $timestamps = false;
    protected $fillable = [
         'cart_id', 'product_id', 'quantity', 'delete_flag'
    ];

    public function cart()
    {
        return $this->belongsTo('App\Cart');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('delete_flag', function (Builder $builder) {
            $builder->where('cartdetail.delete_flag', 'LIKE', 'n');
        });
    }
}
