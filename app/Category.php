<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'category';
    public $timestamps = false;
    protected $fillable = [
        'name', 'categorylevel_id', 'parent_id', 'description', 'enable_flag', 'delete_flag'
    ];

    public function parent()
    {
        return $this->belongsTo('App\Category','parent_id');
    }
}
