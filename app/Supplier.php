<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    //
    protected $table = 'company';
    public $timestamps = false;
    protected $fillable = [
         'name', 'vat', 'businesstype_id', 'businessregistration', 'blacklisted_flag', 'blacklistedremarks', 'dateblacklisted', 'enabled_flag', 'supplier_flag', 'delete_flag'
    ];

}
