<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactDetails extends Model
{
    //
    protected $table = 'contactdetails';
    public $timestamps = false;
    protected $fillable = [
          'contact_id', 'name', 'contacttype_id', 'delete_flag'
    ];

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    public function type()
    {
        return $this->belongsTo('App\ContactType');
    }

}
