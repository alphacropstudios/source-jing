<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\QuoteDetail;
use App\Category;
use Carbon\Carbon;
use Excel;
use App\User;
use App\UserType;
use Hash;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $limit = 20;
        $sort = 'desc';

        $quotedetails = QuoteDetail::select('quotedetail.*')
        ->with('quote')
        ->with('product')
        ->with('cartdetail');

        $stats = [
            'quantity' => 0,
            'total' => 0,
            'totalprice' => 0,
        ];

        if($request->has('orderdatefrom')){
            $quotedetails = $quotedetails->whereHas('quote',function ($query) use($request){
                $query->where('orderdate', '>=',(new Carbon($request->orderdatefrom))->startOfDay()->toDateTimeString());
            });
        }

        if($request->has('orderdateto')){
            $quotedetails = $quotedetails->whereHas('quote',function ($query) use($request){
                $query->where('orderdate', '<=',(new Carbon($request->orderdateto))->endOfDay()->toDateTimeString());
            });
        }

        if($request->has('deliverydatefrom')){
            $quotedetails = $quotedetails->whereHas('quote',function ($query) use($request){
                $query->where('deliverydate', '>=',(new Carbon($request->deliverydatefrom))->startOfDay()->toDateTimeString());
            });
        }

        if($request->has('deliverydateto')){
            $quotedetails = $quotedetails->whereHas('quote',function ($query) use($request){
                $query->where('deliverydate', '<=',(new Carbon($request->deliverydateto))->endOfDay()->toDateTimeString());
            });
        }

        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('main') && $request->get('main') != 'All'){
            $quotedetails = $quotedetails->whereHas('product.category.parent.parent',function($query) use($request){
                $query->where('name','=',$request->get('main'));
            });
        }

        if($request->has('general') && $request->get('general') != 'All'){
            $quotedetails = $quotedetails->whereHas('product.category.parent',function($query) use($request){
                $query->where('name','=',$request->get('general'));
            });
        }

        if($request->has('brand') && $request->get('brand') != 'All'){
            $quotedetails = $quotedetails->whereHas('product.category',function($query) use($request){
                $query->where('name','=',$request->get('brand'));
            });
        }


        if($request->has('q')){
            $quotedetails = $quotedetails->whereHas('product',function($query) use($request){
                $query->where('name','like','%' . $request->get('q') . '%');
            });
        }

        if($request->has('sortdir') && $request->has('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }

        if($request->has('sortby') && $request->has('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'description':
                $quotedetails = $quotedetails->join('product as p','p.id','=','quotedetail.product_id')
                ->orderBy('p.name', $sort);
                break;
                case 'brand':
                $quotedetails = $quotedetails->join('product as p','p.id','=','quotedetail.product_id')
                ->join('category as c1','c1.id','=','p.category_id')
                ->orderBy('c1.name', $sort);
                break;
                case 'general':
                $quotedetails = $quotedetails->join('product as p','p.id','=','quotedetail.product_id')
                ->join('category as c1','c1.id','=','p.category_id')
                ->join('category as c2','c2.id','=','c1.parent_id')
                ->orderBy('c2.name', $sort);
                break;
                case 'qty':
                $quotedetails = $quotedetails->orderBy('quotedetail.quantity', $sort);
                break;
                case 'unitprice':
                $quotedetails = $quotedetails->orderBy('quotedetail.unitprice', $sort);
                break;
                case 'totalprice':
                $quotedetails = $quotedetails->orderBy('quotedetail.totalprice', $sort);
                break;
                case 'main':
                $quotedetails = $quotedetails->join('product as p','p.id','=','quotedetail.product_id')
                ->join('category as c1','c1.id','=','p.category_id')
                ->join('category as c2','c2.id','=','c1.parent_id')
                ->join('category as c3','c3.id','=','c2.parent_id')
                ->orderBy('c3.name', $sort);
                break;
                case 'deliverydate':
                $quotedetails = $quotedetails->join('quote as q','q.id','=','quotedetail.quote_id')
                ->orderBy('q.deliverydate', $sort);
                break;
                case 'quoteid':
                $quotedetails = $quotedetails->orderBy('quotedetail.quote_id', $sort);
                break;
                case 'customername':
                $quotedetails = $quotedetails->join('cartdetail as cd','cd.id','=','quotedetail.cartdetail_id')
                ->join('cart as c','c.id','=','cd.cart_id')
                ->join('contact as co','co.id','=','c.contact_id')
                ->orderBy('co.name', $sort);
                break;
                case 'company':
                $quotedetails = $quotedetails->join('cartdetail as cd','cd.id','=','quotedetail.cartdetail_id')
                ->join('cart as c','c.id','=','cd.cart_id')
                ->join('contact as co','co.id','=','c.contact_id')
                ->join('company as cp','cp.id','=','co.company_id')
                ->orderBy('cp.name', $sort);
                break;
                case 'supplier':
                $quotedetails = $quotedetails->join('supplier_quotedetail as sq','sq.quotedetail_id','=','quotedetail.id')
                ->join('company as cp','cp.id','=','sq.supplier_id')
                ->addSelect('cp.name as suppliername')
                ->orderBy('cp.name', $sort);
                break;
                case 'status':
                $quotedetails = $quotedetails->join('quote as q','q.id','=','quotedetail.quote_id')
                ->join('quotestatus as qs','qs.id','=','q.status_id')
                ->orderBy('qs.name', $sort);
                break;
                default:
                $quotedetails = $quotedetails->join('quote as q','q.id','=','quotedetail.quote_id')
                ->orderBy('q.orderdate', $sort);
                break;
            }
        }else{
            $quotedetails = $quotedetails->join('quote as q','q.id','=','quotedetail.quote_id')
            ->orderBy('q.orderdate', $sort);
        }

        if($request->has('export')){
            $titles = [
                "Order Date",
                "Description",
                "Main ",
                "General",
                "Brand",
                "Quantity",
                "Unit Price",
                "Total Price",
                "Delivery Date",
                "Quote ID",
                "Customer Name",
                "Company",
                "Supplier",
                "Status",
            ];


            $quotedetails = $quotedetails->get()->map(function ($item, $key) use($titles) {
                return [
                    $item->quote->orderdate->timestamp <=0 ? '-' : $item->quote->orderdate->format('M d, Y'),
                    $item->product->name,
                    $item->product->category->parent->parent->name,
                    $item->product->category->parent->name,
                    $item->product->category->name,
                    formatNumber($item->quantity,0),
                    formatNumber($item->unitprice),
                    formatNumber($item->totalprice),
                    $item->quote->deliverydate->timestamp <=0 ? '-' : $item->quote->deliverydate->format('M d, Y'),
                    $item->quote->id,
                    $item->cartdetail->cart->contact->name,
                    $item->cartdetail->cart->contact->company->name,
                    $item->quote->supplier->count() > 0 ? $item->quote->supplier->first()->name : '-',
                    $item->quote->status->name,
                ];
            });

            $quotedetails = $quotedetails->toArray();
            Excel::create('export-quotes-' . date('YmdHisa'), function($excel) use($quotedetails,$titles){

                $excel->setTitle('Quotes Table Export');

                $excel->sheet('Results', function($sheet) use($quotedetails,$titles){
                    $sheet->fromArray($quotedetails, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })->export('xls');

            });

            return back();
        }else{

            $stats = [
                'quantity' => $quotedetails->sum('quotedetail.quantity'),
                'total' => $quotedetails->count(),
                'totalprice' => $quotedetails->sum('quotedetail.totalprice'),
            ];


            $quotedetails = $quotedetails->paginate($limit);
        }

        $categories = Category::orderBy('name','asc')->get()->unique('name')->groupBy('categorylevel_id');



        return view('dashboard',compact('quotedetails','categories' ,'stats' ));
    }

    public function adminusers(Request $request){

        $limit = 20;
        $sort = 'asc';

        $users = User::select('adminuser.*')->where('adminuser.delete_flag','NOT LIKE','y');

        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('sortdir') && $request->get('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }

        if($request->has('type') && $request->get('type')!='All'){
            $users = $users->where('type_id','=',$request->get('type'));
        }

        if($request->has('sortby') && $request->get('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'type':
                $users = $users->join('adminusertype as t','adminuser.type_id','=','t.id')
                ->orderBy('t.name', $sort);
                break;
                case 'username':
                $users = $users->orderBy('adminuser.username', $sort);
                break;
                case 'email':
                $users = $users->orderBy('adminuser.email', $sort);
                break;
                default:
                case 'name':
                $users = $users->orderBy('adminuser.name', $sort);
                break;
            }
        }else{
            $users = $users->orderBy('adminuser.name', $sort);
        }

        if($request->has('export')){
            $titles = [
                'Username',
                'Email',
                'Type',
                'Name',
            ];


            $users = $users->get()->map(function ($user, $key) use($titles) {
                return [
                    $user->username,
                    $user->email,
                    $user->type ? $user->type->name : '-',
                    $user->name
                ];
            });

            $users = $users->toArray();
            Excel::create('export-admin-users-' . date('YmdHisa'), function($excel) use($users,$titles){

                $excel->setTitle('Admin Users');

                $excel->sheet('Results', function($sheet) use($users,$titles){
                    $sheet->fromArray($users, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })->export('xls');

            });

            return back();
        }

        $users = $users->paginate($limit);

        $types = UserType::select(['id','name'])->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        return view('admin-users',compact(['users','types']));
    }
    public function adminusersadd(){
        $types = UserType::select(['id','name'])->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        return view('admin-users-add',compact(['types']));
    }
    public function adminuserscreate(Request $request){
        $this->validate($request,[
            'username' => 'required|unique:adminuser,username',
            'email' => 'required',
            'name' => 'required',
            'type' => 'required',
            'password' => 'required|confirmed'
        ]);

        User::create([
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'name' => $request->get('name'),
            'type_id' => $request->get('type'),
            'password' => bcrypt( $request->get('password') ),
            'delete_flag' => 'n'
        ]);

        return back()->with('status','Admin User added successfully');
    }
    public function adminusersedit(Request $request,User $user){
        $types = UserType::select(['id','name'])->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        return view('admin-users-edit',compact(['types','user']));
    }

    public function adminusersupdate(Request $request,User $user){

        if (!Hash::check($request->old_password, $user->password)) {
            return back()->withErrors(['Current Password Incorrect']);
        }

        $this->validate($request,[
            'username' => 'required|unique:adminuser,username,' . $user->id . '',
            'email' => 'required',
            'name' => 'required',
            'type' => 'required',
        ]);

        $user->update([
            'username' => $request->username,
            'email' => $request->get('email'),
            'name' => $request->get('name'),
            'type_id' => $request->get('type'),
        ]);

        if($request->has('password')){
            $this->validate($request,[
                'password' => 'required|confirmed'
            ]);

            $user->update([
                'password' => bcrypt($request->password),
            ]);
        }

        return back()->with('status','Admin User Updated successfully');
    }

    public function adminuserdestroy(Request $request,User $user){

        $user->update([
            'delete_flag' => 'y',
        ]);

        return back()->with('status','Admin User deleted successfully');
    }

    public function adminuserdestroyBatch(Request $request,User $user){

        $ids = explode(',',$request->get('ids'));
        $users = User::whereIn('id', $ids)
        ->get();
        foreach ($users as $key => $user) {
            $users->update([
                'delete_flag' => 'y',
            ]);
        }
        return back()->with('status','Admin Users deleted successfully');
    }
}
