<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\CategoryLevel;
use Excel;

class CategoryController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct(Request $request)
    {
        $this->middleware('auth');

    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $limit = 20;
        $sort = 'desc';

        $categories = Category::select('category.*')->where('category.delete_flag','NOT LIKE','y');

        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('level')){
            switch ($request->get('level')) {
                case 'main':
                $categories = $categories->where('category.categorylevel_id','=','1');
                break;
                case 'general':
                $categories = $categories->where('category.categorylevel_id','=','2');
                break;
                case 'brand':
                $categories = $categories->where('category.categorylevel_id','=','3');
                break;
            }
        }

        if($request->has('parent')){
            $categories = $categories->where('category.parent_id','=',$request->get('parent'));
        }

        if($request->has('sortdir') && $request->has('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }

        if($request->has('sortby') && $request->has('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'name':
                $categories = $categories->orderBy('name', $sort);
                break;
                case 'level':
                $categories = $categories->orderBy('categorylevel_id', $sort);
                break;
                case 'parent':
                if($request->has('level') && $request->get('level')=='main'){
                    break;
                }
                $categories = $categories->leftJoin('category as c2','c2.id','=','category.parent_id')
                ->orderBy('c2.name', $sort);
                break;
                case 'status':
                $categories = $categories->orderBy('category.enable_flag', $sort);
                break;

            }
        }

        if($request->has('q')){
            $categories = $categories->where('name','like','%' . $request->get('q') . '%');
        }


        if($request->has('export')){
            $titles = [
                "Name",
                "Level",
                "Parent",
                "Status",
            ];

            $categories = $categories->get()->map(function ($item, $key) use($titles) {
                return [
                    $item->name,
                    ['','Main','General','Brand'][$item->categorylevel_id],
                    $item->parent_id === 0 ? 'None' : $item->parent->name,
                    $item->enable_flag == 'y' ? 'Active' : 'Inactive',
                ];
            });

            $categories = $categories->toArray();
            Excel::create('export-category-' . date('YmdHisa'), function($excel) use($categories,$titles){

                $excel->setTitle('Category Table Export');

                $excel->sheet('Results', function($sheet) use($categories,$titles){
                    $sheet->fromArray($categories, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })->export('xls');

            });

            return back();
        }else{
            $categories = $categories->paginate($limit);
        }


        $minParentLevel = 1;
        if($request->has('level')){
            switch ($request->get('level')) {
                case 'main':
                $minParentLevel = 0;
                break;
                case 'general':
                $minParentLevel = 1;
                break;
                case 'brand':
                $minParentLevel = 2;
                break;
            }
        }else{
            $minParentLevel = 1;
        }



        $parents = Category::where('categorylevel_id','=',$minParentLevel)->get();

        return view('category',compact(['categories','parents']));
    }

    public function create(Request $request){
        if($request->session()){
            $request->session()->forget('_old_input');
        }

        $minParentLevel = 0;
        if($request->has('level')){
            switch ($request->get('level')) {
                case 'general':
                $minParentLevel = 1;
                break;
                case 'brand':
                $minParentLevel = 2;
                break;
                case 'main':
                case 'default':
                $minParentLevel = 0;
                break;
            }
        }else{
            $minParentLevel = 0;
        }
        $parents = Category::where('categorylevel_id','=',$minParentLevel)->orderBy('name')->get();

        return view('category-add',compact(['parents']));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'body' => 'string',
        ]);

        Category::create([
            'name' => $request->name,
            'description' => $request->description,
            'categorylevel_id' => ['main' => 1,'general' => 2 ,'brand' => 3][strtolower($request->level)],
            'parent_id' => $request->parent,
            'enable_flag' => $request->enable_flag,
            'delete_flag' => 'n',
        ]);

        return back()->with('status','Category added successfully');
    }

    public function edit(Request $request,Category $category){

        $minParentLevel = 1;
        if($request->has('level')){
            $minParentLevel = $request->get('level') - 1;
        }else{
            $minParentLevel = $category->categorylevel_id - 1;
        }

        $parents = Category::where('categorylevel_id','=',$minParentLevel)->get();

        return view('category-edit',compact(['category','parents']));
    }

    public function update(Request $request,Category $category){

        $category->update([
            'name' => $request->name,
            'description' => $request->description,
            'categorylevel_id' => $request->level,
            'parent_id' => $request->parent,
            'enable_flag' => $request->enable_flag,
            'delete_flag' => 'n',
        ]);

        return back()->with('status','Category updated successfully');
    }

    public function destroy(Request $request,Category $category){

        $category->update([
            'delete_flag' => 'y',
        ]);

        return back()->with('status','Category deleted successfully');
    }

    public function destroyBatch(Request $request){

        $ids = explode(',',$request->get('ids'));
        $categories = Category::whereIn('id', $ids)
        ->get();
        foreach ($categories as $key => $category) {
            $category->update([
                'delete_flag' => 'y',
            ]);
        }
        return back()->with('status','Categories deleted successfully');
    }

    public function level(Request $request){

        $categorylevels = CategoryLevel::select('categorylevel.*')->where('categorylevel.delete_flag','NOT LIKE','y');
        $limit = 20;
        $sort = 'asc';

        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('sortdir') && $request->has('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }

        if($request->has('sortby') && $request->has('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'name':
                case 'description':
                $categorylevels = $categorylevels->orderBy('categorylevel.' . $request->get('sortby'), $sort);
                break;
            }
        }

        if($request->has('export')){
            $titles = [
                'Name',
                'Description',
            ];


            $categorylevels = $categorylevels->get()->map(function ($categorylevel, $key) use($titles) {
                return [
                    $categorylevel->name ,
                    $categorylevel->description
                ];
            });

            $categorylevels = $categorylevels->toArray();
            Excel::create('export-category-levels-' . date('YmdHisa'), function($excel) use($categorylevels,$titles){

                $excel->setTitle('Category Levels');

                $excel->sheet('Results', function($sheet) use($categorylevels,$titles){
                    $sheet->fromArray($categorylevels, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })->export('xls');

            });

            return back();
        }else{
            $categorylevels = $categorylevels->paginate($limit);
        }

        return view('category-level',compact(['categorylevels']));
    }

    public function leveladd(Request $request){
        return view('category-level-add');
    }

    public function levelstore(Request $request){
        $this->validate($request,[
            'name' => 'required|string'
        ]);

        CategoryLevel::create([
            'name' => $request->name,
            'description' => $request->description
        ]);

        return back()->with('status','Category Level created successfully');
    }

    public function leveledit(Request $request,CategoryLevel $level){
        $categorylevel = $level;
        return view('category-level-edit',compact(['categorylevel']));
    }



    public function levelupdate(Request $request,CategoryLevel $level){
        $this->validate($request,[
            'name' => 'required|string'
        ]);

        $level->update([
            'name' => $request->name,
            'description' => $request->description
        ]);

        return back()->with('status','Category Level updated successfully');
    }

    public function leveldestroy(Request $request,CategoryLevel $level){
        $level->update([
            'delete_flag' => 'y',
        ]);

        return back()->with('status','Category Level deleted successfully');
    }

    public function leveldestroyBatch(Request $request){

        $ids = explode(',',$request->get('ids'));
        $categories = CategoryLevel::whereIn('id', $ids)
        ->get();

        foreach ($categories as $key => $category) {
            $category->update([
                'delete_flag' => 'y',
            ]);
        }
        return back()->with('status','Category Levels deleted successfully');
    }
}
