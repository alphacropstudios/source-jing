<?php

namespace App\Http\Controllers;

use App\Quote;
use App\QuoteCustom;
use App\QuoteDetail;
use App\Contact;
use App\Cart;
use App\CartCustom;
use App\CartDetail;
use App\Company;
use App\Supplier;
use App\Category;
use App\Product;
use App\QuoteStatus;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Excel;
use Cache;
use ZanySoft\LaravelPDF\PDF;

class QuoteController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('quote');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function regularindex(Request $request)
    {
        $stats = [
            'quantity' => 0,
            'total' => 0,
            'totalprice' => 0,
        ];

        $limit = 20;
        $sort = 'asc';

        $quotes = Quote::select([
            'quote.id',
            'quote.orderdate',
            'quote.deliverydate',
            'quote.totalquoteprice',
            'quote.contact_id',
            'quote.company_id',
            'quote.status_id'
        ])
        ->where('quote.delete_flag','NOT LIKE','y');

        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('sortdir') && $request->has('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }
        if($request->has('orderdatefrom')){
            $quotes = $quotes->where('quote.orderdate', '>=',(new Carbon($request->orderdatefrom))->startOfDay()->toDateTimeString());
        }

        if($request->has('orderdateto')){
            $quotes = $quotes->where('quote.orderdate', '<=',(new Carbon($request->orderdateto))->endOfDay()->toDateTimeString());
        }

        if($request->has('deliverydatefrom')){
            $quotes = $quotes->where('quote.deliverydate', '>=',(new Carbon($request->deliverydatefrom))->startOfDay()->toDateTimeString());
        }

        if($request->has('deliverydateto')){
            $quotes = $quotes->where('quote.deliverydate', '<=',(new Carbon($request->deliverydateto))->endOfDay()->toDateTimeString());
        }

        if($request->has('customer')){
            $quotes = $quotes->whereHas('contact',function ($query) use($request){
                $query->where('id', '=',$request->get('customer'));
            });
        }
        if($request->has('company')){
            $quotes = $quotes->whereHas('company',function ($query) use($request){
                $query->where('id', '=',$request->get('company'));
            });
        }
        if($request->has('supplier')){
            $quotes = $quotes->whereHas('supplier',function ($query) use($request){
                $query->where('supplier_id', '=',$request->get('supplier'));
            });
        }

        if($request->has('status')){
            $quotes = $quotes->whereHas('status',function ($query) use($request){
                $query->where('id', '=',$request->get('status'));
            });
        }

        if($request->has('q')){
            $quotes = $quotes
            ->leftJoin('contact as ct','ct.id','=','quote.contact_id')
            ->leftJoin('company as cp','cp.id','=','quote.company_id')
            ->leftJoin('supplier_quotedetail as sq','sq.quote_id','=','quote.id')
            ->leftJoin('company as cp1','cp1.id','=','sq.supplier_id')
            ->orWhere('ct.name','like','%' . $request->get('q') . '%')
            ->orWhere('cp.name','like','%' . $request->get('q') . '%')
            ->orWhere('cp1.name','like','%' . $request->get('q') . '%');
        }

        if($request->has('sortby') && $request->has('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'quote':
                $quotes = $quotes->orderBy('quote.id', $sort);
                break;
                case 'customer':
                $quotes = $quotes->leftJoin('contact as ct','ct.id','=','quote.contact_id')
                ->orderBy('ct.name', $sort);
                break;
                case 'company':
                $quotes = $quotes->leftJoin('company as cp','cp.id','=','quote.company_id')
                ->orderBy('cp.name', $sort);
                break;
                case 'amount':
                $quotes = $quotes->orderBy('quote.totalquoteprice', $sort);
                break;
                case 'items':
                $quotes = $quotes->leftJoin('quotedetail as qd','qd.quote_id','=','quote.id')
                ->addSelect(DB::raw('COUNT(`qd`.id) as items'))
                ->groupBy(DB::raw('`quote`.`id`,`quote`.`id`, `quote`.`orderdate`, `quote`.`deliverydate`, `quote`.`totalquoteprice`, `quote`.`contact_id`, `quote`.`company_id`, `quote`.`status_id`'))
                ->orderBy('items', $sort);
                break;
                case 'quantity':
                $quotes = $quotes->leftJoin('quotedetail as qd','qd.quote_id','=','quote.id')
                ->addSelect(DB::raw('SUM(`qd`.quantity) as qty'))
                ->groupBy(DB::raw('`quote`.`id`,`quote`.`id`, `quote`.`orderdate`, `quote`.`deliverydate`, `quote`.`totalquoteprice`, `quote`.`contact_id`, `quote`.`company_id`, `quote`.`status_id`'))
                ->orderBy('qty', $sort);
                break;
                case 'deliverydate':
                $quotes = $quotes->orderBy('quote.deliverydate', $sort);
                break;
                case 'supplier':
                $quotes = $quotes->leftJoin('supplier_quotedetail as sq','sq.quote_id','=','quote.id')
                ->leftJoin('company as cp','cp.id','=','sq.supplier_id')
                ->addSelect('cp.name as suppliername')
                ->orderBy('cp.name', $sort);
                break;
                case 'stats':
                $quotes = $quotes->join('quotestatus as qs','qs.id','=','quote.status_id')
                ->orderBy('qs.name', $sort);
                break;
                case 'orderdate':
                default:
                $quotes = $quotes->orderBy('quote.orderdate', $sort);
                break;
            }
        }else{
            $quotes = $quotes->orderBy('quote.orderdate', $sort);
        }


        if($request->has('export')){
            $titles = [
                "ID",
                "Customer Name",
                "Company",
                "Amount",
                "Items",
                "Quantity",
                "Order Date",
                "Delivery Date",
                "Supplier",
                "Status",
            ];

            $quotes = $quotes->get()->map(function ($item, $key) use($titles) {
                return [
                    str_pad($item->id, 6, "0", STR_PAD_LEFT),
                    $item->contact->name,
                    $item->company->name,
                    formatNumber($item->totalquoteprice),
                    formatNumber($item->quoteDetails->count(),0),
                    formatNumber($item->quoteDetails->sum('quantity'),0),
                    $item->orderdate->timestamp <= 0 ? '-' : $item->orderdate->format('M d, Y'),
                    $item->deliverydate->timestamp <= 0 ? '-' : $item->deliverydate->format('M d, Y'),
                    $item->supplier && $item->supplier->count() > 0 ? $item->supplier->first()->name : '-',
                    $item->status->name
                ];
            });

            $quotes = $quotes->toArray();
            Excel::create('export-quotes-regular' . date('YmdHisa'), function($excel) use($quotes,$titles){

                $excel->setTitle('Quotes Regular Table Export');

                $excel->sheet('Results', function($sheet) use($quotes,$titles){
                    $sheet->fromArray($quotes, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })->export('xls');

            });

            return back();
        }else{
            $stats = [
                'quantity' => $quotes->get()->reduce(function ($carry, $item) {
                    return $carry + $item->quoteDetails->sum('quantity');
                }),
                'total' => $quotes->get()->reduce(function ($carry, $item) {
                    return $carry + $item->quoteDetails->count();
                }),
                'totalprice' => $quotes->get()->reduce(function ($carry, $item) {
                    return $carry + $item->quoteDetails->sum('totalprice');
                }),
            ];
            $quotes = $quotes->paginate($limit);
        }

        $customers = Contact::select(['id','name'])->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $companies = Company::select(['id','name'])->where('supplier_flag','=','n')->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $suppliers = Supplier::select(['id','name'])->where('supplier_flag','=','y')->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $statuses = QuoteStatus::select(['id','name'])->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });



        return view('quote-regular',compact(['stats','quotes','customers','companies','suppliers','statuses' ]));
    }

    public function regularcreateprint(Request $request)
    {
        $pdf = new PDF();

        $customer = Contact::findOrFail($request->customer);

        $cart = [];
        $cartdata = [];
        $carttotal = [
            'item' => 0,
            'qty' => 0,
            'price' => 0,
        ];
        if($cart = cache('cart',[])){
            foreach ($cart as $key => $item) {

                $itemproduct = Product::find($item['product']);
                $cart[$key]['product'] = $itemproduct;
                if($item['supplier'] && Supplier::find($item['supplier'])){
                    $supplieroffer = $itemproduct->prices->where('company_id','=',$item['supplier'])->first();
                    $cart[$key]['unitprice'] = $itemproduct->wholesalequantity > $cart[$key]['qty'] ? $supplieroffer->retailprice : $supplieroffer->wholesaleprice;
                    $cart[$key]['supplier'] = Supplier::find($item['supplier']);
                }else{
                    $cart[$key]['unitprice'] = $itemproduct->srp;
                }

            }


            foreach ($cart as $key => $item) {
                $cartdata[] = [
                    'item' => $key + 1 ,
                    'name' => ($item['product'])->name ,
                    'qty' => $item['qty'] ,
                    'unitprice' => $item['unitprice'] ,
                    'totalprice' => formatNumber($item['unitprice'] * $item['qty']) ,
                    'supplier' => is_object($item['supplier']) ? ($item['supplier'])->name : 'None'
                ];
            }

            $carttotal = [
                'item' => collect($cart)->count(),
                'qty' => collect($cart)->sum('qty'),
                'price' => formatNumber(collect($cart)->reduce(function($carry,$item){
                    return $carry + ($item['unitprice'] * $item['qty']);
                }))
            ];
        }

        $pdf->loadView('quotes-print', [
            'quote' => [
                'title' => $request->title,
                'date' => $request->date,
                'instructions' => $request->instructions
            ],
            'customer' => [
                'name' => $customer->company->name,
                'address' => join(', ',[
                    $customer->company->address->street,
                    $customer->company->address->city,
                    $customer->company->address->state,
                    $customer->company->address->zipcode,
                ]),
                'contact' => $customer->details->where('contacttype_id','=',2)->first()->name,
                'email' => $customer->details->where('contacttype_id','=',1)->first()->name,
            ],
            'cartdata' => $cartdata,
            'carttotal' => $carttotal

        ]);
        return $pdf->Stream('quote-add-print.pdf');
    }



    public function regulareditprint(Request $request,Quote $quote)
    {
        $pdf = new PDF();

        $customer = $quote->contact;

        $cart = [];
        $cartdata = [];
        $carttotal = [
            'item' => 0,
            'qty' => 0,
            'price' => 0,
        ];
        if($cart = $quote->quoteDetails){


            foreach ($cart as $key => $item) {
                $cartdata[] = [
                    'item' => $key + 1,
                    'name' => $item->product->name,
                    'qty' => $item->quantity,
                    'unitprice' => $item->unitprice,
                    'totalprice' => formatNumber($item->unitprice * $item->quantity),
                    'supplier' => $item->supplier->count() ? $item->supplier->first()->name : 'None',
                ];
            }

            $carttotal = [
                'item' => collect($quote->quoteDetails)->count(),
                'qty' => collect($quote->quoteDetails)->sum('quantity'),
                'price' => formatNumber(collect($quote->quoteDetails)->reduce(function($carry,$item){
                    return $carry + ($item['unitprice'] * $item['quantity']);
                }))
            ];
        }

        $pdf->loadView('quotes-print', [
            'quote' => [
                'title' => $quote->title,
                'date' => $quote->deliverydate,
                'instructions' => $quote->instruction,
                'id' => $quote->id
            ],
            'customer' => [
                'name' => $customer->company->name,
                'address' => join(', ',[
                    $customer->company->address->street,
                    $customer->company->address->city,
                    $customer->company->address->state,
                    $customer->company->address->zipcode,
                ]),
                'contact' => $customer->details->where('contacttype_id','=',2)->first()->name,
                'email' => $customer->details->where('contacttype_id','=',1)->first()->name,
            ],
            'cartdata' => $cartdata,
            'carttotal' => $carttotal

        ]);
        return $pdf->Stream('quote-add-print.pdf');
    }

    public function regularcreate(Request $request)
    {
        $customer = false;
        $category = false;
        $products = false;
        $product = false;
        $categories = Category::orderBy('name','asc')->get()->groupBy('categorylevel_id');
        $customers = Contact::select(['id','name'])->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });

        if($request->has('main') && $request->get('main')!='All'){

            $parent_id = Category::where('name','LIKE',$request->get('main'))->firstOrFail()->id;
            $category = $parent_id;
            $categories['2'] = $categories['2']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });
        }

        if($request->has('general') && $request->get('general')!='All'){
            $parent_id = Category::where('name','=',$request->get('general'))->firstOrFail()->id;
            $category = $parent_id;
            $categories['3'] = $categories['3']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });
        }
        if($request->has('brand') && $request->get('brand')!='All'){
            $parent_id = Category::where('name','=',$request->get('brand'))->firstOrFail()->id;
            $category = $parent_id;
        }

        if($category){
            $products = Product::where('category_id','=',$category)->orderBy('name')->get();
        }else{
            $products = Product::orderBy('name')->get();
        }

        if($request->has('product')){
            $product = Product::findOrFail($request->product);
        }

        if($request->has('customer')){
            $customer = Contact::findOrFail($request->customer);
        }



        $cart = [];
        if($cart = cache('cart')){
            foreach ($cart as $key => $item) {

                $itemproduct = Product::find($item['product']);
                $cart[$key]['product'] = $itemproduct;
                if($item['supplier'] && Supplier::find($item['supplier'])){
                    $supplieroffer = $itemproduct->prices->where('company_id','=',$item['supplier'])->first();
                    $cart[$key]['unitprice'] = $itemproduct->wholesalequantity > $cart[$key]['qty'] ? $supplieroffer->retailprice : $supplieroffer->wholesaleprice;
                    $cart[$key]['supplier'] = Supplier::find($item['supplier']);
                }else{
                    $cart[$key]['unitprice'] = $itemproduct->srp;
                }

            }
        }

        $custom = [];
        if($custom = cache('custom')){

        }

        return view('quote-regular-add',compact([
            'categories',
            'customers',
            'customer',
            'products',
            'product',
            'cart',
            'custom',
        ]));
    }

    public function regularaddtmpitem(Request $request){

        $this->validate($request,[
            'product' => 'required',
            'supplier' => 'required',
            'qty' => 'required',
        ]);
        $cart = Cache::get('cart', []);

        $exists = false;
        foreach ($cart as $key => $item) {
            if($item['product'] == $request->product && $item['supplier'] == $request->supplier){
                $cart[$key]['qty'] = $request->qty;
                $exists = true;
                break;
            }
        }

        if(!$exists){
            $cart[] = [
                'product' => $request->product,
                'supplier' => $request->supplier,
                'qty' => $request->qty,
            ];
        }

        Cache::put('cart', $cart, 60);

        return back();

    }

    public function regularaddtempcartedit(Request $request){
        $this->validate($request,[
            'item' => 'required',
            'qty' => 'required',
        ]);

        $cart = Cache::get('cart', []);

        if(isset($cart[$request->item - 1])){
            $cart[$request->item - 1]['qty'] = $request->qty;
            Cache::put('cart', $cart, 60);
        }

        return back();

    }

    public function regularaddtempcartdelete(Request $request){
        $this->validate($request,[
            'item' => 'required',
        ]);

        $cart = Cache::get('cart', []);

        if(isset($cart[$request->item - 1])){
            $item = array_splice($cart,$request->item - 1,1);
            Cache::put('cart', $cart, 60);
        }

        return back();

    }

    public function regularstore(Request $request){

        $this->validate($request,[
            'deliverydate' => 'required',
            'customer' => 'required'
        ]);

        $customer = Contact::findOrFail($request->customer);
        $cart = Cart::create([
            'contact_id' => $customer->id,
            'company_id'=> $customer->company->id,
            'instruction' => request('instructions',''),
            'orderdate' => Carbon::now(),
            'deliverydate' => (new Carbon($request->deliverydate))->startOfDay()->toDateTimeString(),
            'status_id' => 1,
            'title' => request('title',''),
            'delete_flag' => 'n'
        ]);

        $cartitems = Cache::get('cart', []);

        $quote = Quote::create([
            'cart_id' => $cart->id,
            'contact_id' => $customer->id,
            'company_id'=> $customer->company->id,
            'instruction' => request('instructions',''),
            'orderdate' => Carbon::now(),
            'deliverydate' => (new Carbon($request->deliverydate))->startOfDay()->toDateTimeString(),
            'totalquoteprice' => 0,
            'expirydate' => (new Carbon($request->deliverydate))->addYear(),
            'status_id' => 1,
            'title' => request('title',''),
            'delete_flag' => 'n'
        ]);

        foreach ($cartitems as $key => $item) {

            $itemproduct = Product::find($item['product']);
            $cartitems[$key]['product'] = $itemproduct;
            if($item['supplier'] && Supplier::find($item['supplier'])){
                $supplieroffer = $itemproduct->prices->where('company_id','=',$item['supplier'])->first();
                $cartitems[$key]['unitprice'] = $itemproduct->wholesalequantity > $cartitems[$key]['qty'] ? $supplieroffer->retailprice : $supplieroffer->wholesaleprice;
                $cartitems[$key]['supplier'] = Supplier::find($item['supplier']);


            }else{
                $cartitems[$key]['unitprice'] = $itemproduct->srp;
            }

            $cartdetail = CartDetail::create([
                'cart_id' => $cart->id,
                'product_id' => $item['product'],
                'quantity' => $item['qty'],
                'delete_flag'
            ]);

            $quotedetail = QuoteDetail::create([
                'quote_id' => $quote->id,
                'cartdetail_id' => $cartdetail->id,
                'product_id' => $itemproduct->id,
                'quantity' => $item['qty'],
                'unitprice' => $cartitems[$key]['unitprice'],
                'totalprice' => $cartitems[$key]['unitprice'] * $item['qty'],
                'delete_flag' => 'n'
            ]);

            DB::table('supplier_quotedetail')->insert([
                'quote_id' => $quote->id,
                'quotedetail_id' => $quotedetail->id,
                'supplier_id' => $item['supplier'],
                'delete_flag' => 'n'
            ]);
        }

        $customitems = Cache::get('custom', []);

        foreach ($customitems as $key => $item) {
            $category = Category::where('name','LIKE',$item['general'])->first();

            $cartcustom = CartCustom::create([
                'cart_id' =>  $cart->id,
                'category_id' => $category->id,
                'quantity' => $item['qty'],
                'customname' => $item['name'],
                'delete_flag' => 'n'
            ]);

            QuoteCustom::create([
                'quote_id' => $quote->id,
                'cartcustom_id' => $cartcustom->id,
                'category_id' => $category->id,
                'quantity' => $item['qty'],
                'customname' => $item['name'],
                'delete_flag' => 'n'
            ]);

        }

        $quote->update([
            'totalquoteprice' => collect($cartitems)->reduce(function($carry,$item){
                return $carry + ($item['unitprice'] * $item['qty']);
            }) ?: 0
        ]);

        Cache::forget('cart');
        Cache::forget('custom');

        if($request->has('print') && $request->get('print') == 'true'):
            return redirect(route('quote.regular.edit.print',$quote))->with('print','true');
        endif;

        return back()->with('status','Quote Created Successfully');
    }

    public function regulardestroy(Request $request,Quote $quote){
        foreach ($quote->quoteDetails as $key => $detail) {
            if($detail->cartdetail()->count()){
                if($detail->cartdetail->cart()->count()){
                    $detail->cartdetail->cart->update([
                        'delete_flag' => 'y'
                    ]);
                }
                $detail->cartdetail->update([
                    'delete_flag' => 'y'
                ]);
            }

            $detail->update([
                'delete_flag' => 'y'
            ]);
        }

        $quote->update([
            'delete_flag' => 'y'
        ]);

        return back()->with('status','Quote deleted successfully');
    }

    public function regulardestroyBatch(Request $request){

        $ids = explode(',',$request->get('ids'));

        $quotes = Quote::whereIn('id', $ids)
        ->get();
        foreach ($quotes as $key => $quote) {
            $quote->update([
                'delete_flag' => 'y',
            ]);
        }
        return back()->with('status','Quotes deleted successfully');
    }

    public function regularedit(Request $request,Quote $quote){

        $customer = false;
        $category = false;
        $products = false;
        $product = false;
        $categories = Category::orderBy('name','asc')->get()->groupBy('categorylevel_id');
        $customers = Contact::select(['id','name'])->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });

        if($request->has('main') && $request->get('main')!='All'){

            $parent_id = Category::where('name','LIKE',$request->get('main'))->firstOrFail()->id;
            $category = $parent_id;
            $categories['2'] = $categories['2']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });
        }

        if($request->has('general') && $request->get('general')!='All'){
            $parent_id = Category::where('name','=',$request->get('general'))->firstOrFail()->id;
            $category = $parent_id;
            $categories['3'] = $categories['3']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });
        }
        if($request->has('brand') && $request->get('brand')!='All'){
            $parent_id = Category::where('name','=',$request->get('brand'))->firstOrFail()->id;
            $category = $parent_id;
        }

        if($category){
            $products = Product::where('category_id','=',$category)->orderBy('name')->get();
        }else{
            $products = Product::orderBy('name')->get();
        }

        if($request->has('product')){
            $product = Product::findOrFail($request->product);
        }

        if($request->has('customer')){
            $customer = Contact::findOrFail($request->customer);
        }

        return view('quote-regular-edit',compact([
            'categories',
            'customers',
            'customer',
            'products',
            'product',
            'quote'
        ]));
    }


    public function regularaddcartitem(Request $request,Quote $quote){

        $this->validate($request,[
            'product' => 'required',
            'supplier' => 'required',
            'qty' => 'required',
        ]);

        $unitprice = 0;
        $supplier = null;
        $itemproduct = Product::find($request->product);
        if($request->supplier && Supplier::find($request->supplier)){
            $supplieroffer = $itemproduct->prices->where('company_id','=',$request->supplier)->first();
            $unitprice = $itemproduct->wholesalequantity > $request->qty ? $supplieroffer->retailprice : $supplieroffer->wholesaleprice;
        }else{
            $unitprice = $itemproduct->srp;
        }

        $cartdetail = CartDetail::updateOrCreate([
            'cart_id' => $quote->cart_id,
            'product_id' => $itemproduct->id,
        ],
        [
            'quantity' => $request->qty,
            'delete_flag' => 'n'
        ]);

        QuoteDetail::updateOrCreate([
            'quote_id' => $quote->id,
            'cartdetail_id' => $cartdetail->id,
            'product_id' => $itemproduct->id
        ],
        [
            'quantity' => $request->qty,
            'unitprice' => $unitprice,
            'totalprice' => $unitprice * $request->qty,
            'delete_flag' => 'n'
        ]);


        return back();

    }

    public function regularaddcartedit(Request $request,Quote $quote){
        $this->validate($request,[
            'item' => 'required',
            'qty' => 'required',
        ]);


        $quotedetail = QuoteDetail::where([
            ['product_id','=',$request->item],
            ['quote_id','=',$quote->id],
        ])
        ->first();

        $unitprice = 0;
        $supplier = null;
        $itemproduct = Product::find($request->item);
        if($quotedetail->supplier->count()){
            $supplieroffer = $itemproduct->prices->where('company_id','=',$quotedetail->supplier->id)->first();
            $unitprice = $itemproduct->wholesalequantity > $request->qty ? $supplieroffer->retailprice : $supplieroffer->wholesaleprice;
        }else{
            $unitprice = $itemproduct->srp;
        }

        $quotedetail->update([
            'quantity' => $request->qty,
            'unitprice' => $unitprice,
            'totalprice' => $unitprice * $request->qty,
        ]);


        CartDetail::find($quotedetail->cartdetail_id)->first()
        ->update([
            'quantity' => $request->qty
        ]);

        return back();

    }

    public function regularaddcartdelete(Request $request,Quote $quote){
        $this->validate($request,[
            'item' => 'required',
        ]);

        $quotedetail = QuoteDetail::where([
            ['product_id','=',$request->item],
            ['quote_id','=',$quote->id],
        ])
        ->first();

        if($quotedetail){
            $quotedetail->update([
                'delete_flag' => 'y'
            ]);
    
            $cartdetail = CartDetail::find($quotedetail->cartdetail_id);

            if($cartdetail->first()){
                $cartdetail->first()
                ->update([
                    'delete_flag' => 'y'
                ]);
            }
        }


        return back();

    }

    public function regularupdate(Request $request,Quote $quote){

        $this->validate($request,[
            'deliverydate' => 'required',
            'customer' => 'required'
        ]);

        $customer = Contact::findOrFail($request->customer);

        $quote->update([
            'contact_id' => $customer->id,
            'company_id'=> $customer->company->id,
            'instruction' => request('instructions',''),
            'deliverydate' => (new Carbon($request->deliverydate))->startOfDay()->toDateTimeString(),
            'expirydate' => (new Carbon($request->deliverydate))->addYear(),
            'title' => request('title',''),
        ]);


        return back()->with('status','Quote Updated Successfully');
    }

    public function regularaddcustomitem(Request $request){

        $categories = Category::orderBy('name','asc')->get()->groupBy('categorylevel_id');

        if($request->has('main') && $request->get('main')!='All'){

            $parent_id = Category::where('name','LIKE',$request->get('main'))->firstOrFail()->id;
            $category = $parent_id;
            $categories['2'] = $categories['2']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });
        }

        if($request->has('general') && $request->get('general')!='All'){
            $parent_id = Category::where('name','=',$request->get('general'))->firstOrFail()->id;
            $category = $parent_id;
            $categories['3'] = $categories['3']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });
        }
        return view('quote-regular-add-custom-item',compact([
            'categories'
        ]));
    }

    public function regularaddcustomitemstore(Request $request){

        $this->validate($request,[
            'general' => 'required',
            'name' => 'required',
            'qty' => 'required',
        ]);

        if($request->has('tmp')){


            $cart = Cache::get('custom', []);

            $exists = false;
            foreach ($cart as $key => $item) {
                if($item['general'] == $request->general && $item['name'] == $request->name){
                    $cart[$key]['qty'] = $request->qty;
                    $exists = true;
                    break;
                }
            }

            if(!$exists){
                $cart[] = [
                    'general' => $request->general,
                    'name' => $request->name,
                    'qty' => $request->qty,
                ];
            }

            Cache::put('custom', $cart, 60);
            $url = parse_url(urldecode(urldecode($request->redirect)));
            $params = [];
            if(isset($url['query'])){
                parse_str($url['query'],$params);
            }
            $url = url($url['path']) . '?' . http_build_query($params);

            return redirect()->intended($url);
        }

        if($request->has('quote')){
            $quote = Quote::findOrFail($request->get('quote'));
            $category = Category::where('name','LIKE',$request->general)->first();

            $cartcustom = CartCustom::updateOrCreate([
                'cart_id' => $quote->cart_id,
                'category_id' => $category->id,
                'customname' => $request->name,
            ],[
                'quantity' => $request->qty,
                'delete_flag' => 'n'
            ]);

            QuoteCustom::updateOrCreate([
                'quote_id' => $quote->id,
                'cartcustom_id' => $cartcustom->id,
                'category_id' => $category->id,
                'customname' => $request->name,
            ],[
                'quantity' => $request->qty,
                'delete_flag' => 'n'
            ]);

            return redirect()->route('quote.regular.edit',[
                'quote' => $quote
            ]);

        }


    }

    public function regularassign(Request $request){
        $this->validate($request,[
            'product' => 'required',
            'supplier' => 'required',
            'redirect' => 'required',
        ]);
        if($request->tmp){

            $cart = Cache::get('cart', []);

            $exists = false;
            foreach ($cart as $key => $item) {
                if($item['product'] == $request->product){
                    $cart[$key]['supplier'] = $request->supplier;
                    $exists = true;
                    break;
                }
            }

            Cache::put('cart', $cart, 60);

        }else{

            $quote = Quote::find($request->quote);
            $quotedetail = QuoteDetail::find($request->quotedetail);
            $supplier = Supplier::find($request->supplier);
            $product = Product::find($request->product);

            $offer = $product->prices()->where('company_id','=',$supplier->id)->first();

            $unitprice = $product->wholesalequantity > $request->qty ? $offer->retailprice : $offer->wholesaleprice;

            $quotedetail->update([
                'unitprice' => $unitprice,
                'totalprice' => $unitprice * $quotedetail->quantity
            ]);

            if(!$quotedetail->supplier->count()){
                $supplier = DB::table('supplier_quotedetail')->insert([
                    'quote_id' => $quote->id,
                    'quotedetail_id' => $quotedetail->id,
                    'supplier_id' => $supplier->id,
                    'delete_flag' => 'n'
                ]);
            }
            else{
                $supplier = DB::table('supplier_quotedetail')->where([
                    'quote_id' => $quote->id,
                    'quotedetail_id' => $quotedetail->id,
                ])
                ->update([
                    'supplier_id' => $supplier->id,
                ]);
            }

        }

        $url = parse_url(urldecode(urldecode($request->redirect)));
        $params = [];
        if(isset($url['query'])){
            parse_str($url['query'],$params);
        }
        $url = url($url['path']) . '?' . http_build_query($params);

        return redirect()->intended($url);
    }


    public function customindex(Request $request)
    {

        $limit = 20;
        $sort = 'asc';

        $quotecustoms = QuoteCustom::select([
            'quotecustom.*',
        ]);

        $categories = Category::orderBy('name','asc')->get()->groupBy('categorylevel_id');

        if($request->has('main') && $request->get('main')!='All'){

            $parent_id = Category::where('name','LIKE',$request->get('main'))->firstOrFail()->id;
            $category = $parent_id;
            $categories['2'] = $categories['2']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });
        }

        if($request->has('general') && $request->get('general')!='All'){
            $parent_id = Category::where('name','=',$request->get('general'))->firstOrFail()->id;
            $category = $parent_id;
            $categories['3'] = $categories['3']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });

            $quotecustoms->where('category_id', '=',$request->get('general'));

        }

        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('sortdir') && $request->has('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }
        if($request->has('orderdatefrom')){
            $quotecustoms->whereHas('quote',function ($query) use($request){
                $query->where('quote.orderdate', '>=',(new Carbon($request->orderdatefrom))->startOfDay()->toDateTimeString());
            });
        }

        if($request->has('orderdateto')){
            $quotecustoms->whereHas('quote',function ($query) use($request){
                $query->where('quote.orderdate', '<=',(new Carbon($request->orderdateto))->endOfDay()->toDateTimeString());
            });
        }

        if($request->has('deliverydatefrom')){
            $quotecustoms->whereHas('quote',function ($query) use($request){
                $query->where('quote.deliverydate', '>=',(new Carbon($request->deliverydatefrom))->startOfDay()->toDateTimeString());
            });
        }

        if($request->has('deliverydateto')){
            $quotecustoms->whereHas('quote',function ($query) use($request){
                $query->where('quote.deliverydate', '<=',(new Carbon($request->deliverydateto))->endOfDay()->toDateTimeString());
            });
        }

        if($request->has('customer')){
            $quotecustoms = $quotecustoms->whereHas('quote.contact',function ($query) use($request){
                $query->where('id', '=',$request->get('customer'));
            });
        }
        if($request->has('supplier')){
            $quotecustoms = $quotecustoms->whereHas('supplier',function ($query) use($request){
                $query->where('supplier_id', '=',$request->get('supplier'));
            });
        }

        if($request->has('status')){
            $quotecustoms = $quotecustoms->whereHas('status',function ($query) use($request){
                $query->where('id', '=',$request->get('status'));
            });
        }

        if($request->has('q')){
            $quotecustoms = $quotecustoms->where('quotecustom.customname','like','%' . $request->get('q') . '%');
        }

        if($request->has('sortby') && $request->has('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'deliverydate':
                $quotecustoms = $quotecustoms->join('quote as q','q.id','=','quotecustom.quote_id')
                ->orderBy('q.deliverydate', $sort);
                break;
                case 'quote':
                $quotecustoms = $quotecustoms->orderBy('quotecustom.quote_id', $sort);
                break;
                case 'description':
                $quotecustoms = $quotecustoms->orderBy('quotecustom.customname', $sort);
                break;
                case 'general':
                $quotecustoms = $quotecustoms->join('category as c1','c1.id','=','quotecustom.category_id')
                ->orderBy('c1.name', $sort);
                break;
                case 'main':
                $quotecustoms = $quotecustoms->join('category as c1','c1.id','=','quotecustom.category_id')
                ->join('category as c2','c2.id','=','c1.parent_id')
                ->orderBy('c2.name', $sort);
                break;
                case 'qty':
                $quotecustoms = $quotecustoms->orderBy('quotecustom.quantity', $sort);
                break;
                case 'customer':
                $quotecustoms = $quotecustoms->leftJoin('quote as q','q.id','=','quotecustom.quote_id')
                ->leftJoin('contact as ct','ct.id','=','q.contact_id')
                ->orderBy('ct.name', $sort);
                break;
                case 'company':
                $quotecustoms =$quotecustoms->leftJoin('quote as q','q.id','=','quotecustom.quote_id')
                ->leftJoin('company as cp','cp.id','=','q.company_id')
                ->orderBy('cp.name', $sort);
                break;
                case 'supplier':
                $quotecustoms = $quotecustoms->leftJoin('quote as q','q.id','=','quotecustom.quote_id')
                ->leftJoin('supplier_quotecustom as sq','sq.quote_id','=','q.id')
                ->leftJoin('company as cp','cp.id','=','sq.supplier_id')
                ->addSelect('cp.name as suppliername')
                ->orderBy('cp.name', $sort);
                break;
                case 'status':
                $quotecustoms = $quotecustoms->leftJoin('quote as q','q.id','=','quotecustom.quote_id')
                ->join('quotestatus as qs','qs.id','=','q.status_id')
                ->orderBy('qs.name', $sort);
                break;
                case 'orderdate':
                default:
                $quotecustoms = $quotecustoms->join('quote as q','q.id','=','quotecustom.quote_id')
                ->orderBy('q.orderdate', $sort);
                break;
            }
        }else{
            $quotecustoms = $quotecustoms->join('quote as q','q.id','=','quotecustom.quote_id')
            ->orderBy('q.orderdate', $sort);
        }


        if($request->has('export')){
            $titles = [
                "Order Date",
                "Description",
                "Main",
                "General",
                "Quantity",
                "Quote ID",
                "Customer Name",
                "Company",
                "Supplier",
                "Status",
            ];

            $quotecustoms = $quotecustoms->get()->map(function ($item, $key) use($titles) {
                return [
                    $item->quote->orderdate->timestamp <= 0 ? '-' : $item->quote->orderdate->format('M d, Y'),
                    $item->customname,
                    $item->category->parent->name,
                    $item->category->name,
                    $item->quantity,
                    str_pad($item->quote->id, 6, "0", STR_PAD_LEFT),
                    $item->quote->contact->name,
                    $item->quote->company->name,
                    $item->supplier && $item->supplier->count() > 0 ? $item->supplier->first()->name : '-',
                    $item->quote->status->name,
                ];
            });

            $quotecustoms = $quotecustoms->toArray();
            Excel::create('export-quote-custom' . date('YmdHisa'), function($excel) use($quotecustoms,$titles){

                $excel->setTitle('Quote Custom Table Export');

                $excel->sheet('Results', function($sheet) use($quotecustoms,$titles){
                    $sheet->fromArray($quotecustoms, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })
                ->export('xls');

            });

            return back();
        }else{
            $quotecustoms = $quotecustoms->paginate($limit);
        }

        $customers = Contact::select(['id','name'])->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $companies = Company::select(['id','name'])->where('supplier_flag','=','n')->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $suppliers = Supplier::select(['id','name'])->where('supplier_flag','=','y')->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });
        $statuses = QuoteStatus::select(['id','name'])->orderBy('name')->get()->mapWithKeys(function ($item) {
            return [$item['id'] => $item['name']];
        });


        return view('quote-custom',compact([
            'stats',
            'quotecustoms',
            'customers',
            'categories',
            'companies',
            'suppliers',
            'statuses'
        ]));
    }

    public function customdestroy(Request $request,QuoteCustom $quote){
        $quote->update([
            'delete_flag' => 'y'
        ]);

        return back()->with('status','Quote deleted successfully');
    }

    public function customdestroyBatch(Request $request){

        $ids = explode(',',$request->get('ids'));

        $quotes = Quote::whereIn('id', $ids)
        ->get();
        foreach ($quotes as $key => $quote) {
            $quote->update([
                'delete_flag' => 'y',
            ]);
        }
        return back()->with('status','Quotes deleted successfully');
    }
}
