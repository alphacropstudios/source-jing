<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\BusinessType;
use App\Address;
use App\Account;
use App\ContactDetails;
use App\Contact;
use App\Country;
use App\Product;
use App\Category;
use App\CompanyImage;
use App\ImageType;
use App\Review;
use Hash;
use DB;
use Carbon\Carbon;
use Excel;
use Image;

use App\Mail\CompanyActivation;
use Illuminate\Support\Facades\Mail;
use Faker\Factory as Faker;

class CompanyController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['activateaccount','activateaccountsuccess']]);
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

        $companies = Company::select('company.*')->where('company.delete_flag','NOT LIKE','y');;

        $limit = 20;
        $sort = 'asc';

        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('sortdir') && $request->get('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }

        if($request->has('q')){
            $companies = $companies->where('name','like','%' . $request->get('q') . '%');
        }

        if($request->has('type') && $request->get('type')!='All'){
            $companies = $companies->whereHas('businesstype',function($query) use($request){
                $query->where('name','=',$request->get('type'));
            });
        }

        if($request->has('state') && $request->get('state')!='All'){
            $companies = $companies->whereHas('address',function($query) use($request){
                $query->where('state','=',$request->get('state'));
            });
        }

        if($request->has('city') && $request->get('city')!='All'){
            $companies = $companies->whereHas('address',function($query) use($request){
                $query->where('city','=',$request->get('city'));
            });
        }

        if($request->has('supplier') && $request->get('supplier')!='All'){
            $companies = $companies->where('supplier_flag','=',$request->get('supplier'));
        }

        if($request->has('sortby') && $request->get('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'name':
                $companies = $companies->orderBy('company.name', $sort);
                break;

                case 'type':
                $companies = $companies->join('businesstype as b','b.id','=','company.businesstype_id')
                ->orderBy('b.name', $sort);
                break;

                case 'state':
                $companies = $companies->join('address as a','a.company_id','=','company.id')
                ->orderBy('a.state', $sort);
                break;

                case 'city':
                $companies = $companies->join('address as a','a.company_id','=','company.id')
                ->orderBy('a.city', $sort);
                break;

                case 'supplier':
                $companies = $companies->orderBy('company.supplier_flag', $sort);
                break;

                default:
                # code...
                break;
            }
        }else{
            $companies = $companies->orderBy('company.name', $sort);
        }


        if($request->has('export')){
            $titles = [
                "Name",
                "Type of Business",
                "City",
                "Province",
                "Supplier",
                "Status"
            ];

            $companies = $companies->get()->map(function ($item, $key) use($titles) {
                return [
                    $item->name,
                    $item->businesstype ? $item->businesstype->name : '-',
                    $item->address ? $item->address->city : '-',
                    $item->address ? $item->address->state : '-',
                    $item->isSupplier(),
                    $item->isEnabled(),
                ];
            });

            $companies = $companies->toArray();
            Excel::create('export-company-' . date('YmdHisa'), function($excel) use($companies,$titles){

                $excel->setTitle('Company Table Export');

                $excel->sheet('Results', function($sheet) use($companies,$titles){
                    $sheet->fromArray($companies, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })
                ->export('xls');

            });

            return back();
        }else{
            $companies = $companies->paginate($limit);
        }


        $businesstypes = BusinessType::all();
        $city = DB::table('city')->select(['name'])->distinct()->get();
        $state = DB::table('state')->select(['name'])->distinct()->get();

        return view('company',compact(['companies','businesstypes','city','state']));
    }

    public function create(){
        $city = DB::table('city')->select(['name'])->distinct()->get();
        if(!$city->count()){
            $city->push((object)[
                'name' => 'None'
            ]);
        }
        $state = DB::table('state')->select(['name'])->distinct()->get();

        if(!$state->count()){
            $state->push((object)[
                'name' => 'None'
            ]);
        }


        $country = Country::orderBy('name')->get(['name','id']);
        $businesstypes = BusinessType::orderBy('name')->get(['name','id']);
        return view('company-add',compact([
            'city',
            'state',
            'businesstypes',
            'country'
        ]));
    }

    public function store(Request $request){

        $this->validate($request,[
            'username' => 'required|unique:users,username',
            'password' => 'required|confirmed',
            'companyname' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'contact' => 'required',
            'email' => 'required|email',
        ]);

        $company = Company::create([
            'name' => $request->companyname,
            'blacklisted_flag' => 'n',
            'enabled_flag' => 'y',
            'supplier_flag' => $request->supplier == 'Yes' ? 'y' : 'n',
            'vat' => $request->vat,
            'businesstype_id' => $request->businesstype,
            'businessregistration' => $request->registration,
            'delete_flag' => 'n',
        ]);

        $user = Account::create([
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'activation_code' => str_random(16),
            'company_id' => $company->id,
            'delete_flag' => 'n',
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'email' => '',
            'activation_code' => '',
            'forgotten_password_code' => time(),
            'forgotten_password_time' => time(),
            'remember_code' => '',
            'created_on' => time(),
            'last_login' => time(),
            'active' => 1,
            'first_name' => '',
            'last_name' => '',
            'company_id' => $company->id,
            'company' => $company->name,
            'phone' => ''

        ]);

        $contact = Contact::create([
            'company_id' => $company->id,
            'name' => $request->contact,
            'designation' => $request->designation,
            'primary_flag' => 'y',
            'enabled_flag' => 'y',
            'delete_flag' => 'n',
        ]);

        if($request->has('contact2')){
            $this->validate($request,[
                "contact2" => 'required',
                "contact_number2" => 'required',
                "email2" => 'required|email',
                "designation2" => 'required',
            ]);

            $contact2 = Contact::create([
                'company_id' => $company->id,
                'name' => $request->contact2,
                'designation' => $request->designation2,
                'primary_flag' => 'n',
                'enabled_flag' => 'y',
                'delete_flag' => 'n',
            ]);

            if($request->has('contact_number2')){
                ContactDetails::create([
                    'contact_id' => $contact2->id,
                    'name' => $request->get('contact_number2'),
                    'contacttype_id' => 2,
                    'delete_flag' => 'n',
                ]);
            }

            if($request->has('email2')){
                ContactDetails::create([
                    'contact_id' => $contact2->id,
                    'name' => $request->get('email2'),
                    'contacttype_id' => 1,
                    'delete_flag' => 'n',
                ]);
            }
        }

        if($request->has('contact_number')){
            ContactDetails::create([
                'contact_id' => $contact->id,
                'name' => $request->get('contact_number'),
                'contacttype_id' => 2,
                'delete_flag' => 'n',
            ]);
        }

        if($request->has('email')){
            ContactDetails::create([
                'contact_id' => $contact->id,
                'name' => $request->get('email'),
                'contacttype_id' => 1,
                'delete_flag' => 'n',
            ]);
        }


        if($request->hasFile('upload') && $request->file('upload')->isValid()){
            $imageType = ImageType::firstOrCreate([
                'name' => 'Company Location',
                'description' => 'Image of location of the office of the company',
            ]);

            $this->validate($request,[
                'upload' => 'required|file'
            ]);

            $filename = str_slug($company->name) . '-' . str_slug($imageType->name) . '-' . date('ymdhis') . '.jpg';

            $img = Image::make($_FILES['upload']['tmp_name']);
            $img->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save(public_path('uploads/' . $filename), 90);

            $image = CompanyImage::create([
                'company_id' => $company->id,
                'filename' => $filename,
                'domain' => env('APP_URL','http://localhost'),
                'filelocation' => '/uploads/',
                'imagetype_id' => $imageType->id,
                'primaryimage_flag' => 'y',
                'delete_flag' => 'n'
            ]);
        }

        $address = Address::create([
            'company_id' => $company->id,
            'street' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'country_id' => $request->country,
            'zipcode' => $request->zipcode,
        ]);

        return back()->with('status','Company added successfully');
    }

    public function edit(Company $company){
        $city = DB::table('city')->select(['name'])->distinct()->get();
        if(!$city->count()){
            $city->push((object)[
                'name' => 'None'
            ]);
        }
        $state = DB::table('state')->select(['name'])->distinct()->get();

        if(!$state->count()){
            $state->push((object)[
                'name' => 'None'
            ]);
        }


        $country = Country::orderBy('name')->get(['name','id']);
        $businesstypes = BusinessType::orderBy('name')->get(['name','id']);
        return view('company-edit',compact([
            'company',
            'city',
            'state',
            'businesstypes',
            'country'
        ]));
    }

    public function update(Company $company,Request $request){

         if(!empty($request->file('upload')) && !$request->file('upload')->isValid()){
             $error = str_after($request->file('upload')->getErrorMessage(), 'exceeds');

             return back()->withErrors([
                 'File exceeds' . $error
             ]);
         }

        $this->validate($request,[
            'username' => 'required|unique:users,username,'.$company->account->id.',id',
            'companyname' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'contact' => 'required',
            'upload' => 'file|image|size:2048'
        ],[
            'upload.file' => 'The file you uploaded is not a valid image',
            'upload.image' => 'The file you uploaded is not a valid image',
            'upload.size' => 'Please upload an image not greater than :size kb'
        ]);

        $company->update([
            'name' => $request->companyname,
            'supplier_flag' => $request->supplier,
            'vat' => $request->vat,
            'businesstype_id' => $request->businesstype,
            'businessregistration' => $request->registration,
        ]);

        $company->account->update([
            'username' => $request->username,
        ]);

        $company->contact->update([
            'name' => $request->contact,
            'designation' => $request->designation,
        ]);

        $imageType = ImageType::firstOrCreate([
            'name' => 'Primary',
            'description' => 'Primary Image of the Product',
        ]);

        if($request->has('contact_number')){
            ContactDetails::firstOrCreate([
                'contact_id' => $company->contact->id,
                'name' => $request->get('contact_number'),
                'contacttype_id' => 2,
                'delete_flag' => 'n',
            ]);
        }

        if($request->has('email')){
            ContactDetails::firstOrCreate([
                'contact_id' => $company->contact->id,
                'name' => $request->get('email'),
                'contacttype_id' => 1,
                'delete_flag' => 'n',
            ]);
        }

        $company->address->update([
            'street' => $request->address,
            'city' => $request->city,
            'state' => $request->state,
            'country_id' => $request->country,
            'zipcode' => $request->zipcode,
        ]);



        if($request->hasFile('upload') && $request->file('upload')->isValid()){
            $this->validate($request,[
                'upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);

            $imageType = ImageType::firstOrCreate([
                'name' => 'Company Location',
                'description' => 'Image of location of the office of the company',
            ]);

            $filename = str_slug($company->name) . '-' . str_slug($imageType->name) . '-' . date('ymdhis') . '.jpg';

            $img = Image::make($_FILES['upload']['tmp_name']);
            $img->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save(public_path('uploads/' . $filename), 90);

            $image = $company->image()->updateOrCreate([
                'company_id' => $company->id,
            ],[
                'filename' => $filename,
                'domain' => env('APP_URL','http://localhost'),
                'filelocation' => '/uploads/',
                'imagetype_id' => $imageType->id,
                'primaryimage_flag' => 'y',
                'delete_flag' => 'n'
            ]);

            dd($image);

        }else{
            return back()->withErrors(['Upload failed. ' . $request->file('upload')->getErrorMessage() ]);
        }

        return back()->with('status','Company updated successfully');
    }

    public function destroy(Request $request,Company $company){

        $company->update([
            'delete_flag' => 'y',
        ]);

        return back()->with('status','Company deleted successfully');
    }

    public function destroyBatch(Request $request){

        $ids = explode(',',$request->get('ids'));

        $companies = Company::whereIn('id', $ids)
        ->get();
        foreach ($companies as $key => $company) {
            $company->update([
                'delete_flag' => 'y',
            ]);
        }
        return back()->with('status','Companies deleted successfully');
    }

    public function contacts(Request $request,Company $company){
        $limit = 20;
        $contacts = $company->contacts()->where('delete_flag','NOT LIKE','y');
        $designations = Contact::distinct('designation')->orderBy('designation')->get(['designation']);

        if($request->has('export')){
            $titles = [
                'Name',
                'Designation',
                'Phone Number',
                'Email',
                'Primary Contact',
                'Status',
            ];

            $contacts = $contacts->get()->map(function ($contact, $key) use($titles) {
                return [
                    $contact->name,
                    $contact->designation,
                    $contact->details()->where('contacttype_id',2)->first()->name ?: '-',
                    $contact->details()->where('contacttype_id',1)->first()->name ?: '-',
                    $contact->primary_flag =='y' ? 'Yes' : 'No' ,
                    $contact->enable_flag=='y' ? 'Active' : 'Inactive',
                ];
            });

            $contacts = $contacts->toArray();
            Excel::create('export-company-contacts-' . date('YmdHisa'), function($excel) use($contacts,$titles){

                $excel->setTitle('Company Contact Table Export');

                $excel->sheet('Results', function($sheet) use($contacts,$titles){
                    $sheet->fromArray($contacts, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })
                ->export('xls');

            });

            return back();
        }else{
            $contacts = $contacts->paginate($limit);
        }

        return view('company-contact',compact(['company','contacts','designations']));
    }

    public function contactadd(Request $request,Company $company){

        return view('company-contactadd',compact(['company']));
    }
    public function contactstore(Request $request,Company $company){
        $this->validate($request,[
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'designation' => 'required|string',
            'primary' => 'required',
        ]);

        $fullname = implode(' ',[
            $request->first_name,
            $request->middle_name ?: null,
            $request->last_name,
        ]);

        $contact = Contact::create([
            'company_id' => $company->id,
            'name' => $fullname,
            'lastname' => $request->last_name,
            'firstname' => $request->first_name,
            'middlename' => $request->middle_name ?: ' ',
            'designation' => $request->designation,
            'primary_flag' => $request->primary,
            'enable_flag' => 'y',
            'delete_flag' => 'n',
        ]);

        foreach ([ 'email', 'default_phone', 'home_phone', 'work_phone', 'mobile_phone' ] as $key => $value) {
            if($request->has($value)){
                ContactDetails::firstOrCreate([
                    'contact_id' => $contact->id,
                    'contacttype_id' => $key + 1,
                ],[
                    'name' => $request->get($value),
                    'delete_flag' => 'n',
                ]);
            }else{
                ContactDetails::firstOrCreate([
                    'contact_id' => $contact->id,
                    'contacttype_id' => $key + 1,
                ],[
                    'name' => '',
                    'delete_flag' => 'n',
                ]);
            }
        }

        return back()->with('status','Company contact added successfully');
    }

    public function contactedit(Request $request,Company $company,Contact $contact){
        return view('company-contactedit',compact(['company','contact']));
    }
    public function contactupdate(Request $request,Company $company,Contact $contact){
        $this->validate($request,[
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'designation' => 'required|string',
            'primary' => 'required',
        ]);

        $fullname = implode(' ',[
            $request->first_name,
            $request->middle_name ?: null,
            $request->last_name,
        ]);

        $contact->update([
            'name' => $fullname,
            'lastname' => $request->last_name,
            'firstname' => $request->first_name,
            'middlename' => $request->middle_name ?: ' ',
            'designation' => $request->designation,
            'primary_flag' => $request->primary,
        ]);

        foreach ([ 'email', 'default_phone', 'home_phone', 'work_phone', 'mobile_phone' ] as $key => $value) {
            if($request->has($value)){
                ContactDetails::updateOrCreate([
                    'contact_id' => $contact->id,
                    'contacttype_id' => $key + 1,
                ],[
                    'name' => $request->get($value),
                ]);
            }else{
                ContactDetails::updateOrCreate([
                    'contact_id' => $contact->id,
                    'contacttype_id' => $key + 1,
                ],[
                    'name' => '',
                ]);
            }
        }

        return back()->with('status','Company contact updated successfully');
    }

    public function contactdestroy(Request $request,Company $company,Contact $contact){

        $contact->update([
            'delete_flag' => 'y',
        ]);

        return back()->with('status','Contact deleted successfully');
    }

    public function contactdestroyBatch(Request $request,Company $company){

        $ids = explode(',',$request->get('ids'));
        $contacts = Contact::whereIn('id', $ids)
        ->get();
        foreach ($contacts as $key => $contact) {
            $contact->update([
                'delete_flag' => 'y',
            ]);
        }
        return back()->with('status','Contacts deleted successfully');
    }
    public function blacklist(Request $request,Company $company){
        return view('company-blacklist',compact(['company']));
    }

    public function saveblacklist(Request $request,Company $company){

        $company->update([
            'blacklisted_flag' => 'y',
            'blacklistedremarks' => $request->message,
            'dateblacklisted' => date('Y-m-d H:i:s')
        ]);
        return redirect()->intended('company')->with('status','Company Blacklisted successfully');
    }

    public function account(Request $request,Company $company){
        return view('company-account',compact(['company']));
    }

    public function accountsave(Request $request,Company $company){

        $account = $company->account;

        if($account){
            if (!Hash::check($request->old_password, $account->password)) {
                return back()->withErrors(['Current Password Incorrect']);
            }

            if($request->has('username')){
                $this->validate($request,[
                    'username' => 'required|unique:users,username,' . $account->id . ''
                ]);

                $account->update([
                    'username' => $request->username,
                ]);
            }

            if($request->has('password')){
                $this->validate($request,[
                    'password' => 'required|confirmed'
                ]);

                $account->update([
                    'password' => bcrypt($request->password),
                ]);
            }


        }else{
            $this->validate($request,[
                'username' => 'required|unique:users,username',
                'password' => 'required|confirmed'
            ]);

            $faker = Faker::create();

            Account::create([
                'username' => $request->get('username'),
                'company_id' => $company->id,
                'activationcode' => $faker->ean8,
                'password' => bcrypt($request->get('password')),
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'email' => '',
                'created_on' => time(),
                'active' => 1,
                'company_id' => $company->id,
                'company' => $company->name,
            ]);
        }

        return back()->with('status','Company Account updated successfully');

    }

    public function product(Request $request,Company $company)
    {

        if(!$request->has('search')){
            if ($request->session()->has('added.product')) {
                $request->session()->forget('added.product');
            }
        }

        $limit = 20;
        $sort = 'asc';

        $products = $company->products()->select('product.*');
        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('sortdir') && $request->has('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }

        if($request->has('main') && $request->get('main') != 'All'){
            $products = $products->whereHas('category.parent.parent',function($query) use($request){
                $query->where('name','=',$request->get('main'));
            });
        }

        if($request->has('general') && $request->get('general') != 'All'){
            $products = $products->whereHas('category.parent',function($query) use($request){
                $query->where('name','=',$request->get('general'));
            });
        }

        if($request->has('brand') && $request->get('brand') != 'All'){
            $products = $products->whereHas('category',function($query) use($request){
                $query->where('name','=',$request->get('brand'));
            });
        }

        if($request->has('q')){
            $products = $products->where('name','like','%' . $request->get('q') . '%');
        }

        if($request->has('sortby') && $request->has('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'brand':
                $products = $products->join('category as c1','c1.id','=','product.category_id')
                ->orderBy('c1.name', $sort);
                break;
                case 'general':
                $products = $products->join('category as c1','c1.id','=','product.category_id')
                ->join('category as c2','c2.id','=','c1.parent_id')
                ->orderBy('c2.name', $sort);
                break;
                case 'main':
                $products = $products->join('category as c1','c1.id','=','product.category_id')
                ->join('category as c2','c2.id','=','c1.parent_id')
                ->join('category as c3','c3.id','=','c2.parent_id')
                ->orderBy('c3.name', $sort);
                break;
                case 'qty':
                $products = $products->orderBy('product.wholesalequantity', $sort);
                break;
                case 'srp':
                $products = $products->orderBy('product.srp', $sort);
                break;
                case 'status':
                $products = $products->orderBy('product.enable_flag', $sort);
                break;
                case 'name':
                default:
                $products = $products->orderBy('product.name', $sort);
                break;
            }
        }else{
            $products = $products->orderBy('product.name', $sort);
        }

        $products = $products->paginate($limit);

        $categories = Category::orderBy('name','asc')->get()->unique('name')->groupBy('categorylevel_id');
        return view('company-product',compact(['products','categories','company']));
    }

    public function sendactivation(Request $request,Company $company){
        return view('company-sendactivation',compact(['company']));
    }


    public function sendactivationemail(Request $request,Company $company){

        $this->validate($request,[
            'email' => 'required|email'
        ]);

        Mail::to($request->email)->send(new CompanyActivation($company));

        return back()->with('status','Activation Email Sent');
    }

    public function activateaccount(Request $request,$code){
        $account = Account::where('activation_code','=',$code)->firstOrFail();

        $account->update([
            'active' => 1
        ]);

        $account->office()->update([
            'enabled_flag' => 'y'
        ]);

        return redirect()->intended('company/activate/success')->with('status','Account activated successfully. You can now login your credentials');
    }

    public function deactivateaccount(Request $request,Company $company){

        $company->update([
            'enabled_flag' => 'n'
        ]);

        return back()->with('status','Company deactivated successfully');
    }
    public function whitelist(Request $request,Company $company){

        $company->update([
            'blacklisted_flag' => 'n'
        ]);

        return back()->with('status','Company whitelisted successfully');
    }

    public function activateaccountsuccess(Request $request){

        return view('activation-success');
    }

    public function reviews(Request $request){
        $limit = 20;
        $sort = 'desc';

        $reviews = Review::select('review.*');

        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('sortdir') && $request->get('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }

        if($request->has('company') && $request->get('company')!='All'){
            $reviews = $reviews->whereHas('company',function ($query) use($request){
                $query->where('id', '=',$request->get('company'));
            });
        }

        if($request->has('rating') && $request->get('sortby')!='All'){
            $reviews = $reviews->where('review.rating', '=',$request->get('rating'));
        }

        if($request->has('ratedby') && $request->get('ratedby')!='All'){
            $reviews = $reviews->whereHas('account.office',function ($query) use($request){
                $query->where('id', '=',$request->get('ratedby'));
            });
        }

        if($request->has('datefrom')){
            $reviews = $reviews->where('review.date', '>=',(new Carbon($request->datefrom))->startOfDay()->toDateTimeString());
        }

        if($request->has('dateto')){
            $reviews = $reviews->where('review.date', '<=',(new Carbon($request->dateto))->endOfDay()->toDateTimeString());
        }

        if($request->has('q')){
            $reviews = $reviews->where('review.remark','like','%' . $request->get('q') . '%');
        }

        if($request->has('sortby') && $request->get('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'company':
                $reviews = $reviews->leftJoin('company as cp','cp.id','=','review.company_id')
                ->orderBy('cp.name', $sort);
                break;
                case 'rating':
                $reviews = $reviews->orderBy('review.rating', $sort);
                break;
                case 'ratedby':
                $reviews = $reviews->leftJoin('users as u','u.id','=','review.ratedbyuser_id')
                ->leftJoin('company as cp','cp.id','=','u.company_id')
                ->orderBy('cp.name', $sort);
                break;
                default:
                case 'date':
                $reviews = $reviews->orderBy('review.date', $sort);
                break;
            }
        }else{
            $reviews = $reviews->orderBy('review.date', $sort);
        }

        if($request->has('export')){
            $titles = [
                'ID',
                'Company',
                'Rating',
                'Remark',
                'Date',
                'Rated By',
            ];


            $reviews = $reviews->get()->map(function ($review, $key) use($titles) {
                return [
                    str_pad($review->id, 6, "0", STR_PAD_LEFT),
                    $review->company->name,
                    $review->rating,
                    str_limit($review->remark,100),
                    $review->date->timestamp <= 0 ? '-' : $review->date->format('M d, Y'),
                    $review->account->office->name,
                ];
            });

            $reviews = $reviews->toArray();
            Excel::create('export-customer-review-' . date('YmdHisa'), function($excel) use($reviews,$titles){

                $excel->setTitle('Customer review');

                $excel->sheet('Results', function($sheet) use($reviews,$titles){
                    $sheet->fromArray($reviews, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })->export('xls');

            });

            return back();
        }

        $companies = Company::orderBy('name','asc')->get(['id','name'])->unique('name');
        // $users = Account::select(DB::raw('users.id , CONCAT(users.first_name," ",users.last_name) as name'))->orderBy('name','asc')->get(['id','name'])->unique('name');

        $users = Review::get()->unique('ratedbyuser_id')->map(function($review,$key){
            return $review->account->office;
        });

        $reviews = $reviews->paginate($limit);

        return view('customer-reviews',compact(['reviews','companies','users']));
    }

    public function reviewdestroy(Request $request,Review $review){

        $review->update([
            'delete_flag' => 'y',
        ]);

        return back()->with('status','Review deleted successfully');
    }

    public function reviewdestroyBatch(Request $request){

        $ids = explode(',',$request->get('ids'));
        $reviews = Review::whereIn('id', $ids)
        ->get();
        foreach ($reviews as $key => $review) {
            $review->update([
                'delete_flag' => 'y',
            ]);
        }
        return back()->with('status','Reviews deleted successfully');
    }
}
