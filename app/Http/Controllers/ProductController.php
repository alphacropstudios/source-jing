<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductImage;
use App\ImageType;
use App\Category;
use App\Supplier;
use Illuminate\Support\Facades\Input;
use Image;
use Excel;


class ProductController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {

        if(!$request->has('search')){
            if ($request->session()->has('added.product')) {
                $request->session()->forget('added.product');
            }
        }

        $limit = 20;
        $sort = 'asc';

        $products = Product::select('product.*');

        if($request->has('perpage')){
            $limit = $request->get('perpage');
        }

        if($request->has('sortdir') && $request->has('sortdir')!=''){
            switch ($request->get('sortdir')) {
                case 'asc':
                $sort = 'asc';
                break;
                default:
                $sort = 'desc';
                break;
            }
        }

        if($request->has('main') && $request->get('main') != 'All'){
            $products = $products->whereHas('category.parent.parent',function($query) use($request){
                $query->where('name','=',$request->get('main'));
            });
        }

        if($request->has('general') && $request->get('general') != 'All'){
            $products = $products->whereHas('category.parent',function($query) use($request){
                $query->where('name','=',$request->get('general'));
            });
        }

        if($request->has('brand') && $request->get('brand') != 'All'){
            $products = $products->whereHas('category',function($query) use($request){
                $query->where('name','=',$request->get('brand'));
            });
        }

        if($request->has('q')){
            $products = $products->where('product.name','like','%' . $request->get('q') . '%');
        }

        if($request->has('sortby') && $request->has('sortby')!=''){
            switch ($request->get('sortby')) {
                case 'brand':
                $products = $products->join('category as c1','c1.id','=','product.category_id')
                ->orderBy('c1.name', $sort);
                break;
                case 'general':
                $products = $products->join('category as c1','c1.id','=','product.category_id')
                ->join('category as c2','c2.id','=','c1.parent_id')
                ->orderBy('c2.name', $sort);
                break;
                case 'main':
                $products = $products->join('category as c1','c1.id','=','product.category_id')
                ->join('category as c2','c2.id','=','c1.parent_id')
                ->join('category as c3','c3.id','=','c2.parent_id')
                ->orderBy('c3.name', $sort);
                break;
                case 'qty':
                $products = $products->orderBy('product.wholesalequantity', $sort);
                break;
                case 'srp':
                $products = $products->orderBy('product.srp', $sort);
                break;
                case 'status':
                $products = $products->orderBy('product.enable_flag', $sort);
                break;
                case 'name':
                default:
                $products = $products->orderBy('product.name', $sort);
                break;
            }
        }else{
            $products = $products->orderBy('product.name', $sort);
        }

        if($request->has('export')){
            $titles = [
                "Name",
                "Main",
                "General",
                "Brand",
                "SRP",
                "Wholesale Quantity",
                "Status"
            ];

            $products = $products->get()->map(function ($item, $key) use($titles) {
                return [
                    $item->name,
                    $item->category->parent->parent->name,
                    $item->category->parent->name,
                    $item->category->name,
                    formatNumber($item->srp),
                    formatNumber($item->wholesalequantity,0),
                    $item->enable_flag == 'y' ? 'Active' : 'Inactive',
                ];
            });

            $products = $products->toArray();
            Excel::create('export-products-' . date('YmdHisa'), function($excel) use($products,$titles){

                $excel->setTitle('Products Table Export');

                $excel->sheet('Results', function($sheet) use($products,$titles){
                    $sheet->fromArray($products, null, 'A1', false, false);
                    $sheet->prependRow($titles);
                })
                ->export('xls');

            });

            return back();
        }else{
            $products = $products->paginate($limit);
        }







        $categories = Category::orderBy('name','asc')->get()->unique('name')->groupBy('categorylevel_id');
        return view('product',compact(['products','categories']));
    }

    public function create(Request $request){

        $categories = Category::orderBy('name','asc')->get()->groupBy('categorylevel_id');

        if($request->has('main') && $request->get('main')!='All'){
            $parent_id = Category::where('name','LIKE',$request->get('main'))->firstOrFail()->id;
            $categories['2'] = $categories['2']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });
        }

        if($request->has('general') && $request->get('general')!='All'){
            $parent_id = Category::where('name','=',$request->get('general'))->firstOrFail()->id;
            $categories['3'] = $categories['3']->reject(function($category) use($parent_id){
                return $category->parent_id != $parent_id;
            });
        }


        if($request->has('replicate')){
            $product = Product::find($request->get('replicate'));

            $request->session()->forget('_old_input');

            foreach ([
                '_old_input.description' => $product->name,
                '_old_input.srp' => $product->srp,
                '_old_input.wholesalequantity' => $product->wholesalequantity,
                ] as $key => $value) {
                    $request->session()->put($key,$value);
                }

                $image = $product->images()->where('primaryimage_flag','=','y')->latest('id')->first();

                if($image){

                    foreach ([
                        '_old_input.current_image_id' => $image->id,
                        '_old_input.current_image' => implode('',[
                            in_array($image->domain,[ 'http://localhost', 'http://127.0.0.1', 'http://127.0.0.1:8000', ]) ? null : $image->domain,
                            $image->filelocation,
                            $image->filename,
                        ]),
                        ] as $key => $value) {
                            $request->session()->put($key,$value);
                        }

                    }

                    return view('product-add',[ 'categories' => $categories ])->withInput($request);
                }

                return view('product-add',compact(['categories']));
            }

            public function store(Request $request){

                // Validate Request
                $this->validate($request,[
                    'description' => 'required|string',
                    'brand' => 'required|string',
                    'srp' => 'required|numeric|min:0',
                    'wholesalequantity' => 'required|numeric|min:0',
                    'image' => 'required',
                ],[
                    'brand.required' => 'Please select a product brand category'
                ]);

                // Get the ID of the selected category
                $category_id = Category::where('name','=',$request->get('brand'))->firstOrFail()->id;

                // Check if the product name and category exist
                $isexist = Product::where([ ['name','=',$request->get('description')] ])->count();

                if($isexist > 0){
                    return back()->withErrors([ 'Product already exist' ])->withInput();
                }

                $status = 'y';

                if($request->has('status')){
                    $status = $request->get('status');
                }

                if($request->has('image')){

                    $product = Product::create([
                        'name' => $request->get('description'),
                        'wholesalequantity' => $request->get('wholesalequantity'),
                        'srp' => $request->get('srp'),
                        'enable_flag' => $status,
                        'delete_flag' => 'n',
                        'category_id' => $category_id
                    ]);


                    $request->session()->push('added.product', [
                        'name' => $product->name,
                        'wholesalequantity' => $product->wholesalequantity,
                        'srp' => $product->srp,
                    ]);

                    $imageType = ImageType::firstOrCreate([
                        'name' => 'Primary',
                        'description' => 'Primary Image of the Product',
                    ]);

                    switch ($request->get('image')) {
                        case 'upload':

                        $this->validate($request,[
                            'upload' => 'required|file'
                        ]);

                        $filename = str_slug($product->name) . '-' . str_slug($imageType->name) . '-' . date('ymdhis') . '.jpg';

                        $img = Image::make($_FILES['upload']['tmp_name']);
                        $img->resize(1000, 1000, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        $img->save(public_path('uploads/' . $filename), 90);

                        $image = ProductImage::create([
                            'product_id' => $product->id,
                            'filename' => $filename,
                            'domain' => env('APP_URL','http://localhost'),
                            'filelocation' => '/uploads/',
                            'imagetype_id' => $imageType->id,
                            'enable_flag' => 'y',
                            'primaryimage_flag' => 'y',
                            'delete_flag' => 'n'
                        ]);

                        break;
                        case 'url':

                        $this->validate($request,[
                            'imageurl' => 'required|url'
                        ]);

                        if($request->has('imageurl')){

                            $url = parse_url($request->get('imageurl'));

                            $image = ProductImage::create([
                                'product_id' => $product->id,
                                'filename' => basename($url['path']) . (isset($url['query']) ? '?' . $url['query'] : ''),
                                'domain' => $url['scheme'] . '://' . $url['host'],
                                'filelocation' => str_replace(basename($url['path']),'',$url['path']),
                                'imagetype_id' => $imageType->id,
                                'primaryimage_flag' => 'y',
                                'enable_flag' => 'y',
                                'delete_flag' => 'n'
                            ]);

                        }
                        break;
                        case 'current':
                        $this->validate($request,[
                            'current_image_id' => 'required'
                        ]);

                        $current_image = ProductImage::find($request->get('current_image_id'))->replicate();
                        $current_image->product_id = $product->id;
                        $current_image->save();

                        break;
                    }
                }
                if($request->has('remember') && $request->get('remember')=='remember'){
                    return back()->withInput()->with('status','Product Added Successfully');
                }
                return back()->with('status','Product Added Successfully');
            }

            public function edit(Product $product,Request $request){


                $categories = Category::orderBy('name','asc')->get()->unique('name')->groupBy('categorylevel_id');
                $image = $product->images()->where('primaryimage_flag','=','y')->latest('id')->first();
                if($request->has('main') && $request->get('main')!='All'){
                    $parent_id = Category::where('name','=',$request->get('main'))->firstOrFail()->id;
                    $categories['2'] = $categories['2']->reject(function($category) use($parent_id){
                        return $category->parent_id != $parent_id;
                    });
                }

                if($request->has('general') && $request->get('general')!='All'){
                    $parent_id = Category::where('name','=',$request->get('general'))->firstOrFail()->id;
                    $categories['3'] = $categories['3']->reject(function($category) use($parent_id){
                        return $category->parent_id != $parent_id;
                    });
                }

                return view('product-edit',compact(['product','categories','image']));
            }

            public function update(Product $product,Request $request){

                // Validate Request
                $this->validate($request,[
                    'description' => 'required|string',
                    'brand' => 'required|string',
                    'srp' => 'required|numeric|min:0',
                    'wholesalequantity' => 'required|numeric|min:0',
                    'image' => 'required',
                ],[
                    'brand.required' => 'Please select a product brand category'
                ]);

                // Get the ID of the selected category
                $category_id = Category::where('name','=',$request->get('brand'))->firstOrFail()->id;
                $status = 'y';

                if($request->has('status')){
                    $status = $request->get('status');
                }


                if($request->has('image')){

                    $product->update([
                        'name' => $request->get('description'),
                        'wholesalequantity' => $request->get('wholesalequantity'),
                        'srp' => $request->get('srp'),
                        'enable_flag' => $status,
                        'delete_flag' => 'n',
                        'category_id' => $category_id
                    ]);

                    $imageType = ImageType::firstOrCreate([
                        'name' => 'Primary',
                        'description' => 'Primary Image of the Product',
                    ]);

                    switch ($request->get('image')) {
                        case 'upload':

                        $this->validate($request,[
                            'upload' => 'required|file'
                        ]);

                        $filename = str_slug($product->name) . '-' . str_slug($imageType->name) . '-' . date('ymdhis') . '.jpg';

                        $img = Image::make($_FILES['upload']['tmp_name']);
                        $img->resize(1000, 1000, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        $img->save(public_path('uploads/' . $filename), 90);

                        $image = ProductImage::create([
                            'product_id' => $product->id,
                            'filename' => $filename,
                            'domain' => env('APP_URL','http://localhost'),
                            'filelocation' => '/uploads/',
                            'imagetype_id' => $imageType->id,
                            'enable_flag' => 'y',
                            'primaryimage_flag' => 'y',
                            'delete_flag' => 'n'
                        ]);

                        break;
                        case 'url':

                        $this->validate($request,[
                            'imageurl' => 'required|url'
                        ]);

                        if($request->has('imageurl')){

                            $url = parse_url($request->get('imageurl'));
                            $image = ProductImage::create([
                                'product_id' => $product->id,
                                'filename' => basename($url['path']) . (!empty($url['query']) ? '?' . $url['query'] : ''),
                                'domain' => $url['scheme'] . '://' . $url['host'],
                                'filelocation' => str_replace(basename($url['path']),'',$url['path']),
                                'imagetype_id' => $imageType->id,
                                'enable_flag' => 'y',
                                'primaryimage_flag' => 'y',
                                'delete_flag' => 'n'
                            ]);

                        }
                        break;
                        case 'current':
                        $this->validate($request,[
                            'current_image_id' => 'required'
                        ]);

                        $current_image = ProductImage::find($request->get('current_image_id'))->replicate();
                        $current_image->product_id = $product->id;
                        $current_image->save();

                        break;
                    }
                }

                return back()->with('status','Product Updated Successfully');

            }


            public function destroy(Request $request,Product $product){
                $product->update([
                    'delete_flag' => 'y'
                ]);

                return back()->with('status','Product deleted successfully');
            }

            public function destroyBatch(Request $request){

                $ids = explode(',',$request->get('ids'));

                $products = Product::whereIn('id', $ids)
                ->get();
                foreach ($products as $key => $product) {
                    $product->update([
                        'delete_flag' => 'y',
                    ]);
                }
                return back()->with('status','Products deleted successfully');
            }


            public function prices(Product $product,Request $request)
            {
                $limit = 10;
                $sort = 'asc';

                $prices = $product->prices()->select('company_product.*');

                if($request->has('perpage')){
                    $limit = $request->get('perpage');
                }

                if($request->has('sortdir') && $request->has('sortdir')!=''){
                    switch ($request->get('sortdir')) {
                        case 'asc':
                        $sort = 'asc';
                        break;
                        default:
                        $sort = 'desc';
                        break;
                    }
                }

                if($request->has('sortby') && $request->has('sortby')!=''){
                    switch ($request->get('sortby')) {
                        case 'supplier':
                        $prices = $prices->join('company as s','s.id','=','company_product.company_id')
                        ->addSelect('s.name as suppliername')
                        ->orderBy('s.name', $sort);
                        break;
                        case 'retailprice':
                        case 'wholesaleprice':
                        case 'dateupdatedretailprice':
                        case 'dateupdatedwholesaleprice':
                        $prices = $prices->orderBy('company_product.' . $request->get('sortby'), $sort);
                        break;
                        case 'status':
                        $prices = $prices->orderBy('enable_flag', $sort);
                        break;
                    }
                }


                if($request->has('export')){
                    $titles = [
                        'Supplier',
                        'Retail Price',
                        'Wholesale Price',
                        'Date Updated Retail Price',
                        'Date Wholesale Retail Price',
                        'Status',
                    ];


                    $prices = $prices->get()->map(function ($price, $key) use($titles) {
                        return [
                            $price->supplier->name ,
                            $price->retailprice ,
                            $price->wholesaleprice ,
                            $price->dateupdatedretailprice ,
                            $price->dateupdatedwholesaleprice ,
                            $price->enable_flag=='y' ? 'Active' : 'Inactive',
                        ];
                    });

                    $prices = $prices->toArray();
                    Excel::create('export-prices-' . str_slug($product->name) . '-' . date('YmdHisa'), function($excel) use($prices,$product,$titles){

                        $excel->setTitle('Product Prices - '. $product->name);

                        $excel->sheet('Results', function($sheet) use($prices,$titles){
                            $sheet->fromArray($prices, null, 'A1', false, false);
                            $sheet->prependRow($titles);
                        })->export('xls');

                    });

                    return back();
                }else{
                    $prices = $prices->paginate($limit);
                }


                return view('product-prices',compact(['product','prices']))->withInput($request);
            }

            public function images(Product $product,Request $request)
            {
                $limit = 10;
                $sort = 'asc';

                $images = $product->images()->select('productimage.*')->where('productimage.delete_flag','NOT LIKE','y');

                if($request->has('perpage')){
                    $limit = $request->get('perpage');
                }

                if($request->has('sortdir') && $request->has('sortdir')!=''){
                    switch ($request->get('sortdir')) {
                        case 'asc':
                        $sort = 'asc';
                        break;
                        default:
                        $sort = 'desc';
                        break;
                    }
                }

                if($request->has('imagetype') && $request->get('imagetype') != 'All'){
                    $images = $images->whereHas('imagetype',function($query) use($request){
                        $query->where('name','=',$request->get('imagetype'));
                    });
                }

                if($request->has('sortby') && $request->has('sortby')!=''){
                    switch ($request->get('sortby')) {
                        case 'name':
                        $images = $images->orderBy('filename', $sort);
                        break;
                        case 'type':
                        $images = $images->join('imagetype as i','i.id','=','productimage.imagetype_id')
                        ->orderBy('i.name', $sort);;
                        break;
                        case 'status':
                        $images = $images->orderBy('enable_flag', $sort);
                        break;
                    }
                }


                if($request->has('export')){
                    $titles = [
                        'URI',
                        'Name',
                        'Type',
                        'Status',
                    ];


                    $images = $images->get()->map(function ($image, $key) use($titles) {
                        return [
                            $image->getImageURI(),
                            $image->filename,
                            $image->imagetype->name,
                            $image->isEnabled()
                        ];
                    });

                    $images = $images->toArray();
                    Excel::create('export-images-' . str_slug($product->name) . '-' . date('YmdHisa'), function($excel) use($images,$product,$titles){

                        $excel->setTitle('Product Images - '. $product->name);

                        $excel->sheet('Results', function($sheet) use($images,$titles){
                            $sheet->fromArray($images, null, 'A1', false, false);
                            $sheet->prependRow($titles);
                        })->export('xls');

                    });

                    return back();
                }else{
                    $images = $images->paginate($limit);
                }

                $imagetype  = ImageType::all();

                return view('product-images',compact(['product','images','imagetype']))->withInput($request);
            }

            public function imageedit(Product $product,ProductImage $image,Request $request){
                return view('product-image-edit',compact(['product','image']));
            }

            public function imageupdate(Product $product,ProductImage $image,Request $request){

                if($request->has('imagetype')){

                    $imageType = ImageType::firstOrCreate([
                        'name' => $request->get('imagetype'),
                        'description' => $request->get('imagetype') . ' Image of the Product',
                    ]);

                    $image->update([
                        'imagetype_id' => $imageType->id
                    ]);
                }else{
                    $imageType = ImageType::firstOrCreate([
                        'name' => 'Primary',
                        'description' => 'Primary Image of the Product',
                    ]);
                }

                if($request->has('image')){



                    switch ($request->get('image')) {
                        case 'upload':

                        $this->validate($request,[
                            'upload' => 'required|file'
                        ]);

                        $filename = str_slug($product->name) . '-' . str_slug($imageType->name) . '-' . date('ymdhis') . '.jpg';

                        $img = Image::make($_FILES['upload']['tmp_name']);
                        $img->resize(1000, 1000, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        $img->save(public_path('uploads/' . $filename), 90);

                        $image->update([
                            'filename' => $filename,
                            'domain' => env('APP_URL','http://localhost'),
                            'filelocation' => '/uploads/',
                        ]);

                        break;
                        case 'url':

                        $this->validate($request,[
                            'imageurl' => 'required|url'
                        ]);

                        if($request->has('imageurl')){

                            $url = parse_url($request->get('imageurl'));
                            $image->update([
                                'filename' => basename($url['path']) . (!empty($url['query']) ? '?' . $url['query'] : ''),
                                'domain' => $url['scheme'] . '://' . $url['host'],
                                'filelocation' => str_replace(basename($url['path']),'',$url['path']),
                            ]);

                        }
                        break;
                        case 'current':
                        $this->validate($request,[
                            'current_image_id' => 'required'
                        ]);

                        break;
                    }
                }


                if($request->has('isPrimary')){
                    $image->update([
                        'primaryimage_flag' => $request->get('isPrimary') == 'yes' ? 'y' : 'n'
                    ]);
                }

                if($request->has('isactive')){
                    $image->update([
                        'enable_flag' => $request->get('isactive') == 'yes' ? 'y' : 'n'
                    ]);
                }

                return back()->with('status','Product Image Updated Successfully');

            }

            public function imagedestroy(Request $request,Product $product,ProductImage $image){

                $image->update([
                    'delete_flag' => 'y',
                ]);

                return back()->with('status','Product Image deleted successfully');
            }

            public function imagedestroyBatch(Request $request){

                $ids = explode(',',$request->get('ids'));

                $productimages = ProductImage::whereIn('id', $ids)
                ->get();
                foreach ($productimages as $key => $productimage) {
                    $productimage->update([
                        'delete_flag' => 'y',
                    ]);
                }
                return back()->with('status','Product Images deleted successfully');
            }

            public function imageadd(Product $product,ProductImage $image,Request $request){
                return view('product-image-add',compact(['product','image']));
            }
            public function imagecreate(Product $product,Request $request){

                $imagetypename = 'Primary';

                if($request->has('imagetype')){

                    $imagetypename = $request->get('imagetype');

                }

                $imageType = ImageType::firstOrCreate([
                    'name' => $imagetypename,
                    'description' => $imagetypename . ' Image of the Product',
                ]);


                if($request->has('image')){

                    switch ($request->get('image')) {
                        case 'upload':

                        $this->validate($request,[
                            'upload' => 'required|file'
                        ]);

                        $filename = str_slug($product->name) . '-' . str_slug($imageType->name) . '-' . date('ymdhis') . '.jpg';

                        $img = Image::make($_FILES['upload']['tmp_name']);
                        $img->resize(1000, 1000, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        });

                        $img->save(public_path('uploads/' . $filename), 90);

                        $image = ProductImage::create([
                            'product_id' => $product->id,
                            'filename' => $filename,
                            'domain' => env('APP_URL','http://localhost'),
                            'filelocation' => '/uploads/',
                            'imagetype_id' => $imageType->id,
                            'enable_flag' => 'y',
                            'primaryimage_flag' => 'y',
                            'delete_flag' => 'n'
                        ]);

                        break;
                        case 'url':

                        $this->validate($request,[
                            'imageurl' => 'required|url'
                        ]);

                        if($request->has('imageurl')){

                            $url = parse_url($request->get('imageurl'));

                            $image = ProductImage::create([
                                'product_id' => $product->id,
                                'filename' => basename($url['path']) . (!empty($url['query']) ? '?' . $url['query'] : ''),
                                'domain' => $url['scheme'] . '://' . $url['host'],
                                'filelocation' => str_replace(basename($url['path']),'',$url['path']),
                                'imagetype_id' => $imageType->id,
                                'enable_flag' => 'y',
                                'primaryimage_flag' => 'y',
                                'delete_flag' => 'n'
                            ]);
                        }
                        break;
                        case 'current':
                        $this->validate($request,[
                            'current_image_id' => 'required'
                        ]);

                        $current_image = ProductImage::find($request->get('current_image_id'))->replicate();
                        $current_image->product_id = $product->id;
                        $current_image->save();

                        break;
                    }
                }

                if($request->has('isPrimary')){
                    $image->update([
                        'primaryimage_flag' => $request->get('isPrimary') == 'yes' ? 'y' : 'n'
                    ]);
                }

                if($request->has('isactive')){
                    $image->update([
                        'enable_flag' => $request->get('isactive') == 'yes' ? 'y' : 'n'
                    ]);
                }

                return back()->with('status','Product Image Added Successfully');
            }

            public function offers(Product $product){
                $offers = $product->prices->where('enable_flag','=','y');
                $offerpack = [];
                foreach ($offers as $key => $offer) {
                    $offerpack[] = [
                        'supplier' => Supplier::select(['name','id'])->find($offer['company_id']),
                        'retailprice' => $offer['retailprice'],
                        'wholesaleprice' => $offer['wholesaleprice']
                    ];
                }
                return response()->json($offerpack);
            }
        }
