<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
class CompanyImage extends Model
{
    //
    protected $table = 'companyimage';
    public $timestamps = false;
    protected $fillable = [
         'company_id', 'filename', 'domain', 'filelocation', 'imagetype_id', 'primaryimage_flag', 'delete_flag'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function getImageURI(){
        return implode('',[
            in_array($this->domain,[ 'http://localhost', 'http://127.0.0.1', 'http://127.0.0.1:8000', ]) ? null : $this->domain,
            $this->filelocation,
            $this->filename,
        ]);
    }

    public function imagetype()
    {
        return $this->belongsTo('App\ImageType');
    }

    public function isEnabled(){
        return $this->enable_flag == 'y' ? 'Active' : 'Inactive';
    }
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('delete_flag', function (Builder $builder) {
            $builder->where('companyimage.delete_flag', 'LIKE', 'n');
        });
    }
}
