<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Product extends Model
{
    //
    protected $table = 'product';
    public $timestamps = false;
    protected $fillable = [
        'name', 'category_id', 'description', 'wholesalequantity', 'srp', 'enable_flag', 'delete_flag'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }

    public function prices()
    {
        return $this->hasMany('App\ProductPrices');
    }

    public function bestRetailPrice()
    {
        return $this->hasMany('App\ProductPrices')->orderBy('retailprice','asc')->first();
    }

    public function bestWholeSalePrice()
    {
        return $this->hasMany('App\ProductPrices')->orderBy('wholesaleprice','asc')->first();
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('delete_flag', function (Builder $builder) {
            $builder->where('product.delete_flag', 'LIKE', 'n');
        });
    }
}
