<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $table = 'contact';
    public $timestamps = false;
    protected $fillable = [
         'company_id', 'name', 'lastname', 'firstname', 'middlename', 'designation', 'primary_flag', 'enable_flag', 'delete_flag'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function details()
    {
        return $this->hasMany('App\ContactDetails');
    }

}
