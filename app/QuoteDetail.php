<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class QuoteDetail extends Model
{
    //
    protected $table = 'quotedetail';
    public $timestamps = false;
    protected $fillable = [
        'quote_id', 'cartdetail_id', 'product_id', 'quantity', 'unitprice', 'totalprice', 'delete_flag'
    ];

    public function quote()
    {
        return $this->belongsTo('App\Quote');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function cartdetail()
    {
        return $this->belongsTo('App\CartDetail');
    }

    public function supplier()
    {
        return $this->belongsToMany('App\Supplier','supplier_quotedetail','quotedetail_id','supplier_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('delete_flag', function (Builder $builder) {
            $builder->where('quotedetail.delete_flag', 'LIKE', 'n');
        });
    }
}
