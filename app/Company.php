<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Company extends Model
{
    //
    protected $table = 'company';
    public $timestamps = false;
    protected $fillable = [
        'name', 'vat', 'businesstype_id', 'businessregistration', 'blacklisted_flag', 'blacklistedremarks', 'dateblacklisted', 'enabled_flag', 'supplier_flag', 'delete_flag'
    ];

    public function businesstype()
    {
        return $this->belongsTo('App\BusinessType');
    }

    public function isSupplier(){
        return $this->supplier_flag == 'y' ? 'Yes' : 'No';
    }

    public function isEnabled(){
        return $this->enabled_flag == 'y' ? 'Active' : 'Not Active';
    }

    public function isBlacklisted(){
        return $this->blacklisted_flag == 'y';
    }

    public function address()
    {
        return $this->hasOne('App\Address');
    }

    public function account()
    {
        return $this->hasOne('App\Account');
    }

    public function user()
    {
        return $this->hasOne('App\Account');
    }

    public function contact()
    {
        return $this->hasOne('App\Contact');
    }
    public function image()
    {
        return $this->hasOne('App\CompanyImage');
    }

    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product','company_product','company_id','product_id')->withPivot('retailprice' , 'wholesaleprice' , 'dateupdatedretailprice' , 'dateupdatedwholesaleprice');
    }


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('delete_flag', function (Builder $builder) {
            $builder->where('company.delete_flag', 'LIKE', 'n');
        });
    }
}
