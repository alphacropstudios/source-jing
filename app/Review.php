<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Review extends Model
{
    //
    protected $table = 'review';
    public $timestamps = false;
    public $dates = [
        'date'
    ];
    protected $fillable = [
        'company_id', 'rating', 'date', 'remark', 'ratedbyuser_id', 'quote_id', 'delete_flag'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function account()
    {
        return $this->belongsTo('App\Account','ratedbyuser_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('delete_flag', function (Builder $builder) {
            $builder->where('review.delete_flag', 'LIKE', 'n');
        });
    }
}
