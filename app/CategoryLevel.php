<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryLevel extends Model
{
    //
    protected $table = 'categorylevel';
    public $timestamps = false;
    protected $fillable = [
        'name', 'description', 'delete_flag'
    ];

    public function categories()
    {
        return $this->belongsTo('App\Category');
    }
}
