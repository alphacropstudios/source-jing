<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'type_id' => 1,
        'delete_flag' => 'n'
        // 'remember_token' => str_random(10),
    ];
});


$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->lastName,
        'categorylevel_id' => 1,
        'parent_id' => 0,
        'description' => ucfirst($faker->words(4,true)),
        'enable_flag' => 'y',
        'delete_flag' => 'n',
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->unique()->domainWord) . ' ' . ucfirst($faker->colorName) ,
        'category_id' => 1,
        'description' => ucfirst($faker->bs()),
        'wholesalequantity' => $faker->numerify('#50'),
        'srp' => $faker->randomFloat(2,100,9999),
        'enable_flag' => 'y',
        'delete_flag' => 'n',
    ];
});

$factory->define(App\ProductImage::class, function (Faker\Generator $faker) {
    $url = parse_url($faker->imageUrl(300,300));
    return [
        'product_id' => 1,
        'filename' => basename($url['path']) . ($url['query'] ? '?' . $url['query'] : ''),
        'domain' => $url['scheme'] . '://' . $url['host'],
        'filelocation' => trim(rtrim($url['path'],'/'),basename($url['path'])),
        'imagetype_id' => 1,
        'primaryimage_flag' => 'n',
        'enable_flag' => 'y',
        'delete_flag' => 'n',
    ];
});

$factory->define(App\ImageType::class, function (Faker\Generator $faker) {
    return [
        'name' => 'Primary',
        'description' => 'Primary Image of the Product',
        'delete_flag' => 'n',
    ];
});

$factory->define(App\BusinessType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->jobTitle(),
        'description' => $faker->bs(),
        'delete_flag' => 'n',
    ];
});

$factory->define(App\Country::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->country,
        'description' => '',
        'delete_flag' => 'n',
    ];
});

$factory->define(App\Company::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company(),
        'vat' => $faker->iban('US'),
        'businesstype_id' => 1,
        'businessregistration' => $faker->swiftBicNumber(),
        'blacklisted_flag' => 'n',
        'blacklistedremarks' => '',
        'dateblacklisted' => null,
        'enabled_flag' => 'y',
        'supplier_flag' => $faker->randomElement(['y','n']),
        'delete_flag' => 'n',
    ];
});

$factory->define(App\Address::class, function (Faker\Generator $faker) {
    return [
        'company_id' => 1,
        'street' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->state,
        'country_id' => 1,
        'zipcode' => $faker->buildingNumber,
        'delete_flag' => 'n'
    ];
});

$factory->define(App\Account::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->unique()->userName,
        'company_id' => 1,
        'activation_code' => $faker->ean8,
        'password' => bcrypt($faker->password),
        'ip_address' => '' ,
        'salt' => str_random(16) ,
        'email' =>  $faker->unique()->safeEmail ,
        'forgotten_password_code' => time() ,
        'forgotten_password_time' => time() ,
        'remember_code' => '' ,
        'created_on' => time() ,
        'last_login' => time() ,
        'active' => 1 ,
        'first_name' => $faker->firstName ,
        'last_name' =>  $faker->lastName ,
        'company' => '' ,
        'phone' => ''
    ];
});

$factory->define(App\Contact::class, function (Faker\Generator $faker) {

    $firstname = $faker->firstName;
    $lastname = $faker->lastName;

    return [
        'company_id' => 1,
        'name' => $firstname . ' ' . $lastname,
        'lastname' => $firstname,
        'firstname' => $lastname,
        'middlename' => $faker->lastName,
        'designation' => $faker->jobTitle,
        'primary_flag' => 'n',
        'enable_flag' => 'y',
        'delete_flag' => 'n',
    ];
});

$factory->define(App\Cart::class, function (Faker\Generator $faker) {
    return [
        'contact_id' => 1,
        'company_id' => 1,
        'instruction' => $faker->optional()->text(100),
        'orderdate' => $faker->dateTimeBetween('-60 Days','-30 Days'),
        'deliverydate' => $faker->dateTimeThisMonth,
        'status_id' => 1,
        'title' => $faker->optional()->text(30),
        'delete_flag' => 'n',
    ];
});

$factory->define(App\Quote::class, function (Faker\Generator $faker) {
    return [
        'cart_id' => 1,
        'contact_id' => 1,
        'company_id' => 1,
        'instruction' => $faker->optional()->text(100),
        'orderdate' => $faker->dateTimeBetween('-60 Days','-30 Days'),
        'deliverydate' => $faker->dateTimeThisMonth,
        'totalquoteprice' => $faker->randomFloat(2,1000,99999),
        'expirydate' => $faker->dateTimeBetween('60 Days','120 Days'),
        'title' => $faker->optional()->text(30),
        'status_id' => 1,
        'delete_flag' => 'n',
    ];
});



$factory->define(App\QuoteDetail::class, function (Faker\Generator $faker) {
    return [
        'quote_id' => 1,
        'cartdetail_id' => 1,
        'product_id' => 1,
        'quantity' => $faker->numerify('#0'),
        'unitprice' => $faker->randomFloat(2,1000,99999),
        'totalprice' => $faker->randomFloat(2,1000,99999),
        'delete_flag' => 'n',

    ];
});
$factory->define(App\CartDetail::class, function (Faker\Generator $faker) {
    return [
        'cart_id' => 1,
        'product_id' => 1,
        'quantity' => $faker->numerify('#0'),
        'delete_flag' => 'n',
    ];
});
$factory->define(App\ContactDetails::class, function (Faker\Generator $faker) {
    return [
        'contact_id' => 1,
        'name' => $faker->email,
        'contacttype_id' => $faker->numberBetween(1,5),
        'delete_flag' => 'n',
    ];
});
$factory->define(App\ContactType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->randomElement([
            'email',
            'default_phone',
            'home_phone',
            'work_phone',
            'mobile_phone'
        ]),
        'description' => $faker->text(30),
        'delete_flag' => 'n',
    ];
});
