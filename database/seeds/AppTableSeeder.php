<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
class AppTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {

        $faker = Faker\Factory::create();

        DB::table('category')->truncate();
        DB::table('product')->truncate();
        DB::table('imagetype')->truncate();
        DB::table('productimage')->truncate();
        DB::table('product')->truncate();
        DB::table('businesstype')->truncate();
        DB::table('company')->truncate();
        DB::table('address')->truncate();
        DB::table('country')->truncate();
        DB::table('contact')->truncate();
        DB::table('cart')->truncate();
        DB::table('quote')->truncate();
        DB::table('quotedetail')->truncate();
        DB::table('cartdetail')->truncate();
        DB::table('adminuser')->truncate();
        DB::table('contactdetails')->truncate();
        DB::table('contacttype')->truncate();

        factory(App\User::class)->create([
            'username' => 'chrispinugu',
            'email' => 'chrispinugu@yahoo.com',
            'name' => 'Christopher Pinugu',
            'password' => bcrypt('chrispinugu123$$')
        ]);

        factory(App\User::class)->create([
            'username' => 'ralphjesy12',
            'email' => 'ralphjesy@gmail.com',
            'name' => 'Ralph John Galindo',
            'password' => bcrypt('ralph123$$')
        ]);

        factory(App\ImageType::class,1)->create();

        factory(App\ContactType::class,5)->create();

        foreach ( [ 'Healthcare', 'Trading', 'BPO', 'Human Resources', 'Public Relations', 'Information Technology' ] as $key => $value) {
            factory(App\BusinessType::class)->create([
                'name' => $value
            ]);
        }
        $carts = 0;
        for ($i=1; $i <= 30; $i++) {
            factory(App\Company::class)->create([
                'businesstype_id' => $faker->numberBetween(1,6)
            ]);

            factory(App\Address::class)->create([
                'company_id' => $i
            ]);

            factory(App\Account::class)->create([
                'company_id' => $i
            ]);

            factory(App\Contact::class)->create([
                'company_id' => $i,
                'primary_flag' => 'y'
            ]);

            foreach ([
                'email',
                'default_phone',
                'home_phone',
                'work_phone',
                'mobile_phone'
                ] as $key => $value) {
                    if($key==0){
                        $name = $faker->companyEmail;
                    }else{
                        $name = $faker->tollFreePhoneNumber;
                    }

                    factory(App\ContactDetails::class)->create([
                        'contact_id' => $i,
                        'name' => $name,
                        'contacttype_id' => $key+1
                    ]);
            }

            $randomNumber = $faker->numberBetween(1,5);

            factory(App\Cart::class,$randomNumber)->create([
                'company_id' => $i,
                'contact_id' => $i,
            ]);

            for ($j=$carts; $j < $carts + $randomNumber; $j++) {
                factory(App\Quote::class)->create([
                    'cart_id' => $j+1,
                    'company_id' => $i,
                    'contact_id' => $i,
                ]);
                factory(App\QuoteDetail::class)->create([
                    'quote_id' => $j+1,
                    'cartdetail_id' => $j+1,
                    'product_id' => $i,
                ]);
                factory(App\CartDetail::class)->create([
                    'cart_id' => $j+1,
                    'product_id' => $i,
                ]);
            }

            $carts += $randomNumber;
        }




        factory(App\Country::class,30)->create();

        factory(App\Category::class,4)->create()->each(function($category){
            factory(App\Category::class,2)->create([
                'categorylevel_id' => 2,
                'parent_id' => $category->id
                ])->each(function($category){
                    factory(App\Category::class,2)->create([
                        'categorylevel_id' => 3,
                        'parent_id' => $category->id
                        ])->each(function($category){
                            factory(App\Product::class,5)->create([
                                'category_id' => $category->id
                                ])->each(function($product){
                                    factory(App\ProductImage::class,1)->create([
                                        'product_id' => $product->id,
                                        'primaryimage_flag' => 'y'
                                    ]);
                                });
                            });
                        });
                    });
                }
            }
