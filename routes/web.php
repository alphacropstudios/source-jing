<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    if(Auth::user())
    return redirect()->intended('home');
    return view('login');
});

Route::get('themes', function () {
    return view('themes');
});

Route::get('home', 'HomeController@index')->name('home');
Route::get('quote', 'QuoteController@index')->name('quote');

Route::get('category', 'CategoryController@index')->name('category');
Route::get('category/add', 'CategoryController@create')->name('category.create');
Route::post('category/save', 'CategoryController@store')->name('category.store');
Route::get('category/edit/{category}', 'CategoryController@edit')->name('category.edit');
Route::post('category/update/{category}', 'CategoryController@update')->name('category.update');
Route::get('category/delete/{category}', 'CategoryController@destroy')->name('category.destroy');
Route::get('category/deleteBatch', 'CategoryController@destroyBatch')->name('category.destroyBatch');
Route::get('category/level', 'CategoryController@level')->name('category.level');
Route::get('category/leveladd', 'CategoryController@leveladd')->name('category.leveladd');
Route::post('category/levelstore', 'CategoryController@levelstore')->name('category.levelstore');
Route::get('category/leveledit/{level}', 'CategoryController@leveledit')->name('category.leveledit');
Route::post('category/levelupdate/{level}', 'CategoryController@levelupdate')->name('category.levelupdate');
Route::get('category/leveldelete/{level}', 'CategoryController@leveldestroy')->name('category.leveldestroy');
Route::get('category/leveldeleteBatch', 'CategoryController@leveldestroyBatch')->name('category.leveldestroyBatch');

Route::get('product', 'ProductController@index')->name('product');
Route::get('product/add', 'ProductController@create')->name('product.create');
Route::post('product/store', 'ProductController@store')->name('product.store');
Route::get('product/edit/{product}', 'ProductController@edit')->name('product.edit');
Route::get('product/delete/{product}', 'ProductController@destroy')->name('product.destroy');
Route::get('product/deleteBatch', 'ProductController@destroyBatch')->name('product.destroyBatch');

Route::post('product/update/{product}', 'ProductController@update')->name('product.update');
Route::get('product/price/{product}', 'ProductController@prices')->name('product.prices');




Route::get('product/offers/{product}', 'ProductController@offers')->name('product.offers');

Route::get('product/image/deleteBatch', 'ProductController@imagedestroyBatch')->name('product.imagedestroyBatch');
Route::get('product/image/{product}', 'ProductController@images')->name('product.images');
Route::get('product/image/{product}/add', 'ProductController@imageadd')->name('product.imageadd');
Route::post('product/image/{product}/create', 'ProductController@imagecreate')->name('product.imagecreate');
Route::get('product/image/{product}/{image}', 'ProductController@imageedit')->name('product.imageedit');
Route::post('product/image/{product}/{image}', 'ProductController@imageupdate')->name('product.imageupdate');
Route::get('product/image/{product}/delete/{image}', 'ProductController@imagedestroy')->name('product.imagedestroy');


Route::get('company/deleteBatch', 'CompanyController@destroyBatch')->name('company.destroyBatch');
Route::get('company', 'CompanyController@index')->name('company');
Route::get('company/add', 'CompanyController@create')->name('company.create');
Route::post('company/save', 'CompanyController@store')->name('company.store');
Route::get('company/edit/{company}', 'CompanyController@edit')->name('company.edit');
Route::post('company/update/{company}', 'CompanyController@update')->name('company.update');
Route::get('company/{company}/delete', 'CompanyController@destroy')->name('company.destroy');
Route::get('company/{company}/contacts', 'CompanyController@contacts')->name('company.contacts');
Route::get('company/{company}/contacts/add', 'CompanyController@contactadd')->name('company.contactadd');
Route::post('company/{company}/contacts/add', 'CompanyController@contactstore')->name('company.contactstore');
Route::get('company/{company}/contacts/{contact}/edit', 'CompanyController@contactedit')->name('company.contactedit');
Route::post('company/{company}/contacts/{contact}/update', 'CompanyController@contactupdate')->name('company.contactupdate');
Route::get('company/{company}/contacts/{contact}/delete', 'CompanyController@contactdestroy')->name('company.contactdelete');
Route::get('company/{company}/contacts/deleteBatch', 'CompanyController@contactdestroyBatch')->name('company.contactdeletebatch');
Route::get('company/{company}/blacklist', 'CompanyController@blacklist')->name('company.blacklist');
Route::post('company/{company}/blacklist', 'CompanyController@saveblacklist')->name('company.saveblacklist');
Route::get('company/{company}/account', 'CompanyController@account')->name('company.account');
Route::post('company/{company}/accountsave', 'CompanyController@accountsave')->name('company.accountsave');
Route::get('company/{company}/product', 'CompanyController@product')->name('company.product');

Route::get('company/{company}/sendactivation', 'CompanyController@sendactivation')->name('company.sendactivation');
Route::post('company/{company}/sendactivationemail', 'CompanyController@sendactivationemail')->name('company.sendactivationemail');
Route::get('company/activate/success', 'CompanyController@activateaccountsuccess')->name('company.activateaccountsuccess');
Route::get('company/activate/{code}', 'CompanyController@activateaccount')->name('company.activateaccount');
Route::get('company/{company}/deactivate', 'CompanyController@deactivateaccount')->name('company.deactivateaccount');
Route::get('company/{company}/whitelist', 'CompanyController@whitelist')->name('company.whitelist');

Route::get('admin/users', 'HomeController@adminusers')->name('admin.users');
Route::get('admin/users/add', 'HomeController@adminusersadd')->name('admin.users.add');
Route::post('admin/users/create', 'HomeController@adminuserscreate')->name('admin.users.create');
Route::get('admin/user/{user}/edit', 'HomeController@adminusersedit')->name('admin.users.edit');
Route::post('admin/users/{user}/update', 'HomeController@adminusersupdate')->name('admin.users.update');

Route::get('admin/users/{user}/delete', 'HomeController@adminuserdestroy')->name('admin.users.destroy');
Route::get('admin/users/deleteBatch', 'HomeController@adminuserdestroyBatch')->name('admin.users.destroyBatch');

Route::get('quote/regular', 'QuoteController@regularindex')->name('quote.regular.index');
Route::get('quote/regular/add', 'QuoteController@regularcreate')->name('quote.regular.create');
Route::get('quote/regular/add/print', 'QuoteController@regularcreateprint')->name('quote.regular.create.print');
Route::get('quote/regular/add/tempcart', 'QuoteController@regularaddtmpitem')->name('quote.regular.add.tmp.item');
Route::get('quote/regular/add/tempcartedit', 'QuoteController@regularaddtempcartedit')->name('quote.regular.edit.tmp.item');
Route::get('quote/regular/add/tempcartdelete', 'QuoteController@regularaddtempcartdelete')->name('quote.regular.delete.tmp.item');
Route::post('quote/regular/store', 'QuoteController@regularstore')->name('quote.regular.store');
Route::get('quote/regulardelete/{quote}', 'QuoteController@regulardestroy')->name('quote.regular.destroy');
Route::get('quote/regulardeleteBatch', 'QuoteController@regulardestroyBatch')->name('quote.regular.destroyBatch');
Route::get('quote/regular/{quote}/edit', 'QuoteController@regularedit')->name('quote.regular.edit');
Route::get('quote/regular/{quote}/edit/print', 'QuoteController@regulareditprint')->name('quote.regular.edit.print');

Route::get('quote/regular/add/cart/{quote}', 'QuoteController@regularaddcartitem')->name('quote.regular.add.cart.item');
Route::get('quote/regular/add/cart/{quote}/edit', 'QuoteController@regularaddcartedit')->name('quote.regular.edit.cart.item');
Route::get('quote/regular/add/cart/{quote}/delete', 'QuoteController@regularaddcartdelete')->name('quote.regular.delete.cart.item');
Route::post('quote/regular/update/{quote}', 'QuoteController@regularupdate')->name('quote.regular.update');

Route::get('quote/regular/add/custom/item', 'QuoteController@regularaddcustomitem')->name('quote.regular.add.custom.item');
Route::post('quote/regular/add/custom/item', 'QuoteController@regularaddcustomitemstore')->name('quote.regular.add.custom.item.store');
Route::get('quote/regular/assign', 'QuoteController@regularassign')->name('quote.regular.assign');

Route::get('reviews','CompanyController@reviews')->name('customer.reviews');
Route::get('review/delete/{review}', 'CompanyController@reviewdestroy')->name('customer.review.destroy');
Route::get('review/deleteBatch', 'CompanyController@reviewdestroyBatch')->name('customer.review.destroyBatch');

Route::get('quote/custom', 'QuoteController@customindex')->name('quote.custom.index');
Route::get('quote/customdelete/{quote}', 'QuoteController@customdestroy')->name('quote.custom.destroy');
Route::get('quote/customdeleteBatch', 'QuoteController@customdestroyBatch')->name('quote.custom.destroyBatch');
